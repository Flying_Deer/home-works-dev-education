package com.github.guessnumber;

public class ConfirmConstants {
    final private static String Yes = "y";
    final private static String No = "n";

    public static String No() {
        return No;
    }

    public static String Yes() {
        return Yes;
    }
}
