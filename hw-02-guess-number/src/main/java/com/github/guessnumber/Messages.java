package com.github.guessnumber;

public class Messages {

    public static void PrintHelloMessage(){
        System.out.println("Hello!");
        System.out.println("Welcome to \"Guess\" number game");
        System.out.println("In this game you need to guess a number from certain range in limited number of attempts");
        System.out.println("You can set range and number of attempts before every play");
        System.out.println("Default range is from 1 to 100, default number of attempts is 5");
        System.out.println("You can not set limits lesser then 1 and greater then 200");
        System.out.println("And you can not give youself more then 15 attempts");
        System.out.println("You always can quit the game using command \"exit\"");
        System.out.println("Have fun!");
    }

    public static void start(int _min, int _max, int _guessnumber){
        System.out.println("Guess a number in range from " + _min + " to " + _max);
        System.out.println("You have " + _guessnumber + " attempts");
    }

    public static void winGame(int AttemptUsed){
        System.out.println("Congratulations, your guess is correct!");
        if(AttemptUsed == 1)
            System.out.println("You win in 1 attempt");
        else
            System.out.println("You win in " + AttemptUsed + " attempts");
    }

    public static void loseGame(){
        System.out.println("Nooooooo, you used all your attempts...");
        System.out.println("Better luck next time");
    }

    public static void askGuess(){
        System.out.println("Enter your guess:");
    }

    public static void wrongInput(){
        System.out.println("Please enter an integer number");
    }

    public static void firstGuess(){
        System.out.println("Good guess, but no. Try again");
    }

    public static void betterGuess(){
        System.out.println("Not bad, you are getting closer. Try again");
    }

    public static void worseGuess(){
        System.out.println("Nope, previous guess was better. Try again");
    }

    public static void sameGuess(){
        System.out.println("Interesting. Your guess is as close as previous. Try again");
    }

    public static void askSettings(){
        System.out.println("Do you want to set range and number of guesses? (y/n)");
    }

    public static void askPlayAgain(){
        System.out.println("Do you want to play again? (y/n)");
    }

    public static void askQuitConfirmation(){
        System.out.println("Are you sure you want to quit the game? (y/n)");
    }

    public static void askAttempts(){
        System.out.println("Enter number of attempts");
    }

    public static void wrongAttemptNumber(){
        System.out.println("Please enter an integer number in range from 1 to 15");
    }

    public static void askMinValue(){
        System.out.println("Enter minimal value");
    }

    public static void askMaxValue(){
        System.out.println("Enter maximal value");
    }

    public static void wrongRangeValue(){
        System.out.println("Please enter an integer number in range from 1 to 200");
    }

    public static void wrongMinMax(){
        System.out.println("Maximum must not be lesser then minimum");
    }
}
