package com.github.guessnumber;

import java.util.Scanner;

public class Reader {

    public static int readAttempts(){
        Messages.askAttempts();
        Scanner Read = new Scanner(System.in);
        String Buffer;
        int Attempts;
        while(true){
            try{
                Buffer = Read.nextLine();
                Buffer = Buffer.toLowerCase();
                if(Buffer.equals(Exit.exitCommand())){
                    if(Exit.bExit()){
                        return Game.EXIT_KEY();
                    }
                }
                Attempts = Integer.parseInt(Buffer);
                if(!Validation.isValidAttempts(Attempts))
                    Messages.wrongAttemptNumber();
                else
                    return Attempts;
            }
            catch(NumberFormatException nfe){
                Messages.wrongAttemptNumber();
            }
        }
    }
    public static int readMin(){
        Messages.askMinValue();
        Scanner Read = new Scanner(System.in);
        String Buffer;
        int Min;
        while(true){
            try{
                Buffer = Read.nextLine();
                Buffer = Buffer.toLowerCase();
                if(Buffer.equals(Exit.exitCommand())){
                    if(Exit.bExit()){
                        return Game.EXIT_KEY();
                    }
                }
                Min = Integer.parseInt(Buffer);
                if(!Validation.isValidRange(Min))
                    Messages.wrongRangeValue();
                else
                    return Min;
            }
            catch(NumberFormatException nfe){
                Messages.wrongRangeValue();
            }
        }
    }

    public static int readMax(int _min){
        Messages.askMaxValue();
        Scanner Read = new Scanner(System.in);
        String Buffer;
        int Max;
        while(true){
            try{
                Buffer = Read.nextLine();
                Buffer = Buffer.toLowerCase();
                if(Buffer.equals(Exit.exitCommand())){
                    if(Exit.bExit()){
                        return Game.EXIT_KEY();
                    }
                }
                Max = Integer.parseInt(Buffer);
                if(!Validation.isValidRange(Max))
                    Messages.wrongRangeValue();
                else if(Max < _min)
                    Messages.wrongMinMax();
                else
                    return Max;
            }
            catch(NumberFormatException nfe){
                Messages.wrongRangeValue();
            }
        }
    }
}
