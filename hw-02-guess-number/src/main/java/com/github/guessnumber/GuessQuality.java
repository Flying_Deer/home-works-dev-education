package com.github.guessnumber;

public class GuessQuality {

    public static boolean isBetterGuess(int Guess, int PreviousGuess, int Number){
        return Math.abs(Guess - Number) < Math.abs(PreviousGuess - Number);
    }

    public static boolean isWorseGuess(int Guess, int PreviousGuess, int Number){
        return Math.abs(Guess - Number) > Math.abs(PreviousGuess - Number);
    }


}
