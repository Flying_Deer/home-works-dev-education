package com.github.guessnumber;

import java.util.Scanner;


public class Game {

    final private static int EXIT_KEY = -7777;
    public static int EXIT_KEY() { return EXIT_KEY; }

    private static boolean isQuiting = false;
    public static boolean isQuited() { return isQuiting; }

    final private static int initMin = 1;
    final private static int initMax = 100;
    final private static int initAttempts = 5;

    private static int Min;
    private static int Max;
    private static int Attempts;
    private static int Number;

    public static int getMin(){
        return Min;
    }
    public static  void setMin(int _min){ Min = _min;}

    public static int getMax(){
        return Max;
    }
    public static void setMax(int _max) { Max = _max; }

    public static int getAttempts(){
        return Attempts;
    }
    public static void setAttempts(int _attempts){ Attempts = _attempts; }

    public static int getNumber(){
        return Number;
    }
    public static void setNumber(int _min, int _max){
        Number = (int)(Math.random() * (_max - _min)) + _min;
    }

    Game(){
        Min = initMin;
        Max = initMax;
        Attempts = initAttempts;
        if(bSetParameters()){
            Min = Reader.readMin();
            if(Min != EXIT_KEY()) {
                Max = Reader.readMax(Min);
                if(Max != EXIT_KEY()) {
                    Attempts = Reader.readAttempts();
                }
            }
        }
        if(Min != EXIT_KEY() && Max != EXIT_KEY() && Attempts != EXIT_KEY()) {
            setNumber(Min, Max);
            playGame();
        }
        else {
            isQuiting = true;
        }
    }

    public static void playGame(){
        Messages.start(Min, Max, Attempts);
        Scanner Read = new Scanner(System.in);
        String Buffer;
        int CurrentGuess = 0, PreviousGuess = 0, AttemptUsed = 0;
        while(Attempts > 0){
            try{
                Messages.askGuess();
                Buffer = Read.nextLine();
                Buffer = Buffer.toLowerCase();
                if(Buffer.equals(Exit.exitCommand())){
                    isQuiting = Exit.bExit();
                    if(isQuiting){
                        break;
                    }
                }
                CurrentGuess = Integer.parseInt(Buffer);
            }
            catch(NumberFormatException nfe){
                Messages.wrongInput();
                continue;
            }
            AttemptUsed++;
            if (CurrentGuess == Number) {
                Messages.winGame(AttemptUsed);
                return;
            } else if (AttemptUsed == 1) {
                Attempts--;
                if (Attempts == 0)
                    return;
                Messages.firstGuess();
            } else {
                Attempts--;
                if (Attempts == 0)
                    return;
                if (GuessQuality.isBetterGuess(CurrentGuess, PreviousGuess, Number))
                    Messages.betterGuess();
                else if (GuessQuality.isWorseGuess(CurrentGuess, PreviousGuess, Number))
                    Messages.worseGuess();
                else
                    Messages.sameGuess();
            }
            PreviousGuess = CurrentGuess;
        }
        if(!isQuiting) {
            Messages.loseGame();
        }
    }


    public static boolean bSetParameters(){
        Messages.askSettings();
        Scanner Read = new Scanner(System.in);
        String Buffer = Read.nextLine();
        Buffer = Buffer.toLowerCase();
        if(Buffer.equals(ConfirmConstants.Yes()))
            return true;
        else if(Buffer.equals(ConfirmConstants.No()))
            return false;
        else
            return bSetParameters();
    }

    public static boolean bPlayAgain(){
        Messages.askPlayAgain();
        Scanner Read = new Scanner(System.in);
        String Buffer = Read.nextLine();
        Buffer = Buffer.toLowerCase();
        if(Buffer.equals(ConfirmConstants.Yes()))
            return true;
        else if(Buffer.equals(ConfirmConstants.No()))
            return false;
        else
            return bPlayAgain();
    }
}
