package com.github.guessnumber;

public class Validation {
    public static boolean isValidAttempts(int Attempts){
        return Attempts > 0 && Attempts <= 15;
    }

    public static boolean isValidRange(int Range){
        return Range > 0 && Range <= 200;
    }
}
