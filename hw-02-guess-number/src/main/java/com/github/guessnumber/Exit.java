package com.github.guessnumber;

import java.util.Scanner;

public class Exit {

    final private static String  ExitCommand = "exit";

    public static String exitCommand() {
        return ExitCommand;
    }

    public static boolean bExit(){
        Scanner Read = new Scanner(System.in);
        String Buffer;
        while(true){
            Messages.askQuitConfirmation();
            Buffer = Read.nextLine();
            Buffer = Buffer.toLowerCase();
            if(Buffer.equals(ConfirmConstants.Yes()))
                return true;
            else if(Buffer.equals(ConfirmConstants.No()))
                return false;
        }
    }

}
