package com.github.converter;

import com.github.converter.modes.*;
import com.github.converter.modes.modeEnums.EMode;

import javax.swing.*;

public class ModeManager {

    private static final String[] convertorModes = {"Length", "Temperature", "Time", "Volume", "Weight"};

    private static EMode currentMode = EMode.length;

    public static EMode getCurrentMode() {
        return currentMode;
    }

    public static String[] getConvertorModes() {
        return convertorModes;
    }

    public static EMode defineMode(String currentMode){
        switch (currentMode){
            case "Length":
                return EMode.length;
            case "Temperature":
                return EMode.temperature;
            case "Weight":
                return EMode.weight;
            case "Time":
                return EMode.time;
            case "Volume":
                return EMode.volume;
            default:
                throw new RuntimeException();
        }
    }

    private static void addMode(EMode mode, JComboBox<String> firstCombo, JComboBox<String> secondCombo){
        switch (mode){
            case length:{
                for (String unitName : LengthMode.getLengthUnits()){
                    firstCombo.addItem(unitName);
                    secondCombo.addItem(unitName);
                }
                break;
            }
            case temperature:{
                for (String unitName : TemperatureMode.getTemperatureUnits()){
                    firstCombo.addItem(unitName);
                    secondCombo.addItem(unitName);
                }
                break;
            }
            case time:{
                for (String unitName : TimeMode.getTimeUnits()){
                    firstCombo.addItem(unitName);
                    secondCombo.addItem(unitName);
                }
                break;
            }
            case volume:{
                for (String unitName : VolumeMode.getVolumeUnits()){
                    firstCombo.addItem(unitName);
                    secondCombo.addItem(unitName);
                }
                break;
            }
            case weight:{
                for (String unitName : WeightMode.getWeightUnits()){
                    firstCombo.addItem(unitName);
                    secondCombo.addItem(unitName);
                }
                break;
            }
            default:
                break;
        }
    }

    public static void changeMode(EMode mode, Interface convertorInterface){

        convertorInterface.firstUnitCombo.removeAllItems();
        convertorInterface.secondUnitCombo.removeAllItems();

        addMode(mode, convertorInterface.firstUnitCombo, convertorInterface.secondUnitCombo);

        currentMode = mode;
    }
}
