package com.github.converter;

public class Converter {

    Converter() {

        Interface convertorInterface = new Interface();

        convertorInterface.modeCombo.addActionListener( e-> {
            String sMode = (String) convertorInterface.modeCombo.getSelectedItem();
            assert sMode != null;
            ModeManager.changeMode(ModeManager.defineMode(sMode), convertorInterface);
        });

        convertorInterface.firstUnitField.addActionListener(e -> {
            String firstUnit = (String) convertorInterface.firstUnitCombo.getSelectedItem();
            String secondUnit = (String) convertorInterface.secondUnitCombo.getSelectedItem();
            double firstValue;
            double secondValue;

            try{
                firstValue = Double.parseDouble(convertorInterface.firstUnitField.getText());
                secondValue = ConverterExecutor.convert(firstValue, firstUnit, secondUnit);
                convertorInterface.secondUnitField.setText(String.format("%.3e",secondValue));
            }
            catch (Exception exception){
                convertorInterface.secondUnitField.setText("NaN");
            }

        });

        convertorInterface.secondUnitField.addActionListener(e -> {
            String firstUnit = (String) convertorInterface.firstUnitCombo.getSelectedItem();
            String secondUnit = (String) convertorInterface.secondUnitCombo.getSelectedItem();
            double firstValue;
            double secondValue;

            try{
                secondValue = Double.parseDouble(convertorInterface.secondUnitField.getText());
                firstValue = ConverterExecutor.convert(secondValue, secondUnit, firstUnit);
                convertorInterface.firstUnitField.setText(String.format("%.3e",firstValue));
            }
            catch (Exception exception){
                convertorInterface.firstUnitField.setText("NaN");
            }

        });


    }

}
