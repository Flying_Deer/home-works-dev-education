package com.github.converter.modes.modeEnums;

public enum EVolume {
    liter,
    cubicMeter,
    gallon,
    pint,
    quart,
    barrel,
    cubicFeet,
    cubicInch
}
