package com.github.converter.modes;

import com.github.converter.modes.modeEnums.EVolume;
import com.github.converter.modes.modeEnums.EWeight;

public class WeightMode {

    private static final String[] weightUnits = {"Kilogram", "Gram", "Carat",
            "Eng pound", "Pound", "Stone", "Rus pound"};

    public static String[] getWeightUnits() {
        return weightUnits;
    }

    public static EWeight defineUnit(String unit){
        switch (unit){
            case "Kilogram":
                return EWeight.kilogram;
            case "Gram":
                return EWeight.gram;
            case "Carat":
                return EWeight.carat;
            case "Eng pound":
                return EWeight.engPound;
            case "Pound":
                return EWeight.pound;
            case "Stone":
                return EWeight.stone;
            case "Rus pound":
                return EWeight.rusPound;
            default:
                throw new IllegalArgumentException();
        }
    }

    public static final double gramToKilogram = 0.001;
    public static final double caratToKilogram = 0.0002;
    public static final double engPoundToKilogram = 0.454;
    public static final double poundToKilogram = 0.453592;
    public static final double stoneToKilogram = 6.35029;
    public static final double rusPoundToKilogram = 0.40951241;
}

