package com.github.converter.modes.modeEnums;

public enum ELength {
    meter,
    kilometer,
    mile,
    nauticalMile,
    cable,
    league
}
