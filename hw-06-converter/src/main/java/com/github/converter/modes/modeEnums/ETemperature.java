package com.github.converter.modes.modeEnums;

public enum ETemperature {
    c,
    f,
    re,
    ro,
    ra,
    n,
    d
}
