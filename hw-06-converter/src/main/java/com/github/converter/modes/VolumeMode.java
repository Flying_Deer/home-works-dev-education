package com.github.converter.modes;

import com.github.converter.modes.modeEnums.ETime;
import com.github.converter.modes.modeEnums.EVolume;

public class VolumeMode {

    private static final String[] volumeUnits = {"Liter", "Cubic meter", "Gallon", "Pint",
            "Quart", "Barrel", "Cubic feet", "Cubic inch"};

    public static String[] getVolumeUnits() {
        return volumeUnits;
    }

    public static EVolume defineUnit(String unit){
        switch (unit){
            case "Liter":
                return EVolume.liter;
            case "Cubic meter":
                return EVolume.cubicMeter;
            case "Gallon":
                return EVolume.gallon;
            case "Pint":
                return EVolume.pint;
            case "Quart":
                return EVolume.quart;
            case "Barrel":
                return EVolume.barrel;
            case "Cubic feet":
                return EVolume.cubicFeet;
            case "Cubic inch":
                return EVolume.cubicInch;
            default:
                throw new IllegalArgumentException();
        }
    }

    public static final double cubicMeterToLiter = 1000.d;
    public static final double gallonToLiter = 3.78541;
    public static final double pintToLiter = 0.473176;
    public static final double quartToLiter = 0.946353;
    public static final double barrelToLiter = 158.987;
    public static final double cubicFeetToLiter = 28.3168;
    public static final double cubicInchToLiter = 0.0163871;
}
