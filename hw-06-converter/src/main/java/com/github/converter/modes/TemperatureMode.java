package com.github.converter.modes;

import com.github.converter.modes.modeEnums.ETemperature;

public class TemperatureMode {
    private static final String[] temperatureUnits = {"C", "F", "Re",
            "Ro", "Ra", "N", "D"};

    public static String[] getTemperatureUnits() {
        return temperatureUnits;
    }

    public static double toCelsius(double value, String unit){
        switch (unit){
            case "C":
                return value;
            case "F":
                return (value - 32.d) * 5.d/9.d;
            case "Re":
                return 1.25 * value;
            case "Ro":
                return (value - 7.5) / 0.525;
            case "Ra":
                return (value - 491.67) * 5.d / 9.d;
            case "N":
                return value/0.33;
            case "D":
                return (value + 100) / 1.5;
            default:
                throw new IllegalArgumentException();
        }
    }

    public static double fromCelsius(double value, String unit){
        switch (unit){
            case "C":
                return value;
            case "F":
                return value * 9.d / 5.d + 32;
            case "Re":
                return value * 0.8;
            case "Ro":
                return value * 0.525 + 7.5;
            case "Ra":
                return value * 9.d / 5.d + 791.67;
            case "N":
                return value * 3;
            case "D":
                return value * 1.5 - 100;
            default:
                throw new IllegalArgumentException();
        }
    }
}
