package com.github.converter.modes;

import com.github.converter.modes.modeEnums.ETime;

public class TimeMode {

    private static final String[] timeUnits = {"Sec", "Min", "Day",
            "Week", "Month", "Astronomical year"};

    public static String[] getTimeUnits() {
        return timeUnits;
    }

    public static ETime defineUnit(String unit){
        switch (unit){
            case "Sec":
                return ETime.sec;
            case "Min":
                return ETime.min;
            case "Day":
                return ETime.day;
            case "Week":
                return ETime.week;
            case "Month":
                return ETime.month;
            case "Astronomical year":
                return ETime.astronomicalYear;
            default:
                throw new IllegalArgumentException();
        }
    }

    public static final double minToSec = 60.d;
    public static final double dayToSec = 86400.d;
    public static final double weekToSec = 604800.d;
    public static final double monthToSec = 2419200.d;
    public static final double astronomicalYearToSec = 31536000.d;
}
