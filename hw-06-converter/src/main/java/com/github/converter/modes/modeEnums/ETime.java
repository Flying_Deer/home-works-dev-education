package com.github.converter.modes.modeEnums;

public enum ETime {
    sec,
    min,
    day,
    week,
    month,
    astronomicalYear
}
