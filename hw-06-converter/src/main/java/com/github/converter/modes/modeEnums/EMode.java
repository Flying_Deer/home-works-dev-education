package com.github.converter.modes.modeEnums;

public enum EMode {
        length,
        temperature,
        weight,
        time,
        volume
}
