package com.github.converter.modes;

import com.github.converter.modes.modeEnums.ELength;

public class LengthMode {

    private static final String[] lengthUnits = {"Meter", "Kilometer", "Mile",
                                                    "Nautical mile", "Cable", "League"};

    public static String[] getLengthUnits() {
        return lengthUnits;
    }

    public static ELength defineUnit(String unit){
        switch (unit){
            case "Meter":
                return ELength.meter;
            case "Kilometer":
                return ELength.kilometer;
            case "Mile":
                return ELength.mile;
            case "Nautical mile":
                return ELength.nauticalMile;
            case "Cable":
                return ELength.cable;
            case "League":
                return ELength.league;
            default:
                throw new IllegalArgumentException();
        }
    }

    public static final double kilometerToMeter = 1000.d;
    public static final double mileToMeter = 1609.34;
    public static final double nauticalMileToMeter = 1852.d;
    public static final double cableToMeter = 182.880;
    public static final double leagueToMeter = 4828.032;
}
