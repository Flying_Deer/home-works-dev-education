package com.github.converter.modes.modeEnums;

public enum EWeight {
    kilogram,
    gram,
    carat,
    engPound,
    pound,
    stone,
    rusPound
}
