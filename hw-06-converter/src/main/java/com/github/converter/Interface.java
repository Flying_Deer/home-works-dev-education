package com.github.converter;

import com.github.converter.modes.LengthMode;

import javax.swing.*;

public class Interface {

    public JFrame convertorFrame;

    public JComboBox<String> modeCombo = new JComboBox<>(ModeManager.getConvertorModes());

    public JComboBox<String> firstUnitCombo = new JComboBox<>(LengthMode.getLengthUnits());
    public JComboBox<String> secondUnitCombo = new JComboBox<>(LengthMode.getLengthUnits());

    public JTextField firstUnitField = new JTextField();
    public JTextField secondUnitField = new JTextField();

    Interface(){

        convertorFrame = new JFrame();

        final int frameWidth = 300;
        final int frameHeight = 300;
        final int frameX = 600;
        final int frameY = 200;

        JLabel modeComboLabel = new JLabel("Category", SwingConstants.RIGHT);
        modeComboLabel.setBounds(20, 20, 70, 20);

        final int modeComboWidth = 150;
        final int modeComboHeight = 20;
        final int modeComboX = 100;
        final int modeComboY = 20;

        modeCombo.setBounds(modeComboX, modeComboY, modeComboWidth, modeComboHeight);


        JLabel firstUnitLabel = new JLabel("First unit", SwingConstants.RIGHT);
        JLabel secondUnitLabel = new JLabel("Second unit", SwingConstants.RIGHT);

        firstUnitLabel.setBounds(30, 80, 70, 20);
        secondUnitLabel.setBounds(170, 80, 70, 20);


        firstUnitCombo.setBounds(25, 110, 110, 20);
        firstUnitField.setBounds(25, 140, 110, 20);
        secondUnitCombo.setBounds(155, 110, 110, 20);
        secondUnitField.setBounds(155, 140, 110, 20);

        convertorFrame.add(modeComboLabel);
        convertorFrame.add(modeCombo);
        convertorFrame.add(firstUnitLabel);
        convertorFrame.add(secondUnitLabel);
        convertorFrame.add(firstUnitCombo);
        convertorFrame.add(firstUnitField);
        convertorFrame.add(secondUnitCombo);
        convertorFrame.add(secondUnitField);

        convertorFrame.setSize(frameWidth, frameHeight);
        convertorFrame.setLocation(frameX, frameY);
        convertorFrame.setLayout(null);
        convertorFrame.setVisible(true);
    }

}
