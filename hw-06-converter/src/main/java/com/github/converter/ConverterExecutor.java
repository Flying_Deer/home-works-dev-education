package com.github.converter;

import com.github.converter.modes.*;
import com.github.converter.modes.modeEnums.*;

public class ConverterExecutor {
    public static double convert(double value, String firstUnit, String secondUnit){
        switch (ModeManager.getCurrentMode()){
            case length:{
                return convertLength(value,LengthMode.defineUnit(firstUnit), LengthMode.defineUnit(secondUnit));
            }
            case temperature:{
                return convertTemperature(value,firstUnit,secondUnit);
            }
            case time:{
                return convertTime(value, TimeMode.defineUnit(firstUnit), TimeMode.defineUnit(secondUnit));
            }
            case volume:{
                return convertVolume(value, VolumeMode.defineUnit(firstUnit), VolumeMode.defineUnit(secondUnit));
            }
            case weight:{
                return convertWeight(value, WeightMode.defineUnit(firstUnit), WeightMode.defineUnit(secondUnit));
            }
        }
        throw new IllegalArgumentException();
    }

    private static double convertLength(double value, ELength firstUnit, ELength secondUnit){
        switch (firstUnit){
            case meter:
                break;
            case kilometer:
                value *= LengthMode.kilometerToMeter;
                break;
            case mile:
                value *= LengthMode.mileToMeter;
                break;
            case nauticalMile:
                value *= LengthMode.nauticalMileToMeter;
                break;
            case cable:
                value *= LengthMode.cableToMeter;
                break;
            case league:
                value *= LengthMode.leagueToMeter;
                break;
        }

        switch (secondUnit){
            case meter:
                break;
            case kilometer:
                value /= LengthMode.kilometerToMeter;
                break;
            case mile:
                value /= LengthMode.mileToMeter;
                break;
            case nauticalMile:
                value /= LengthMode.nauticalMileToMeter;
                break;
            case cable:
                value /= LengthMode.cableToMeter;
                break;
            case league:
                value /= LengthMode.leagueToMeter;
                break;
        }

        return value;
    }

    private static double convertTemperature(double value, String firstUnit, String secondUnit){
        return TemperatureMode.fromCelsius(TemperatureMode.toCelsius(value, firstUnit),secondUnit);
    }

    private static double convertTime(double value, ETime firstUnit, ETime secondUnit){
        switch (firstUnit){
            case sec:
                break;
            case min:
                value *= TimeMode.minToSec;
                break;
            case day:
                value *= TimeMode.dayToSec;
                break;
            case week:
                value *= TimeMode.weekToSec;
                break;
            case month:
                value *= TimeMode.monthToSec;
                break;
            case astronomicalYear:
                value *= TimeMode.astronomicalYearToSec;
        }
        switch (secondUnit){
            case sec:
                break;
            case min:
                value /= TimeMode.minToSec;
                break;
            case day:
                value /= TimeMode.dayToSec;
                break;
            case week:
                value /= TimeMode.weekToSec;
                break;
            case month:
                value /= TimeMode.monthToSec;
                break;
            case astronomicalYear:
                value /= TimeMode.astronomicalYearToSec;
        }
        return value;
    }

    private static double convertVolume(double value, EVolume firstUnit, EVolume secondUnit){
        switch (firstUnit){
            case liter:
                break;
            case cubicMeter:
                value *= VolumeMode.cubicMeterToLiter;
                break;
            case gallon:
                value *= VolumeMode.gallonToLiter;
                break;
            case pint:
                value *= VolumeMode.pintToLiter;
                break;
            case quart:
                value *= VolumeMode.quartToLiter;
                break;
            case barrel:
                value *= VolumeMode.barrelToLiter;
                break;
            case cubicFeet:
                value *= VolumeMode.cubicFeetToLiter;
                break;
            case cubicInch:
                value *= VolumeMode.cubicInchToLiter;
                break;
        }
        switch (secondUnit){
            case liter:
                break;
            case cubicMeter:
                value /= VolumeMode.cubicMeterToLiter;
                break;
            case gallon:
                value /= VolumeMode.gallonToLiter;
                break;
            case pint:
                value /= VolumeMode.pintToLiter;
                break;
            case quart:
                value /= VolumeMode.quartToLiter;
                break;
            case barrel:
                value /= VolumeMode.barrelToLiter;
                break;
            case cubicFeet:
                value /= VolumeMode.cubicFeetToLiter;
                break;
            case cubicInch:
                value /= VolumeMode.cubicInchToLiter;
                break;
        }

        return value;
    }

    private static double convertWeight(double value, EWeight firstUnit, EWeight secondUnit){
        switch (firstUnit){
            case kilogram:
                break;
            case gram:
                value *= WeightMode.gramToKilogram;
                break;
            case carat:
                value *= WeightMode.caratToKilogram;
                break;
            case engPound:
                value *= WeightMode.engPoundToKilogram;
                break;
            case pound:
                value *= WeightMode.poundToKilogram;
                break;
            case stone:
                value *= WeightMode.stoneToKilogram;
                break;
            case rusPound:
                value *= WeightMode.rusPoundToKilogram;
                break;
        }

        switch (secondUnit){
            case kilogram:
                break;
            case gram:
                value /= WeightMode.gramToKilogram;
                break;
            case carat:
                value /= WeightMode.caratToKilogram;
                break;
            case engPound:
                value /= WeightMode.engPoundToKilogram;
                break;
            case pound:
                value /= WeightMode.poundToKilogram;
                break;
            case stone:
                value /= WeightMode.stoneToKilogram;
                break;
            case rusPound:
                value /= WeightMode.rusPoundToKilogram;
                break;
        }

        return value;
    }
}
