package org.bitbucket.interacting.balls;

import org.bitbucket.interacting.balls.interaction.InteractionType;
import org.bitbucket.interacting.balls.utils.BallManager;

import javax.swing.*;
import java.awt.*;

public class BallPanel extends JPanel {

    public BallPanel(JFrame parent) {

        BallManager ballManager = new BallManager(this);
        ballManager.setInteraction(InteractionType.MERGE);

        this.setLayout(null);
        this.setVisible(true);
        this.setLocation(5, 5);
        this.setSize(parent.getWidth() - 23, parent.getHeight() - 45);
        this.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        this.addMouseListener(new BallCreator(ballManager, Math.min(this.getWidth(), this.getHeight()) / 16, Math.min(this.getWidth(), this.getHeight()) * 100));

    }
}
