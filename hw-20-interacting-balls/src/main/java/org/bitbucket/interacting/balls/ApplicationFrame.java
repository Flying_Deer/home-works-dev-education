package org.bitbucket.interacting.balls;

import javax.swing.*;

public class ApplicationFrame extends JFrame {

    public ApplicationFrame(){

        this.setLayout(null);
        this.setVisible(true);
        this.setBounds(5, 5, 1280, 800);

        JPanel ballsPanel = new BallPanel(this);

        this.add(ballsPanel);

        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

    }

}
