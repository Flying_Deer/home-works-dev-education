package org.bitbucket.interacting.balls.interaction;

import org.bitbucket.interacting.balls.Ball;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class Interaction implements Runnable{

    private final InteractionType interactionType;

    protected final List<Ball> balls;

    protected final AtomicInteger counter;

    protected Interaction(InteractionType interactionType, List<Ball> balls, AtomicInteger counter) {
        this.interactionType = interactionType;
        this.balls = balls;
        this.counter = counter;
    }

    public InteractionType getInteractionType() {
        return interactionType;
    }

}
