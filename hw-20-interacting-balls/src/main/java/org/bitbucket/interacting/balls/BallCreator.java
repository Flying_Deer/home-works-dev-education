package org.bitbucket.interacting.balls;

import org.bitbucket.interacting.balls.utils.BallManager;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class BallCreator implements MouseListener {

    private final BallManager ballManager;

    private int maxRadius;

    private int maxVelocity;

    public BallCreator(BallManager ballManager, int maxRadius, int maxVelocity) {
        this.ballManager = ballManager;
        this.maxRadius = maxRadius;
        this.maxVelocity = maxVelocity;
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {
        Ball ball = Ball.getRandomBall(e.getPoint(), this.maxRadius, this.maxVelocity);
        this.ballManager.addBall(ball);
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

}
