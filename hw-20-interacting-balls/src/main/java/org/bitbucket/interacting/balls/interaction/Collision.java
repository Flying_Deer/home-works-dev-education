package org.bitbucket.interacting.balls.interaction;

import org.bitbucket.interacting.balls.Ball;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public class Collision extends Interaction {

    public Collision(List<Ball> balls, AtomicInteger counter) {
        super(InteractionType.COLLIDE,balls, counter);
    }

    @Override
    public void run() {
        while (true) {
            try {
                for (int i = 0; i < this.balls.size(); i++) {
                    Ball firstBall = this.balls.get(i);
                    for (int j = i + 1; j < this.balls.size(); j++) {
                        Ball secondBall = this.balls.get(j);
                        if (firstBall.isColliding(secondBall)) {
                            collide(firstBall, secondBall);
                        }
                    }
                }
                Thread.sleep(10);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    private double sin(Ball first, Ball second){
        return (second.getCenterX() - first.getCenterX()) / Math.sqrt(Math.pow(first.getCenterX() - second.getCenterX(), 2) + Math.pow(first.getCenterY() - second.getCenterY(), 2));
    }

    private double cos(Ball first, Ball second){
        return (second.getCenterY() - first.getCenterY()) / Math.sqrt(Math.pow(first.getCenterX() - second.getCenterX(), 2) + Math.pow(first.getCenterY() - second.getCenterY(), 2));
    }

    private void pushApart(Ball first, Ball second) {
        if ((Math.sqrt(
                Math.pow(first.getCenterX() - second.getCenterX(), 2) + Math.pow(first.getCenterY() - second.getCenterY(), 2)) < first.getRadius() + second.getRadius()
                && !Objects.deepEquals(first, second))) {

            double sin = sin(first, second);
            double cos = cos(first, second);

            double collisionX = first.getCenterX() + first.getRadius() * sin;
            double collisionY = first.getCenterY() + first.getRadius() * cos;

            second.setCenterX((int) (collisionX + (second.getRadius() + 1) * sin));
            second.setCenterY((int) (collisionY + (second.getRadius() + 1) * cos));
            second.setLocation(second.getCenterX() - second.getRadius(), second.getCenterY() - second.getRadius());
        }
    }

    public void collide(Ball first, Ball second) {

        pushApart(first, second);

        double sin = (second.getCenterX() - first.getCenterX()) / Math.sqrt(Math.pow(first.getCenterX() - second.getCenterX(), 2) + Math.pow(first.getCenterY() - second.getCenterY(), 2));
        double cos = (second.getCenterY() - first.getCenterY()) / Math.sqrt(Math.pow(first.getCenterX() - second.getCenterX(), 2) + Math.pow(first.getCenterY() - second.getCenterY(), 2));

        double V1X = cos * first.getVx() - sin * first.getVy();
        double V1Y = sin * first.getVx() + cos * first.getVy();
        double V2X = cos * second.getVx() - sin * second.getVy();
        double V2Y = sin * second.getVx() + cos * second.getVy();

        double newV1Y = ((first.getWeight() - second.getWeight()) * V1Y + 2 * second.getWeight() * V2Y) / (first.getWeight() + second.getWeight());
        double newV2Y = ((second.getWeight() - first.getWeight()) * V2Y + 2 * first.getWeight() * V1Y) / (first.getWeight() + second.getWeight());

        first.setVx((int) (cos * V1X + sin * newV1Y));
        first.setVy((int) ( -sin * V1X + cos * newV1Y));
        second.setVx((int) (cos * V2X + sin * newV2Y));
        second.setVy((int) ( -sin * V2X + cos * newV2Y));

    }
}