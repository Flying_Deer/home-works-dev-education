package org.bitbucket.interacting.balls.interaction.factory;

import org.bitbucket.interacting.balls.Ball;
import org.bitbucket.interacting.balls.interaction.Collision;
import org.bitbucket.interacting.balls.interaction.Interaction;
import org.bitbucket.interacting.balls.interaction.InteractionType;
import org.bitbucket.interacting.balls.interaction.Merge;
import org.bitbucket.interacting.balls.utils.BallManager;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class InteractionFactory {

    public static Interaction getInteraction(InteractionType interactionType, List<Ball> balls, AtomicInteger counter){
        switch (interactionType){
            case MERGE:
                return new Merge(balls, counter);
            case COLLIDE:
                return new Collision(balls, counter);
            default:
                throw new IllegalArgumentException();
        }
    }

}
