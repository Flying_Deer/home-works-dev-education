package org.bitbucket.interacting.balls;

import javax.swing.*;
import java.awt.*;
import java.util.Objects;
import java.util.Random;

public class Ball extends JPanel implements Runnable{

    private int centerX;

    private int centerY;

    private final int radius;

    private final Color color;

    private int vx;

    private int vy;

    private final int weight;

    private boolean isActive = true;

    public Ball(Point point, int radius, Color color, int vx, int vy) {

        this.centerX = point.x;
        this.centerY = point.y;
        this.radius = radius;
        this.weight = this.radius * this.radius;
        this.color = color;
        this.vx = vx;
        this.vy = vy;

        this.isActive = true;

        this.setLocation(this.centerX - this.radius, this.centerY - this.radius);
        this.setSize(this.radius << 1, this.radius << 1);

        this.setVisible(true);
        this.setLayout(null);
        this.setOpaque(false);

    }

    public int getCenterX() {
        return centerX;
    }

    public void setCenterX(int centerX) {
        this.centerX = centerX;
    }

    public int getCenterY() {
        return centerY;
    }

    public void setCenterY(int centerY) {
        this.centerY = centerY;
    }

    public int getRadius() {
        return radius;
    }

    public int getVx() {
        return vx;
    }

    public void setVx(int vx) {
        this.vx = vx;
    }

    public int getVy() {
        return vy;
    }

    public void setVy(int vy) {
        this.vy = vy;
    }

    public int getWeight() {
        return weight;
    }

    public static Ball getRandomBall(Point point, int maxRadius, int maxVelocity){
        Random random = new Random();
        return new Ball(point,
                random.nextInt(maxRadius),
                new Color(
                        random.nextInt(255),
                        random.nextInt(255),
                        random.nextInt(255)
                ),
                random.nextInt(maxVelocity) - (maxVelocity >> 1),
                random.nextInt(maxVelocity) - (maxVelocity >> 1)
        );
    }

    public static Ball getMergedBall(Ball first, Ball second){
        return new Ball(
                new Point(
                        (first.centerX * first.weight + second.centerX * second.weight) / (first.weight + second.weight),
                        (first.centerY * first.weight + second.centerY * second.weight) / (first.weight + second.weight)
                ),
                (int) Math.sqrt(first.radius * first.radius + second.radius * second.radius),
                new Color(
                        (first.color.getRed() * first.weight + second.color.getRed() * second.weight) / (first.weight + second.weight),
                        (first.color.getGreen() * first.weight + second.color.getGreen() * second.weight) / (first.weight + second.weight),
                        (first.color.getBlue() * first.weight + second.color.getBlue() * second.weight) / (first.weight + second.weight)
                ),
                (first.vx * first.weight+ second.vx * second.weight) / (first.weight + second.weight),
                (first.vy * first.weight + second.vy * second.weight) / (first.weight + second.weight)
        );
    }

    public boolean isColliding(Ball another){
        if(this.equals(another)){
            return false;
        }
        return (Math.sqrt(
                    Math.pow(this.getCenterX() - another.getCenterX(), 2)
                        + Math.pow(this.getCenterY() - another.getCenterY(), 2)
                ) < this.getRadius() + another.getRadius());
    }

    @Override
    public void paint(Graphics g){
        super.paint(g);
        Graphics2D graphics2D = (Graphics2D) g;
        graphics2D.setColor(this.color);
        graphics2D.fillOval(0, 0, radius << 1, radius << 1);
    }

    public void stop(){
        isActive = false;
    }

    private void move(){

        if(this.centerX - this.radius < 0){
            this.centerX = this.radius + 1;
        }

        if(this.centerX + this.radius > this.getParent().getWidth()){
            this.centerX = this.getParent().getWidth() - this.radius - 1;
        }

        if(this.centerY - this.radius < 0){
            this.centerY = this.radius + 1;
        }

        if(this.centerY + this.radius > this.getParent().getHeight()){
            this.centerY = this.getParent().getHeight() - this.radius - 1;
        }

        this.centerX += vx / 4000;
        this.centerY += vy / 4000;

        this.setLocation(this.centerX - this.radius, this.centerY - this.radius);

        if(this.centerX + this.radius > this.getParent().getWidth() || this.centerX - this.radius < 0){
            this.vx = -this.vx;
        }

        if(this.centerY + this.radius > this.getParent().getHeight() || this.centerY - this.radius < 0){
            this.vy = - this.vy;
        }

    }

    @Override
    public void run() {
        try {
            while (isActive) {
                this.move();
                Thread.sleep(25);
            }
        } catch (InterruptedException e){
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ball ball = (Ball) o;
        return centerX == ball.centerX && centerY == ball.centerY && radius == ball.radius && vx == ball.vx && vy == ball.vy && color.equals(ball.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(centerX, centerY, radius, color, vx, vy, weight, isActive);
    }

}
