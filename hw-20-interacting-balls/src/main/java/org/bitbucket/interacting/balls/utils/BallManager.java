package org.bitbucket.interacting.balls.utils;

import org.bitbucket.interacting.balls.Ball;
import org.bitbucket.interacting.balls.interaction.InteractionType;
import org.bitbucket.interacting.balls.interaction.factory.InteractionFactory;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public class BallManager {

    private final JPanel panel;

    private final AtomicInteger counter = new AtomicInteger(0);

    private final List<Ball> balls = new ArrayList<>();

    private InteractionType interactionType;

    private Thread interactionThread;

    public BallManager(JPanel panel) {
        this.panel = panel;
    }

    public List<Ball> getBalls() {
        return balls;
    }

    public void addBall(Ball ball) {
        this.panel.add(ball);
        Thread ballThread = new Thread(ball, "Ball-" + counter.incrementAndGet());
        this.balls.add(ball);
        ballThread.start();
    }

    public void setInteraction(InteractionType interactionType) {
        if (this.interactionThread != null) {
            if (this.interactionType != interactionType) {
                this.interactionThread.interrupt();
            }
        }
        this.interactionType = interactionType;
        this.interactionThread
                = new Thread(InteractionFactory.getInteraction(interactionType, this.balls, this.counter));
        this.interactionThread.start();
    }

}
