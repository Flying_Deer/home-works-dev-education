package org.bitbucket.interacting.balls.interaction;

public enum InteractionType {
    MERGE,
    COLLIDE
}
