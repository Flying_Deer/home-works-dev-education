package org.bitbucket.interacting.balls.interaction;

import org.bitbucket.interacting.balls.Ball;

import java.awt.*;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Merge extends Interaction {

    public Merge(List<Ball> balls, AtomicInteger counter) {
        super(InteractionType.MERGE, balls, counter);
    }

    public void removeBall(Ball ball) {
        ball.stop();
        balls.remove(ball);
        ball.setVisible(false);
    }

    public void addBall(Ball ball, Container parent) {
        parent.add(ball);
        Thread ballThread = new Thread(ball, "Ball-" + counter.incrementAndGet());
        this.balls.add(ball);
        ballThread.start();
    }

    @Override
    public void run() {
        while (true) {
            try {
                for (int i = 0; i < this.balls.size(); i++) {
                    Ball firstBall = this.balls.get(i);
                    for (int j = 0; j < this.balls.size(); j++) {
                        Ball secondBall = this.balls.get(j);
                        if (firstBall.isColliding(secondBall)) {
                            Ball newBall =  Ball.getMergedBall(firstBall, secondBall);
                            removeBall(firstBall);
                            removeBall(secondBall);
                            addBall(newBall, firstBall.getParent());
                            newBall.getParent().repaint();
                        }
                    }
                }
                Thread.sleep(25);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }
}