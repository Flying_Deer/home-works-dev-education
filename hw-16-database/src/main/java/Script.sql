SELECT COUNT(*) FROM Person;

SELECT avg(Age) FROM Person;

SELECT DISTINCT LastName FROM Person ORDER BY LastName;

SELECT LastName, COUNT(*) FROM Person GROUP BY LastName;

SELECT LastName FROM Person WHERE Person.LastName LIKE '_%b%_';

SELECT * FROM Person WHERE Id_Street IS NULL;

SELECT Person.* FROM Person JOIN Street ON Person.Id_Street = Street.Id
WHERE UPPER(Street.Name) LIKE '%Pravdy%' AND Person.age < 18;

SELECT Street.Id, Street.Name, COUNT(Person.Id) FROM Street JOIN Person ON Street.Id = Person.Id_Street
GROUP BY Street.Id, Street.Name;

SELECT * FROM Street WHERE LENGTH(Name) = 6;

SELECT Street.Id, Street.Name FROM Street JOIN Person ON Street.Id = Person.Id_Street
GROUP BY Street.Id, Street.Name
HAVING COUNT(Person.Id) < 3;