package org.bitbucket.http.servlet.handler;

import org.bitbucket.http.servlet.exceptions.EmptyRequestException;
import org.bitbucket.http.servlet.exceptions.IllegalMethodException;
import org.bitbucket.http.servlet.exceptions.ServerSocketException;
import org.bitbucket.http.servlet.request.ISimpleHttpRequest;
import org.bitbucket.http.servlet.request.SimpleHttpRequest;
import org.bitbucket.http.servlet.response.ISimpleHttpResponse;
import org.bitbucket.http.servlet.response.SimpleHttpResponse;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;

public class HttpHandler implements IHttpHandler {

    private boolean isRunning = true;

    private int port;

    private ServerSocket serverSocket;

    public HttpHandler(int port) {
        this.port = port;
    }

    private void executeMethod(ISimpleHttpRequest request, ISimpleHttpResponse response){
        switch (request.getMethod()){
            case "GET":
                this.doGet(request, response);
                break;
            case "HEAD":
                this.doHead(request, response);
                break;
            case "POST":
                this.doPost(request, response);
                break;
            case "PUT":
                this.doPut(request, response);
                break;
            case "DELETE":
                this.doDelete(request, response);
                break;
            case "CONNECT":
                this.doConnect(request, response);
                break;
            case "OPTIONS":
                this.doOptions(request, response);
                break;
            case "TRACE":
                this.doTrace(request, response);
                break;
            case "PATCH":
                this.doPatch(request, response);
                break;
            default:
                throw new IllegalMethodException("Method " + request.getMethod() + " is allowed.");
        }
    }

    @Override
    public void stopHandler(){
        this.isRunning = false;
    }

    @Override
    public void doGet(ISimpleHttpRequest request, ISimpleHttpResponse response) {
        String outString = "<h1>Get method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
        response.setBody(outString);
    }

    @Override
    public void doHead(ISimpleHttpRequest request, ISimpleHttpResponse response) {
        String outString = "<h1>Head method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
    }

    @Override
    public void doPost(ISimpleHttpRequest request, ISimpleHttpResponse response) {
        String outString = "<h1>Post method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
        response.setBody(outString);
    }

    @Override
    public void doPut(ISimpleHttpRequest request, ISimpleHttpResponse response) {
        String outString = "<h1>Put method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
        response.setBody(outString);
    }

    @Override
    public void doDelete(ISimpleHttpRequest request, ISimpleHttpResponse response) {
        String outString = "<h1>Delete method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
        response.setBody(outString);
    }

    @Override
    public void doConnect(ISimpleHttpRequest request, ISimpleHttpResponse response) {
        String outString = "<h1>Connect method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
        response.setBody(outString);
    }

    @Override
    public void doOptions(ISimpleHttpRequest request, ISimpleHttpResponse response) {
        String outString = "<h1>Options method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
        response.setBody(outString);
    }

    @Override
    public void doTrace(ISimpleHttpRequest request, ISimpleHttpResponse response) {
        String outString = "<h1>Trace method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
        response.setBody(outString);
    }

    @Override
    public void doPatch(ISimpleHttpRequest request, ISimpleHttpResponse response) {
        String outString = "<h1>Patch method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
        response.setBody(outString);
    }

    @Override
    public void run() {
        try {
            this.serverSocket = new ServerSocket(this.port);
            this.serverSocket.setSoTimeout(15000);
        } catch (IOException e){
            throw new ServerSocketException("Failed to open port " + port + ".");
        }
        while (this.isRunning){
            try {
                Socket socket = serverSocket.accept();
                ISimpleHttpRequest request = new SimpleHttpRequest();
                ISimpleHttpResponse response = new SimpleHttpResponse();
                request.takeRequest(socket);
                this.executeMethod(request, response);
                response.sendResponse(socket);
            } catch (IOException | EmptyRequestException | IllegalMethodException ignore){
            }

        }
    }

}
