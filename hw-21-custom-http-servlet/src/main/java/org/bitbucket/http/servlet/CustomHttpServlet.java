package org.bitbucket.http.servlet;

import org.bitbucket.http.servlet.handler.IHttpHandler;
import org.bitbucket.http.servlet.handler.HttpHandler;

import java.util.concurrent.TimeUnit;

public class CustomHttpServlet {

    public static void main(String[] args) {

        IHttpHandler socketHandler = new HttpHandler(8080);

        Thread socketHandlerThread = new Thread(socketHandler, "Socket-handler");
        socketHandlerThread.start();
        System.out.println(socketHandlerThread.getName() + " is running.");
        try {
            TimeUnit.SECONDS.sleep(10);
            socketHandler.stopHandler();
        } catch (InterruptedException e){
            socketHandler.stopHandler();
        }

    }

}
