package org.bitbucket.http.servlet.exceptions;

public class IllegalMethodException extends RuntimeException{

    public IllegalMethodException() {
    }

    public IllegalMethodException(String message) {
        super(message);
    }

}
