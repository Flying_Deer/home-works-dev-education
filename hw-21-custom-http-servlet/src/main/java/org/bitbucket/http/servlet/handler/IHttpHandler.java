package org.bitbucket.http.servlet.handler;

import org.bitbucket.http.servlet.request.ISimpleHttpRequest;
import org.bitbucket.http.servlet.response.ISimpleHttpResponse;

public interface IHttpHandler extends Runnable {

    void stopHandler();

    void doGet(ISimpleHttpRequest request, ISimpleHttpResponse response);

    void doHead(ISimpleHttpRequest request, ISimpleHttpResponse response);

    void doPost(ISimpleHttpRequest request, ISimpleHttpResponse response);

    void doPut(ISimpleHttpRequest request, ISimpleHttpResponse response);

    void doDelete(ISimpleHttpRequest request, ISimpleHttpResponse response);

    void doConnect(ISimpleHttpRequest request, ISimpleHttpResponse response);

    void doOptions(ISimpleHttpRequest request, ISimpleHttpResponse response);

    void doTrace(ISimpleHttpRequest request, ISimpleHttpResponse response);

    void doPatch(ISimpleHttpRequest request, ISimpleHttpResponse response);

}
