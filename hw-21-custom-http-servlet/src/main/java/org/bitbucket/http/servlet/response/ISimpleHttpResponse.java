package org.bitbucket.http.servlet.response;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Map;

public interface ISimpleHttpResponse {

    void setProtocol(String protocol);

    void setStatusCode(int statusCode);

    void setStatusText(String statusText);

    void setHeaders(Map<String, String> headers);

    void setBody(String body);

    void addHeader(String header, String value);

    void sendResponse(Socket socket) throws IOException;

}
