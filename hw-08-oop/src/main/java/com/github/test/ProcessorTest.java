package com.github.test;

import com.github.oop.Processor;
import com.github.oop.ProcessorArm;
import com.github.oop.ProcessorX86;
import org.junit.Assert;
import org.junit.Test;

public class ProcessorTest {

    private Processor processor;

    @Test
    public void dataProcessArmString(){
        processor = new ProcessorArm();
        String exp = "QWERTY";
        String act = processor.dataProcess("qwerty");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void dataProcessArmLong(){
        processor = new ProcessorArm();
        String exp = "123456789";
        String act = processor.dataProcess("123456789");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void dataProcessX86String(){
        processor = new ProcessorX86();
        String exp = "qwerty";
        String act = processor.dataProcess("qwerTy");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void dataProcessX86Long(){
        processor = new ProcessorX86();
        String exp = "123456789";
        String act = processor.dataProcess("123456789");
        Assert.assertEquals(exp, act);
    }
}
