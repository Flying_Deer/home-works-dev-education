package com.github.test;

import com.github.oop.Memory;
import com.sun.source.tree.AssertTree;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MemoryTest {

    private Memory memory;

    @Before
    public void setUp(){
        memory = new Memory(2);
    }

    @Test
    public void save(){
        Assert.assertTrue(memory.save("someData"));
    }

    @Test
    public void saveTooMany(){
        memory.save("someData");
        memory.save("someData");
        Assert.assertFalse(memory.save("someData"));
    }

    @Test
    public void saveNull(){
        Assert.assertFalse(memory.save(null));
    }

    @Test(expected = NullPointerException.class)
    public void readLastEmpty(){
        memory.readLast();
    }

    @Test
    public void readLastOne(){
        memory.save("someData");
        Assert.assertEquals("someData", memory.readLast());
    }

    @Test
    public void readLastTwo(){
        memory.save("someData1");
        memory.save("someData2");
        Assert.assertEquals("someData1", memory.readLast());
    }

    @Test(expected = NullPointerException.class)
    public void removeLast(){
        memory.removeLast();
    }

    @Test
    public void removeLastOne(){
        memory.save("someData1");
        Assert.assertEquals("someData1", memory.removeLast());
    }

    @Test
    public void removeLastTwo(){
        memory.save("someData1");
        memory.save("someData2");
        Assert.assertEquals("someData1", memory.removeLast());
    }
}
