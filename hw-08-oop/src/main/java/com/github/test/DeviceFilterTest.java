package com.github.test;

import com.github.oop.Device;
import com.github.oop.DeviceSet;
import com.github.oop.DevicesFilter;
import org.junit.Assert;
import org.junit.Test;

public class DeviceFilterTest {

    @Test
    public void getArms(){
        Device[] exp = new Device[5];
        exp[0] = DeviceSet.devices[3];
        exp[1] = DeviceSet.devices[4];
        exp[2] = DeviceSet.devices[5];
        exp[3] = DeviceSet.devices[6];
        exp[4] = DeviceSet.devices[8];
        Device[] act = DevicesFilter.getArms(DeviceSet.devices);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void getX86(){
        Device[] exp = new Device[5];
        exp[0] = DeviceSet.devices[0];
        exp[1] = DeviceSet.devices[1];
        exp[2] = DeviceSet.devices[2];
        exp[3] = DeviceSet.devices[7];
        exp[4] = DeviceSet.devices[9];
        Device[] act = DevicesFilter.getX86(DeviceSet.devices);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void byProcessorFrequency(){
        Device[] exp = new Device[2];
        exp[0] = DeviceSet.devices[3];
        exp[1] = DeviceSet.devices[8];
        Device[] act = DevicesFilter.byProcessorFrequency(2.1f, DeviceSet.devices);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void byProcessorCache(){
        Device[] exp = new Device[1];
        exp[0] = DeviceSet.devices[4];
        Device[] act = DevicesFilter.byProcessorCache(128, DeviceSet.devices);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void byProcessorBitCapacity(){
        Device[] exp = new Device[2];
        exp[0] = DeviceSet.devices[0];
        exp[1] = DeviceSet.devices[4];
        Device[] act = DevicesFilter.byProcessorBitCapacity(1024, DeviceSet.devices);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void memoryBiggerThan(){
        Device[] exp = new Device[6];
        exp[0] = DeviceSet.devices[0];
        exp[1] = DeviceSet.devices[2];
        exp[2] = DeviceSet.devices[3];
        exp[3] = DeviceSet.devices[4];
        exp[4] = DeviceSet.devices[5];
        exp[5] = DeviceSet.devices[8];
        Device[] act = DevicesFilter.memoryBiggerThan(600, DeviceSet.devices);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void memoryLesserThan(){
        Device[] exp = new Device[4];
        exp[0] = DeviceSet.devices[1];
        exp[1] = DeviceSet.devices[6];
        exp[2] = DeviceSet.devices[7];
        exp[3] = DeviceSet.devices[9];
        Device[] act = DevicesFilter.memoryLesserThan(600, DeviceSet.devices);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void memoryUsedMoreThan(){
        Device[] exp = new Device[0];
        Device[] act = DevicesFilter.memoryUsedMoreThan(60, DeviceSet.devices);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void memoryUsedLessThan(){
        Device[] exp = DeviceSet.devices;
        Device[] act = DevicesFilter.memoryUsedLessThan(60, DeviceSet.devices);
        Assert.assertArrayEquals(exp, act);
    }



}
