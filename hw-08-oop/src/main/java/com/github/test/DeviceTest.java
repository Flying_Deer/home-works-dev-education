package com.github.test;

import com.github.oop.Device;
import com.github.oop.Processor;
import com.github.oop.ProcessorArm;
import com.github.oop.ProcessorX86;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class DeviceTest {

    private static final Device deviceArm = new Device(new ProcessorArm(), 512);
    private static final String[] data = new String[3];

    @BeforeClass
    public static void setUp(){
        data[0] = "data0";
        data[1] = "data1";
        data[2] = "data2";
        deviceArm.save(data);
    }

    @Test
    public void readAll(){
        String[] exp = data.clone();
        String[] act = deviceArm.readAll();
        Assert.assertArrayEquals(exp, act);
    }
}
