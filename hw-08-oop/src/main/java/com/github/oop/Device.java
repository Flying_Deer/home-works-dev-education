package com.github.oop;

import java.util.ArrayList;
import java.util.Objects;

public class Device {

    private final Processor processor;

    private final Memory memory;

    public Device(Processor processor, int memorySize) {
        this.processor = processor;
        this.memory = new Memory(memorySize);
    }

    public void save(String[] data){
        for(String str : data){
            this.memory.save(str);
        }
    }

    public String[] readAll(){
        ArrayList<String> resultList = new ArrayList<>();
        while (true) {
            try {
                resultList.add(memory.readLast());
                memory.removeLast();
            } catch (NullPointerException e) {
                break;
            }
        }
        String[] resultArray = new String[resultList.size()];
        resultArray = resultList.toArray(resultArray);
        return resultArray;
    }

    public void dataProcessing(){
        System.out.println("Data processing...");
    }

    public String getSystemInfo(){
        return String.format("Processor: %s\nMemory: %s", processor.getDetails(), memory.getMemoryInfo().toString());
    }

    public Processor getProcessor() {
        return processor;
    }

    public Memory getMemory() {
        return memory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Device device = (Device) o;
        return processor.equals(device.processor) && memory.equals(device.memory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(processor, memory);
    }
}
