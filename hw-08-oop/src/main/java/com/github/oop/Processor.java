package com.github.oop;

import java.util.Objects;

public abstract class Processor {

    protected float frequency;

    protected int cache;

    protected int bitCapacity;

    public float getFrequency() {
        return frequency;
    }

    public int getCache() {
        return cache;
    }

    public int getBitCapacity() {
        return bitCapacity;
    }

    public String getDetails(){
        return String.format("Frequency: %f, Cache: %d, Bit Capacity: %d",frequency, cache, bitCapacity);
    }

    public abstract String dataProcess(String data);

    public abstract String dataProcess(long data);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Processor processor = (Processor) o;
        return Float.compare(processor.frequency, frequency) == 0 && cache == processor.cache && bitCapacity == processor.bitCapacity;
    }

    @Override
    public int hashCode() {
        return Objects.hash(frequency, cache, bitCapacity);
    }
}
