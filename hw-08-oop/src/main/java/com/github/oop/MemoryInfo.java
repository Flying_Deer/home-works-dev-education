package com.github.oop;

public class MemoryInfo {

    private int numberOfCells;

    private float memoryUsed;

    public MemoryInfo(int numberOfCells, float memoryUsed) {
        this.numberOfCells = numberOfCells;
        this.memoryUsed = memoryUsed;
    }

    public int getNumberOfCells() {
        return numberOfCells;
    }

    public float getMemoryUsed() {
        return memoryUsed;
    }

    @Override
    public String toString() {
        return "Number of cells: " + numberOfCells + ", Memory used: " + memoryUsed;
    }
}
