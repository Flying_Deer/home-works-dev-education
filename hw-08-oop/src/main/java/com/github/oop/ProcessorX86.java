package com.github.oop;

import java.util.Objects;

public class ProcessorX86 extends Processor{

    public final String architecture = "X86";

    public ProcessorX86(float frequency, int cache, int bitCapacity) {
        this.frequency = frequency;
        this.cache = cache;
        this.bitCapacity = bitCapacity;
    }

    public ProcessorX86(){
        this.frequency = 2.4f;
        this.cache = 64;
        this.bitCapacity = 1024;
    }

    @Override
    public String dataProcess(String data) {
        return data.toLowerCase();
    }

    @Override
    public String dataProcess(long data) {
        return Long.toString(data);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProcessorX86 that = (ProcessorX86) o;
        return architecture.equals(that.architecture);
    }

    @Override
    public int hashCode() {
        return Objects.hash(architecture);
    }
}
