package com.github.oop;

import java.util.ArrayList;

public class DevicesFilter {

    public static Device[] getArms(Device[] devices){
        ArrayList<Device> resultList = new ArrayList<>();
        for(Device device : devices){
            if(device.getProcessor() instanceof ProcessorArm){
                resultList.add(device);
            }
        }
        Device[] resultArray = new Device[resultList.size()];
        return resultList.toArray(resultArray);
    }

    public static Device[] getX86(Device[] devices){
        ArrayList<Device> resultList = new ArrayList<>();
        for(Device device : devices){
            if(device.getProcessor() instanceof ProcessorX86){
                resultList.add(device);
            }
        }
        Device[] resultArray = new Device[resultList.size()];
        return resultList.toArray(resultArray);
    }

    public static Device[] byProcessorFrequency(float frequency, Device[] devices){
        ArrayList<Device> resultList = new ArrayList<>();
        for(Device device : devices){
            if(device.getProcessor().getFrequency() == frequency){
                resultList.add(device);
            }
        }
        Device[] resultArray = new Device[resultList.size()];
        return resultList.toArray(resultArray);
    }

    public static Device[] byProcessorCache(int cache, Device[] devices){
        ArrayList<Device> resultList = new ArrayList<>();
        for(Device device : devices){
            if(device.getProcessor().getCache() == cache){
                resultList.add(device);
            }
        }
        Device[] resultArray = new Device[resultList.size()];
        return resultList.toArray(resultArray);
    }

    public static Device[] byProcessorBitCapacity(int bitCapacity, Device[] devices){
        ArrayList<Device> resultList = new ArrayList<>();
        for(Device device : devices){
            if(device.getProcessor().getBitCapacity() == bitCapacity){
                resultList.add(device);
            }
        }
        Device[] resultArray = new Device[resultList.size()];
        return resultList.toArray(resultArray);
    }

    public static Device[] memoryBiggerThan(int memorySize, Device[] devices){
        ArrayList<Device> resultList = new ArrayList<>();
        for(Device device : devices){
            if(device.getMemory().getMemorySize() > memorySize){
                resultList.add(device);
            }
        }
        Device[] resultArray = new Device[resultList.size()];
        return resultList.toArray(resultArray);
    }

    public static Device[] memoryLesserThan(int memorySize, Device[] devices){
        ArrayList<Device> resultList = new ArrayList<>();
        for(Device device : devices){
            if(device.getMemory().getMemorySize() < memorySize){
                resultList.add(device);
            }
        }
        Device[] resultArray = new Device[resultList.size()];
        return resultList.toArray(resultArray);
    }

    public static Device[] memoryUsedMoreThan(float memoryUsed, Device[] devices){
        ArrayList<Device> resultList = new ArrayList<>();
        for(Device device : devices){
            if(device.getMemory().getMemoryInfo().getMemoryUsed() > memoryUsed){
                resultList.add(device);
            }
        }
        Device[] resultArray = new Device[resultList.size()];
        return resultList.toArray(resultArray);
    }

    public static Device[] memoryUsedLessThan(float memoryUsed, Device[] devices){
        ArrayList<Device> resultList = new ArrayList<>();
        for(Device device : devices){
            if(device.getMemory().getMemoryInfo().getMemoryUsed() < memoryUsed){
                resultList.add(device);
            }
        }
        Device[] resultArray = new Device[resultList.size()];
        return resultList.toArray(resultArray);
    }
}
