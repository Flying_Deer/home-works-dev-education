package com.github.oop;

public class DeviceSet {

    public static Device[] devices = new Device[10];

    static {
        devices[0] = new Device(new ProcessorX86(1.0f, 32, 1024), 1024);
        devices[1] = new Device(new ProcessorX86(2.0f, 64, 512), 512);
        devices[2] = new Device(new ProcessorX86(2.4f, 16, 256), 1024);
        devices[3] = new Device(new ProcessorArm(2.1f, 32, 128), 2048);
        devices[4] = new Device(new ProcessorArm(3.6f, 128, 1024), 1024);
        devices[5] = new Device(new ProcessorArm(1.4f, 32, 512), 4096);
        devices[6] = new Device(new ProcessorArm(4.0f, 16, 128), 256);
        devices[7] = new Device(new ProcessorX86(1.2f, 32, 64), 512);
        devices[8] = new Device(new ProcessorArm(2.1f, 16, 32), 8192);
        devices[9] = new Device(new ProcessorX86(1.6f, 64, 128), 128);
    }

}
