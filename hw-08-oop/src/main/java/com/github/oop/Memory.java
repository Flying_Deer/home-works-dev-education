package com.github.oop;

import java.util.Arrays;
import java.util.Objects;

public class Memory {

    private final String[] memoryCell;

    private final int memorySize;

    public Memory(int memorySize) {
        this.memorySize = memorySize;
        this.memoryCell = new String[memorySize];
    }

    public int getMemorySize() {
        return memorySize;
    }

    public String readLast(){
        if(memoryCell.length == 0){
            throw new NullPointerException("MemoryCell is empty.");
        }
        for(int i = memorySize - 1; i >= 0; i--){
            if(memoryCell[i] != null){
                return memoryCell[i];
            }
        }
        throw new NullPointerException("All cells are null.");
    }

    public String removeLast(){
        if(memoryCell.length == 0){
            throw new IllegalArgumentException("MemoryCell is empty.");
        }
        for(int i = memorySize - 1; i >= 0; i--){
            if(memoryCell[i] != null){
                String tmp = memoryCell[i];
                memoryCell[i] = null;
                return tmp;
            }
        }
        throw new NullPointerException("All cells are null.");
    }

    public boolean save(String data){
        if(data == null){
            return false;
        }
        if(memoryCell.length == 0){
            throw new IllegalArgumentException("MemoryCell is empty.");
        }
        for(int i = memorySize - 1; i >= 0; i--){
            if(memoryCell[i] == null){
                memoryCell[i] = data;
                return true;
            }
        }
        return false;
    }

    public MemoryInfo getMemoryInfo(){
        if(memoryCell.length == 0){
            throw new IllegalArgumentException("MemoryCell is empty.");
        }
        float percentage = 0;
        float percentagePerCell = 100.f / memorySize;
        for (int i = 0; i < memorySize; i++){
            if(memoryCell[i] != null){
                percentage += percentagePerCell;
            }
        }
        return new MemoryInfo(memorySize, percentage);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Memory memory = (Memory) o;
        return memorySize == memory.memorySize && Arrays.equals(memoryCell, memory.memoryCell);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(memorySize);
        result = 31 * result + Arrays.hashCode(memoryCell);
        return result;
    }
}
