package com.github.oop;

import java.util.Objects;

public class ProcessorArm extends Processor{

    public final String architecture = "ARM";

    public ProcessorArm(float frequency, int cache, int bitCapacity) {
        this.frequency = frequency;
        this.cache = cache;
        this.bitCapacity = bitCapacity;
    }

    public ProcessorArm(){
        this.frequency = 2.4f;
        this.cache = 64;
        this.bitCapacity = 1024;
    }

    @Override
    public String dataProcess(String data) {
        return data.toUpperCase();
    }

    @Override
    public String dataProcess(long data) {
        return Long.toString(data);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProcessorArm that = (ProcessorArm) o;
        return architecture.equals(that.architecture);
    }

    @Override
    public int hashCode() {
        return Objects.hash(architecture);
    }
}
