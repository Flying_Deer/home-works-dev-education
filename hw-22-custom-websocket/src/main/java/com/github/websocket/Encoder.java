package com.github.websocket;

public class Encoder {

    public static byte[] encode(String str){
        byte[] result;
        int offset = 2;
        if (str.length() < 126) {
            result = new byte[str.length() + offset];
            result[1] = (byte) str.length();
        } else {
            offset = 4;
            result = new byte[str.length() + offset];
            result[1] = (byte) 126;
            result[2] = (byte)((str.length() >> 8) & 0xff);
            result[3] = (byte)(str.length() & 0xff);
        }
        result[0] = (byte) -127;
        for (int i = 0; i < str.length(); i++) {
            result[i + offset] = (byte) str.charAt(i);
        }
        return result;
    }

}
