package com.github.websocket;

public class WebsocketApplication {

    public static void main(String[] args) {

        Websocket websocket = new Websocket();
        websocket.run();

    }

}
