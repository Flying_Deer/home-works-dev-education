package com.github.websocket.exceptions;

public class BitConverterException extends RuntimeException{

    public BitConverterException() {
    }

    public BitConverterException(String message) {
        super(message);
    }

}
