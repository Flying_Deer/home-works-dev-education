package com.github.simple_paint.view.gui.impl;

import com.github.simple_paint.config.ImageFormatChooser;
import com.github.simple_paint.formats.BaseFormat;
import com.github.simple_paint.utils.Figure;
import com.github.simple_paint.view.DrawingPanel;
import com.github.simple_paint.view.components.*;
import com.github.simple_paint.view.gui.Paint;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.File;

public class RightDividedPanelPaint extends Paint implements IOpenSave, IColor, IClear, IThickness, IFigures {

    BaseFormat format;

    @Override
    public void clearButtonSetUp() {
        clearButton.addActionListener(e -> drawingPanel.clear());
    }

    @Override
    public void colorButtonSetUp() {
        colorButton.addActionListener(e -> {
            Color color = JColorChooser.showDialog(new JFrame(), "Color", Color.RED);
            drawingPanel.setColor(color);
            colorField.setBackground(color);
        });
    }

    @Override
    public void ellipseButtonSetUp() {
        ellipseButton.addActionListener(e -> drawingPanel.setFigure(Figure.ELLIPSE));
    }

    @Override
    public void lineButtonSetUp() {
        lineButton.addActionListener(e -> drawingPanel.setFigure(Figure.LINE));
    }

    @Override
    public void pencilButtonSetUp() {
        pencilButton.addActionListener(e -> drawingPanel.setFigure(Figure.PENCIL));
    }

    @Override
    public void rectangleButtonSetUp() {
        rectangleButton.addActionListener(e -> drawingPanel.setFigure(Figure.RECTANGLE));
    }

    @Override
    public void roundRectangleButtonSetUp() {
        roundRectangleButton.addActionListener(e -> drawingPanel.setFigure(Figure.ROUND_RECTANGLE));
    }

    @Override
    public void openButtonSetUp() {
        openButton.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser();
            FileNameExtensionFilter filterImages = new FileNameExtensionFilter(
                    ".png, .jpeg, .bmp",
                    "png", "jpeg", "bmp"
            );
            FileNameExtensionFilter filterObjects = new FileNameExtensionFilter(
                    ".bin, .csv, .json, .xml, .yml",
                    "bin", "csv", "json", "xml", "yml"
            );
            fileChooser.addChoosableFileFilter(filterImages);
            fileChooser.addChoosableFileFilter(filterObjects);
            int rVal = fileChooser.showOpenDialog(paintFrame);
            if(rVal == JFileChooser.APPROVE_OPTION){
                File file = fileChooser.getSelectedFile();
                format = ImageFormatChooser.chooseFormat(file);
                this.drawingPanel.setImage(format.load());
            }
        });
    }

    @Override
    public void saveButtonSetUp() {
        saveButton.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser();
            FileNameExtensionFilter filterImages = new FileNameExtensionFilter(
                    ".png, .jpeg, .bmp",
                    "png", "jpeg", "bmp"
            );
            FileNameExtensionFilter filterObjects = new FileNameExtensionFilter(
                    ".bin, .csv, .json, .xml, .yml",
                    "bin", "csv", "json", "xml", "yml"
            );
            fileChooser.addChoosableFileFilter(filterImages);
            fileChooser.addChoosableFileFilter(filterObjects);
            int rVal = fileChooser.showSaveDialog(paintFrame);
            if(rVal == JFileChooser.APPROVE_OPTION){
                File file = fileChooser.getSelectedFile();
                format = ImageFormatChooser.chooseFormat(file);
                format.save(this.drawingPanel.getImage());
            }
        });
    }

    @Override
    public void thicknessSetUp() {
        thicknessSlider.setValue(1);
        thicknessField.setText("1");
        thicknessSlider.addChangeListener(e -> {
            if(e.getSource() == thicknessSlider){
                thicknessField.setText(String.valueOf(thicknessSlider.getValue()));
                drawingPanel.setThickness(thicknessSlider.getValue());
            }
        });
        thicknessField.addActionListener(e -> {
            if(e.getSource() == thicknessField){
                int thicknessValue;
                try{
                    thicknessValue = Integer.parseInt(thicknessField.getText());
                    thicknessSlider.setValue(thicknessValue);
                    drawingPanel.setThickness(thicknessValue);
                } catch (RuntimeException ignore){ }
            }
        });
    }

    @Override
    public void show() {

        paintFrame = new JFrame("Unnamed");

        //========================================================
        //========   Initialization of drawing panel     =========
        //========================================================

        drawingPanel = new DrawingPanel();
        drawingPanel.setBounds(5, 5, 800, 600);
        drawingPanel.setVisible(true);
        drawingPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        //========================================================
        //========   Initialization of brush panel     ===========
        //========================================================

        JPanel brushPanel = new JPanel();
        brushPanel.setVisible(true);
        brushPanel.setLayout(null);
        brushPanel.setBounds(810, 5, 200, 160);
        brushPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        JLabel brushSettingsLabel = new JLabel("Brush settings");
        brushSettingsLabel.setBounds(5, 10, 100, 10);
        brushPanel.add(brushSettingsLabel);

        thicknessLabel.setBounds(7, 110, 100, 10);
        thicknessField.setBounds(120, 128, 25, 20);
        thicknessSlider.setBounds(5, 125, 110, 30);
        brushPanel.add(thicknessLabel);
        brushPanel.add(thicknessSlider);
        brushPanel.add(thicknessField);
        thicknessSetUp();

        colorButton.setBounds(5, 25, 110, 30);
        colorField.setBounds(120, 25, 30, 30);
        colorField.setModel(new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        });
        colorField.setBackground(drawingPanel.getColor());
        colorField.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        brushPanel.add(colorField);
        brushPanel.add(colorButton);
        colorButtonSetUp();

        clearButton.setBounds(5, 65, 110, 30);
        brushPanel.add(clearButton);
        clearButtonSetUp();

        //========================================================
        //========   Initialization of figure panel     ==========
        //========================================================

        JPanel figurePanel = new JPanel();
        figurePanel.setVisible(true);
        figurePanel.setLayout(null);
        figurePanel.setBounds(810, 170, 200, 150);
        figurePanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        JLabel figureInfo = new JLabel("Figure");
        figureInfo.setBounds(5, 5, 100, 20);
        figurePanel.add(figureInfo);

        ellipseButton.setBounds(5, 30, 90, 30);
        figurePanel.add(ellipseButton);
        ellipseButtonSetUp();

        lineButton.setBounds(100, 30, 90, 30);
        figurePanel.add(lineButton);
        lineButtonSetUp();

        pencilButton.setBounds(5, 65, 90, 30);
        figurePanel.add(pencilButton);
        pencilButtonSetUp();

        rectangleButton.setBounds(100, 65, 90, 30);
        figurePanel.add(rectangleButton);
        rectangleButtonSetUp();

        roundRectangleButton.setBounds(5, 100, 185, 30);
        figurePanel.add(roundRectangleButton);
        roundRectangleButtonSetUp();

        //========================================================
        //========   Initialization of file panel     ============
        //========================================================

        JPanel filePanel = new JPanel();
        filePanel.setVisible(true);
        filePanel.setLayout(null);
        filePanel.setBounds(810, 325, 200, 70);
        filePanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        JLabel fileInfo = new JLabel("File");
        fileInfo.setBounds(5, 5, 100, 20);
        filePanel.add(fileInfo);

        saveButton.setBounds(5, 30, 70, 30);
        filePanel.add(saveButton);
        saveButtonSetUp();

        openButton.setBounds(80, 30, 70, 30);
        filePanel.add(openButton);
        openButtonSetUp();

        //========================================================
        //==========    Mouse tracker initialization    ==========
        //========================================================

        JLabel mouseCoordinateTracker = new JLabel(String.format("X: %-4s Y: %-4s", "0","0"));
        mouseCoordinateTracker.setBounds(810, 400,200, 20);
        mouseCoordinateTracker.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        drawingPanel.addMouseMotionListener(new MouseMotionAdapter(){
            public void mouseMoved(MouseEvent e){
                mouseCoordinateTracker.setText(String.format("X: %-4s Y: %-4s",
                        e.getX(), e.getY()));
            }
        });
        drawingPanel.addMouseMotionListener(new MouseMotionAdapter(){
            public void mouseDragged(MouseEvent e){
                mouseCoordinateTracker.setText(String.format("X: %-4s Y: %-4s",
                        e.getX(), e.getY()));
            }
        });

        //========================================================
        //=============    Frame initialization    ===============
        //========================================================

        paintFrame.setLayout(null);
        paintFrame.setBounds(10, 10, 1030, 650);
        paintFrame.setVisible(true);
        paintFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        paintFrame.add(drawingPanel);
        paintFrame.add(brushPanel);
        paintFrame.add(figurePanel);
        paintFrame.add(filePanel);
        paintFrame.add(mouseCoordinateTracker);

    }

}
