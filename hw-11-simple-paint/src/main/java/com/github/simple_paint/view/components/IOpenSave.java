package com.github.simple_paint.view.components;

import javax.swing.*;

public interface IOpenSave {

    void openButtonSetUp();

    void saveButtonSetUp();

    JButton saveButton = new JButton("Save");

    JButton openButton = new JButton("Open");

}
