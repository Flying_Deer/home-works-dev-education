package com.github.simple_paint.view.components;

import javax.swing.*;

public interface IFigures {

    JButton ellipseButton = new JButton("Ellipse");

    void ellipseButtonSetUp();

    JButton lineButton = new JButton("Line");

    void lineButtonSetUp();

    JButton pencilButton = new JButton("Pencil");

    void pencilButtonSetUp();

    JButton rectangleButton = new JButton("Rectangle");

    void rectangleButtonSetUp();

    JButton roundRectangleButton = new JButton("Round rectangle");

    void roundRectangleButtonSetUp();

}
