package com.github.simple_paint.view.components;

import javax.swing.*;

public interface IThickness {

    JLabel thicknessLabel = new JLabel("Thickness");

    JSlider thicknessSlider = new JSlider();

    JTextField thicknessField = new JTextField();

    void thicknessSetUp();

}
