package com.github.simple_paint.view.components;

import javax.swing.*;

public interface IClear {

    JButton clearButton = new JButton("Clear");

    void clearButtonSetUp();

}
