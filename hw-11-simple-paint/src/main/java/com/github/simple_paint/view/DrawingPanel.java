package com.github.simple_paint.view;

import com.github.simple_paint.utils.Figure;
import com.github.simple_paint.utils.FigureDrawer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;

public class DrawingPanel extends JPanel {

    private Image image, oldImage;

    private Graphics2D graphics, oldGraphics;

    private Color currentColor = Color.black;

    private int currentThickness = 1;

    private int currentX, currentY, oldX, oldY;

    Figure currentFigure = Figure.PENCIL;

    public DrawingPanel() {
        setDoubleBuffered(false);
        addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                oldX = e.getX();
                oldY = e.getY();
                oldImage = new BufferedImage(getSize().width, getSize().height, BufferedImage.TYPE_INT_RGB);
                oldGraphics = (Graphics2D) oldImage.getGraphics();
                oldGraphics.drawImage(image, 0, 0, null);
            }
        });
        addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseDragged(MouseEvent e) {
                currentX = e.getX();
                currentY = e.getY();

                if (graphics != null) {
                    if(currentFigure == Figure.PENCIL) {
                        graphics.drawLine(oldX, oldY, currentX, currentY);
                        repaint();
                        oldX = currentX;
                        oldY = currentY;
                    } else {
                        setImage(oldImage);
                        FigureDrawer.drawFigure(graphics, currentFigure, oldX, oldY, currentX, currentY);
                        repaint();
                    }
                }
            }
        });
    }

    @Override
    protected void paintComponent(Graphics g) {
        if (image == null) {
            image = createImage(getSize().width, getSize().height);
            graphics = (Graphics2D) image.getGraphics();
            graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            clear();
            graphics.setPaint(currentColor);
            graphics.setStroke(new BasicStroke(currentThickness));
        }
        g.drawImage(image, 0, 0, null);
    }

    public void clear() {
        graphics.setPaint(Color.white);
        graphics.fillRect(0, 0, getSize().width, getSize().height);
        graphics.setPaint(currentColor);
        repaint();
    }

    public Color getColor() {
        return currentColor;
    }

    public void setColor(Color color){
        currentColor = color;
        graphics.setPaint(currentColor);
    }

    public void setThickness(int thickness){
        currentThickness = thickness;
        graphics.setStroke(new BasicStroke(thickness));
    }

    public Image getImage() {
        return image;
    }

    public int getThickness() {
        return currentThickness;
    }

    public void setImage(Image image) {
        graphics.drawImage(image, 0, 0, getSize().width, getSize().height, null);
        repaint();
    }

    public void setFigure(Figure figure){
        this.currentFigure = figure;
    }
}