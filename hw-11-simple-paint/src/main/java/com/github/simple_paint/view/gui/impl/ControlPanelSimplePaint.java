package com.github.simple_paint.view.gui.impl;

import com.github.simple_paint.config.ImageFormatChooser;
import com.github.simple_paint.formats.BaseFormat;
import com.github.simple_paint.utils.Thickness;
import com.github.simple_paint.view.DrawingPanel;
import com.github.simple_paint.view.components.*;
import com.github.simple_paint.view.gui.Paint;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.File;

public class ControlPanelSimplePaint extends Paint implements IColor, IThickness, IClear, IOpenSave  {

    BaseFormat format;

    @Override
    public void colorButtonSetUp() {
        colorButton.addActionListener(e -> {
            Color color = JColorChooser.showDialog(new JFrame(), "Color", Color.RED);
            drawingPanel.setColor(color);
            colorButton.setBackground(color);
        });
    }

    @Override
    public void clearButtonSetUp() {
        clearButton.addActionListener(e -> drawingPanel.clear());
    }

    @Override
    public void saveButtonSetUp() {
        saveButton.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser();
            FileNameExtensionFilter filterImages = new FileNameExtensionFilter(
                    ".png, .jpeg, .bmp",
                    "png", "jpeg", "bmp"
            );
            FileNameExtensionFilter filterObjects = new FileNameExtensionFilter(
                    ".bin, .csv, .json, .xml, .yml",
                    "bin", "csv", "json", "xml", "yml"
            );
            fileChooser.addChoosableFileFilter(filterImages);
            fileChooser.addChoosableFileFilter(filterObjects);
            int rVal = fileChooser.showSaveDialog(paintFrame);
            if(rVal == JFileChooser.APPROVE_OPTION){
                File file = fileChooser.getSelectedFile();
                format = ImageFormatChooser.chooseFormat(file);
                format.save(this.drawingPanel.getImage());
            }
        });
    }

    @Override
    public void openButtonSetUp() {
        openButton.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser();
            FileNameExtensionFilter filterImages = new FileNameExtensionFilter(
                    ".png, .jpeg, .bmp",
                    "png", "jpeg", "bmp"
            );
            FileNameExtensionFilter filterObjects = new FileNameExtensionFilter(
                    ".bin, .csv, .json, .xml, .yml",
                    "bin", "csv", "json", "xml", "yml"
            );
            fileChooser.addChoosableFileFilter(filterImages);
            fileChooser.addChoosableFileFilter(filterObjects);
            int rVal = fileChooser.showOpenDialog(paintFrame);
            if(rVal == JFileChooser.APPROVE_OPTION){
                File file = fileChooser.getSelectedFile();
                format = ImageFormatChooser.chooseFormat(file);
                this.drawingPanel.setImage(format.load());
            }
        });
    }

    @Override
    public void thicknessSetUp() {
//        thicknessButton.addActionListener(e -> drawingPanel.setThickness(
//                Integer.parseInt((String) JOptionPane.showInputDialog(
//                new JFrame(), "Thickness", "Thickness", JOptionPane.PLAIN_MESSAGE,
//                null, Thickness.set, String.valueOf(drawingPanel.getThickness())))));
    }

    @Override
    public void show() {
        paintFrame = new JFrame("Swing Paint");
        Container content = paintFrame.getContentPane();
        content.setLayout(new BorderLayout());
        drawingPanel = new DrawingPanel();

        content.add(drawingPanel, BorderLayout.CENTER);

        JPanel controlPanel = new JPanel();

        clearButtonSetUp();
        controlPanel.add(clearButton);
        colorButtonSetUp();
        controlPanel.add(colorButton);
        thicknessSetUp();
        controlPanel.add(thicknessSlider);
        openButtonSetUp();
        controlPanel.add(openButton);
        saveButtonSetUp();
        controlPanel.add(saveButton);

        content.add(controlPanel, BorderLayout.NORTH);

        paintFrame.setSize(734, 553);
        paintFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        paintFrame.setVisible(true);
    }
}
