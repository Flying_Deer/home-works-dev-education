package com.github.simple_paint.view.components;

import javax.swing.*;

public interface IColor {

    JButton colorButton = new JButton("Color");

    JTable colorField = new JTable(1, 1);

    void colorButtonSetUp();

}
