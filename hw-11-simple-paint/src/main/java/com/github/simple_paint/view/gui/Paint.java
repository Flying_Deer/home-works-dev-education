package com.github.simple_paint.view.gui;

import com.github.simple_paint.view.DrawingPanel;

import javax.swing.*;

public abstract class Paint {

    protected JFrame paintFrame;

    protected DrawingPanel drawingPanel;

    public abstract void show();

}
