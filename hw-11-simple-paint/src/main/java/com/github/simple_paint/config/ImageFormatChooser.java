package com.github.simple_paint.config;

import com.github.simple_paint.formats.BaseFormat;
import com.github.simple_paint.formats.impl.*;
import com.github.simple_paint.formats.impl.images.BmpFormat;
import com.github.simple_paint.formats.impl.images.JpegFormat;
import com.github.simple_paint.formats.impl.images.PngFormat;
import org.apache.commons.io.FilenameUtils;

import java.io.File;

public class ImageFormatChooser {

    public static BaseFormat chooseFormat(File file){
        switch (FilenameUtils.getExtension(file.getName())){
            case "png": return new PngFormat(file);
            case "jpeg": return new JpegFormat(file);
            case "bmp": return new BmpFormat(file);
            case "bin": return new BinFormat(file);
            case "csv": return new CsvFormat(file);
            case "json": return new JsonFormat(file);
            case "xml": return new XmlFormat(file);
            case "yml": return new YmlFormat(file);
        }
        throw new IllegalArgumentException();
    }
}
