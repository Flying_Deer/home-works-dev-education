package com.github.simple_paint.utils;

public enum Figure {
    ELLIPSE,
    LINE,
    PENCIL,
    RECTANGLE,
    ROUND_RECTANGLE
}
