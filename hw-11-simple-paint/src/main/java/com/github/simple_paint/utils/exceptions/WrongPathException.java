package com.github.simple_paint.utils.exceptions;

public class WrongPathException extends RuntimeException{

    public WrongPathException(String errorMessage){
        super(errorMessage);
    }

}
