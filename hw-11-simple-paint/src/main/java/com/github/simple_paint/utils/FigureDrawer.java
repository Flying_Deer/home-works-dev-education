package com.github.simple_paint.utils;

import java.awt.*;
import java.awt.geom.Ellipse2D;

public class FigureDrawer {

    public static void drawFigure(Graphics2D graphics, Figure figure, int oldX, int oldY, int newX, int newY){
        switch (figure){
            case LINE:
                graphics.drawLine(oldX, oldY, newX, newY);
                break;
            case ELLIPSE:
                graphics.draw(new Ellipse2D.Double(
                        Math.min(oldX, newX),
                        Math.min(oldY, newY),
                        Math.abs(newX - oldX),
                        Math.abs(newY - oldY)));
                break;
            case RECTANGLE:
                graphics.drawRect(
                        Math.min(oldX, newX),
                        Math.min(oldY, newY),
                        Math.abs(newX - oldX),
                        Math.abs(newY - oldY));
                break;
            case ROUND_RECTANGLE:
                graphics.drawRoundRect(
                        Math.min(oldX, newX),
                        Math.min(oldY, newY),
                        Math.abs(newX - oldX),
                        Math.abs(newY - oldY),
                        Math.abs(newX - oldX)/4,
                        Math.abs(newY - oldY)/4);
                break;
        }
    }

}
