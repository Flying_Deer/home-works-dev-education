package com.github.simple_paint.utils.exceptions;

public class WrongFormatException extends RuntimeException{

    public WrongFormatException(String errorMessage){
        super(errorMessage);
    }
}
