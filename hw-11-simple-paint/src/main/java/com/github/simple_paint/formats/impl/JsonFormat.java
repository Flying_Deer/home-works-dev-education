package com.github.simple_paint.formats.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.simple_paint.formats.BaseFormat;
import com.github.simple_paint.models.CustomImage;
import com.github.simple_paint.utils.exceptions.WrongFormatException;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class JsonFormat implements BaseFormat {

    private final File file;

    public JsonFormat(File file) {
        this.file = file;
    }

    @Override
    public Image load() {
        CustomImage customImage;
        ObjectMapper mapper = new ObjectMapper();
        try {
            customImage = mapper.readValue(this.file, CustomImage.class);
            mapper.readValue(this.file, List.class);
        } catch (IOException e){
            throw new WrongFormatException("Failed to read a file.");
        }
        return customImage.toBufferedImage();
    }

    @Override
    public boolean save(Image image) {
        CustomImage customImage = new CustomImage((BufferedImage) image);
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(this.file, customImage);
            return true;
        } catch (IOException e){
            return false;
        }
    }

}
