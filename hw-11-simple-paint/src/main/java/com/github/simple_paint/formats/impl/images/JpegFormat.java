package com.github.simple_paint.formats.impl.images;

import com.github.simple_paint.formats.BaseFormat;
import com.github.simple_paint.utils.exceptions.WrongFormatException;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;

public class JpegFormat implements BaseFormat {

    private final File file;

    public JpegFormat(File file) {
        this.file = file;
    }

    @Override
    public Image load(){
        try {
            return ImageIO.read(this.file);
        } catch (IOException e){
            throw new WrongFormatException("Failed to read a file.");
        }
    }

    @Override
    public boolean save(Image image){
        try {
            return ImageIO.write((RenderedImage) image, "jpeg", this.file);
        } catch (IOException e){
            throw new WrongFormatException("Failed to write a file");
        }
    }

}
