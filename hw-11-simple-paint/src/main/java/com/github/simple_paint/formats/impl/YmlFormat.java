package com.github.simple_paint.formats.impl;

import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import com.github.simple_paint.formats.BaseFormat;
import com.github.simple_paint.models.CustomImage;
import com.github.simple_paint.utils.exceptions.WrongFormatException;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class YmlFormat implements BaseFormat {

    private final File file;

    public YmlFormat(File file) {
        this.file = file;
    }

    @Override
    public Image load() {
        CustomImage customImage;
        YAMLMapper mapper = new YAMLMapper();
        try {
            customImage = mapper.readValue(this.file, CustomImage.class);
        } catch (IOException e){
            e.printStackTrace();
            throw new WrongFormatException("Failed to read a file.");
        }
        return customImage.toBufferedImage();
    }

    @Override
    public boolean save(Image image) {
        CustomImage customImage = new CustomImage((BufferedImage) image);
        YAMLMapper mapper = new YAMLMapper();
        try {
            mapper.writeValue(this.file, customImage);
            return true;
        } catch (IOException e){
            return false;
        }
    }

}
