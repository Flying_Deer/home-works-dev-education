package com.github.simple_paint.formats;

import java.awt.*;
import java.io.File;

public interface BaseFormat {

    Image load();

    boolean save(Image image);

}
