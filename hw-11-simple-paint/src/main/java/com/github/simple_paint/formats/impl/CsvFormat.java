package com.github.simple_paint.formats.impl;

import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.github.simple_paint.formats.BaseFormat;
import com.github.simple_paint.models.CustomImage;
import com.github.simple_paint.utils.exceptions.WrongFormatException;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class CsvFormat implements BaseFormat {

    private final File file;

    public CsvFormat(File file) {
        this.file = file;
    }

    @Override
    public Image load() {
        CustomImage customImage;
        CsvMapper mapper = new CsvMapper();
        try {
            customImage = mapper.readValue(this.file, CustomImage.class);
        } catch (IOException e){
            throw new WrongFormatException("Failed to read a file.");
        }
        return customImage.toBufferedImage();
    }

    @Override
    public boolean save(Image image) {
        CustomImage customImage = new CustomImage((BufferedImage) image);
        CsvMapper mapper = new CsvMapper();
        try {
            mapper.writeValue(this.file, customImage);
            return true;
        } catch (IOException e){
            e.printStackTrace();
            return false;
        }
    }

}
