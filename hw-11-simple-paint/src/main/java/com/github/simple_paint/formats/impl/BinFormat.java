package com.github.simple_paint.formats.impl;

import com.github.simple_paint.formats.BaseFormat;
import com.github.simple_paint.models.CustomImage;
import com.github.simple_paint.utils.FileUtils;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

public class BinFormat implements BaseFormat {

    private final File file;

    public BinFormat(File file) {
        this.file = file;
    }

    @Override
    public Image load() {
        byte[] bytes = FileUtils.readBinFile(file);
        CustomImage customImage = new CustomImage();
        try(ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
            ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream)){
            customImage = (CustomImage) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return customImage.toBufferedImage();
    }

    @Override
    public boolean save(Image image) {
        if(!this.file.exists()){
            FileUtils.createFile(this.file);
        }
        byte[] bytes = null;
        CustomImage customImage = new CustomImage((BufferedImage) image);
        try(ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream)){
            objectOutputStream.writeObject(customImage);
            bytes = byteArrayOutputStream.toByteArray();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        return FileUtils.writeToBinFile(file, bytes);
    }

}
