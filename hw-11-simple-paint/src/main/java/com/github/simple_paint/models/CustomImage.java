package com.github.simple_paint.models;

import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.util.Arrays;

public class CustomImage implements Serializable {

    private static final long serialVersionUID = 3209363645813774245L;

    private int width = 0;

    private int height = 0;

    private int[] pixels = null;

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int[] getPixels() {
        return pixels;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setPixels(int[] pixels) {
        this.pixels = pixels;
    }

    public CustomImage() { }

    public CustomImage(int width, int height, int[] pixels) {
        this.width = width;
        this.height = height;
        this.pixels = pixels;
    }

    public CustomImage(BufferedImage image){
        this.width = image.getWidth();
        this.height = image.getHeight();
        this.pixels = new int[this.width * this.height];
        for(int x = 0; x < this.width; x++){
            for(int y = 0; y < this.height; y++){
                pixels[x + this.width * y] = image.getRGB(x, y);
            }
        }
    }

    @Override
    public String toString() {
        return "CustomImage{" +
                "width=" + width +
                ", height=" + height +
                ", pixels=" + Arrays.toString(pixels) +
                '}';
    }

    public BufferedImage toBufferedImage(){
        BufferedImage image = new BufferedImage(this.width, this.height, BufferedImage.TYPE_INT_RGB);
        for(int x = 0; x < this.width; x++){
            for( int y = 0; y < this.height; y++){
                image.setRGB(x, y, pixels[x + this.width * y]);
            }
        }
        return image;
    }


}
