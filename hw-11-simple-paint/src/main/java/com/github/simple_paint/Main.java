package com.github.simple_paint;

import com.github.simple_paint.view.gui.Paint;
import com.github.simple_paint.view.gui.impl.RightDividedPanelPaint;

public class Main {

    public static void main(String[] args) {

        Paint paint = new RightDividedPanelPaint();
        paint.show();

    }

}