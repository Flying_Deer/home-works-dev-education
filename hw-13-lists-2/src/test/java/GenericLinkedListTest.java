import com.github.collections.list.DGenericLinkedList;
import com.github.collections.list.IGenericList;
import org.junit.Assert;
import org.junit.Test;

import java.util.NoSuchElementException;

public class GenericLinkedListTest {

    private IGenericList<Integer> list = new DGenericLinkedList<>();

    private final Integer[] manyNumbers = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    private final Integer[] twoNumbers = {0, 1};
    private final Integer[] oneNumber = {0};
    private final Integer[] emptyArray = {};

    //=================================================
    //=================== Null ========================
    //=================================================

    @Test(expected = NullPointerException.class)
    public void nullArray() {
        list = new DGenericLinkedList<>(null);
    }

    //=================================================
    //=================== Clean =======================
    //=================================================
    @Test
    public void cleanMany() {
        list = new DGenericLinkedList<>(manyNumbers);
        list.clear();
        Object[] exp = {};
        Object[] act = this.list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void cleanTwo() {
        list = new DGenericLinkedList<>(twoNumbers);
        list.clear();
        Object[] exp = {};
        Object[] act = this.list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void cleanOne() {
        list = new DGenericLinkedList<>(oneNumber);
        list.clear();
        Object[] exp = {};
        Object[] act = this.list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void cleanZero() {
        list = new DGenericLinkedList<>(emptyArray);
        list.clear();
        Object[] exp = {};
        Object[] act = this.list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    //=================================================
    //=================== Size ========================
    //=================================================

    @Test
    public void sizeMany() {
        list = new DGenericLinkedList<>(manyNumbers);
        int exp = manyNumbers.length;
        int act = list.size();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void sizeTwo() {
        list = new DGenericLinkedList<>(twoNumbers);
        int exp = twoNumbers.length;
        int act = list.size();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void sizeOne() {
        list = new DGenericLinkedList<>(oneNumber);
        int exp = oneNumber.length;
        int act = list.size();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void sizeZero() {
        list = new DGenericLinkedList<>(emptyArray);
        int exp = emptyArray.length;
        int act = list.size();
        Assert.assertEquals(exp, act);
    }


    //=================================================
    //=============== Push forward ====================
    //=================================================

    @Test
    public void pushForwardMany() {
        list = new DGenericLinkedList<>(manyNumbers);
        Object[] exp = {-1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        list.pushForward(-1);
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void pushForwardTwo() {
        list = new DGenericLinkedList<>(twoNumbers);
        Object[] exp = {-1, 0, 1};
        list.pushForward(-1);
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void pushForwardOne() {
        list = new DGenericLinkedList<>(oneNumber);
        Object[] exp = {-1, 0};
        list.pushForward(-1);
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void pushForwardZero() {
        list = new DGenericLinkedList<>(emptyArray);
        Object[] exp = {-1};
        list.pushForward(-1);
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void pushNullForward() {
        list = new DGenericLinkedList<>(oneNumber);
        Object[] exp = {null, 0};
        list.pushForward(null);
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    //=================================================
    //================== Push back ====================
    //=================================================

    @Test
    public void pushBackMany() {
        list = new DGenericLinkedList<>(manyNumbers);
        Object[] exp = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        list.pushBack(10);
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void pushBackTwo() {
        list = new DGenericLinkedList<>(twoNumbers);
        Object[] exp = {0, 1, 2};
        list.pushBack(2);
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void pushBackOne() {
        list = new DGenericLinkedList<>(oneNumber);
        Object[] exp = {0, 1};
        list.pushBack(1);
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void pushBackZero() {
        list = new DGenericLinkedList<>(emptyArray);
        Object[] exp = {0};
        list.pushBack(0);
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void pushNullBack() {
        list = new DGenericLinkedList<>(oneNumber);
        Object[] exp = {0, null};
        list.pushBack(null);
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    //=================================================
    //=================== Push ========================
    //=================================================

    @Test
    public void pushMany() {
        list = new DGenericLinkedList<>(manyNumbers);
        list.push(0, 5);
        Object[] exp = {5, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void pushTwo() {
        list = new DGenericLinkedList<>(twoNumbers);
        list.push(2, 7);
        Object[] exp = {0, 1, 7};
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void pushOne() {
        list = new DGenericLinkedList<>(oneNumber);
        list.push(0, 4);
        Object[] exp = {4, 0};
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void pushZero() {
        list = new DGenericLinkedList<>(emptyArray);
        list.push(0, 2);
        Object[] exp = {2};
        Object[] act = list.toArray();
        Assert.assertEquals(exp.length, act.length);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void pushNull() {
        list = new DGenericLinkedList<>(oneNumber);
        Object[] exp = {0, null};
        list.push(1, null);
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void pushOutOfBounds() {
        list = new DGenericLinkedList<>(oneNumber);
        list.push(100, 1);
    }

    //=================================================
    //================ Remove forward =================
    //=================================================

    @Test
    public void removeForwardMany() {
        list = new DGenericLinkedList<>(manyNumbers);
        Object[] exp = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        list.removeForward();
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void removeForwardTwo() {
        list = new DGenericLinkedList<>(twoNumbers);
        Object[] exp = {1};
        list.removeForward();
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void removeForwardOne() {
        list = new DGenericLinkedList<>(oneNumber);
        Object[] exp = {};
        list.removeForward();
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeForwardZero() {
        list = new DGenericLinkedList<>(emptyArray);
        list.removeForward();
    }

    //=================================================
    //================ Remove back ====================
    //=================================================

    @Test
    public void removeBackMany() {
        list = new DGenericLinkedList<>(manyNumbers);
        Object[] exp = {0, 1, 2, 3, 4, 5, 6, 7, 8};
        list.removeBack();
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void removeBackTwo() {
        list = new DGenericLinkedList<>(twoNumbers);
        Object[] exp = {0};
        list.removeBack();
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void removeBackOne() {
        list = new DGenericLinkedList<>(oneNumber);
        Object[] exp = {};
        list.removeBack();
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeBackZero() {
        list = new DGenericLinkedList<>(emptyArray);
        list.removeBack();
    }

    //=================================================
    //==================== Remove =====================
    //=================================================

    @Test
    public void removeMany() {
        list = new DGenericLinkedList<>(manyNumbers);
        list.remove(5);
        Object[] exp = {0, 1, 2, 3, 4, 6, 7, 8, 9};
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void removeTwo() {
        list = new DGenericLinkedList<>(twoNumbers);
        list.remove(1);
        Object[] exp = {0};
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void removeOne() {
        list = new DGenericLinkedList<>(oneNumber);
        list.remove(0);
        Object[] exp = {};
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void removeZero() {
        list = new DGenericLinkedList<>(emptyArray);
        Assert.assertFalse(list.remove(1));
    }

    @Test
    public void removeNoSuchElement() {
        list = new DGenericLinkedList<>(manyNumbers);
        Assert.assertFalse(list.remove(100));
    }

    //=================================================
    //=============== Remove by index =================
    //=================================================

    @Test
    public void removeByIndexManySecondHalf() {
        list = new DGenericLinkedList<>(manyNumbers);
        list.removeByIndex(5);
        Object[] exp = {0, 1, 2, 3, 4, 6, 7, 8, 9};
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void removeByIndexManyFirstHalf() {
        list = new DGenericLinkedList<>(manyNumbers);
        list.removeByIndex(3);
        Object[] exp = {0, 1, 2, 4, 5, 6, 7, 8, 9};
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void removeByIndexTwo() {
        list = new DGenericLinkedList<>(twoNumbers);
        list.removeByIndex(1);
        Object[] exp = {0};
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void removeByIndexOne() {
        list = new DGenericLinkedList<>(oneNumber);
        list.removeByIndex(0);
        Object[] exp = {};
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeByIndexZero() {
        list = new DGenericLinkedList<>(emptyArray);
        list.removeByIndex(1);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeByIndexOutOfBounds() {
        list = new DGenericLinkedList<>(manyNumbers);
        list.removeByIndex(100);
    }


    //=================================================
    //===================== Get =======================
    //=================================================

    @Test
    public void getMany() {
        list = new DGenericLinkedList<>(manyNumbers);
        Object exp = 5;
        Object act = list.get(5);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void getTwo() {
        list = new DGenericLinkedList<>(twoNumbers);
        Object exp = 0;
        Object act = list.get(0);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void getOne() {
        list = new DGenericLinkedList<>(oneNumber);
        Object exp = 0;
        Object act = list.get(0);
        Assert.assertEquals(exp, act);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void getOutOfBounds() {
        list = new DGenericLinkedList<>(manyNumbers);
        Object act = list.get(700);
    }

    //=================================================
    //===================== Set =======================
    //=================================================

    @Test
    public void setMany() {
        list = new DGenericLinkedList<>(manyNumbers);
        list.set(3, 19);
        Object[] exp = {0, 1, 2, 19, 4, 5, 6, 7, 8, 9};
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void setTwo() {
        list = new DGenericLinkedList<>(twoNumbers);
        list.set(1, 12);
        Object[] exp = {0, 12};
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void setOne() {
        list = new DGenericLinkedList<>(oneNumber);
        list.set(0, 5);
        Object[] exp = {5};
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void setOutOfBounds() {
        list = new DGenericLinkedList<>(emptyArray);
        list.set(5, 6);
    }

    //=================================================
    //================== Contains =====================
    //=================================================

    @Test
    public void containsMany() {
        list = new DGenericLinkedList<>(manyNumbers);
        Assert.assertTrue(list.contains(6));
    }

    @Test
    public void containsTwo() {
        list = new DGenericLinkedList<>(twoNumbers);
        Assert.assertTrue(list.contains(1));
    }

    @Test
    public void containsOne() {
        list = new DGenericLinkedList<>(oneNumber);
        Assert.assertTrue(
                list.contains(0)
        );
    }

    @Test
    public void notContains() {
        list = new DGenericLinkedList<>(oneNumber);
        Assert.assertFalse(list.contains(6));
    }

}
