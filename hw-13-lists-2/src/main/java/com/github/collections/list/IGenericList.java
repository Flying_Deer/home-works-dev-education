package com.github.collections.list;

public interface IGenericList<T extends Comparable<?>> {

    void clear();

    int size();

    <T> T[] toArray();

    void pushForward(T element);

    void pushBack(T element);

    void push(int index, T element);

    T removeForward();

    T removeBack();

    T removeByIndex(int index);

    boolean remove(T element);

    T get(int index);

    boolean set(int index, T element);

    boolean contains(T element);

}
