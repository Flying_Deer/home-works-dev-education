package com.github.collections.list;

import java.util.Objects;

public class DGenericLinkedList<T extends Comparable<?>> implements IGenericList<T> {

    private class Node implements Cloneable {

        T value;
        Node next;
        Node prev;

        public Node(T value) {
            this.value = value;
            next = null;
            prev = null;
        }

        @Override
        public Node clone() {
            Node result = new Node(this.value);
            result.next = this.next;
            return result;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node node = (Node) o;
            return Objects.equals(value, node.value);
        }

        @Override
        public int hashCode() {
            return Objects.hash(value);
        }
    }

    private Node first;
    private Node last;
    private int size;

    public DGenericLinkedList() {
        this.first = null;
        this.last = null;
        this.size = 0;
    }

    public DGenericLinkedList(T[] array) {
        this.size = 0;
        for (T element : array) {
            this.pushBack(element);
        }
    }

    @Override
    public void clear() {
        this.first = null;
        this.last = null;
        this.size = 0;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public <T> T[] toArray() {
        Object[] tmp = new Object[this.size];
        Node node = this.first;
        for (int i = 0; i < this.size; i++) {
            tmp[i] = node.value;
            node = node.next;
        }
        return castArray(tmp);
    }

    @Override
    public void pushForward(T element) {
        Node node = new Node(element);
        if (this.size == 0) {
            this.first = node;
            this.last = node;
            size++;
            return;
        }
        this.first.prev = node;
        node.next = this.first;
        this.first = node;
        this.size++;
    }

    @Override
    public void pushBack(T element) {
        Node node = new Node(element);
        if (this.size == 0) {
            this.first = node;
            this.last = node;
            size++;
            return;
        }
        this.last.next = node;
        node.prev = this.last;
        this.last = node;
        this.size++;
    }

    @Override
    public void push(int index, T element) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException();
        }
        Node node = new Node(element);
        if (this.size == 0) {
            this.first = node;
            this.last = node;
            this.size++;
            return;
        }
        if(index == 0){
            pushForward(element);
            return;
        }
        if(index == this.size){
            pushBack(element);
            return;
        }
        Node tmp;
        if (index < (this.size >> 1)) {
            tmp = this.first;
            for (int i = 0; i < index - 1; i++) {
                tmp = tmp.next;
            }
            node.prev = tmp;
            node.next = tmp.next;
            tmp.next.prev = node;
            tmp.next = node;
        } else {
            tmp = this.last;
            for (int i = this.size - 1; i >= index; i++) {
                tmp = tmp.prev;
            }
            node.prev = tmp;
            node.next = tmp.next;
            tmp.next.prev = node;
            tmp.next = node;
            this.size++;
        }
    }

    @Override
    public T removeForward() {
        if (this.size == 0) {
            throw new IndexOutOfBoundsException();
        }
        T tmp = this.first.value;
        if(this.size == 1){
            this.first = null;
            this.last = null;
            this.size--;
            return tmp;
        }
        this.first = this.first.next;
        this.first.prev = null;
        this.size--;
        return tmp;
    }

    @Override
    public T removeBack() {
        if (this.size == 0) {
            throw new IndexOutOfBoundsException();
        }
        T tmp = this.last.value;
        if(this.size == 1){
            this.first = null;
            this.last = null;
            this.size--;
            return tmp;
        }
        this.last = this.last.prev;
        this.last.next = null;
        this.size--;
        return null;
    }

    @Override
    public T removeByIndex(int index) {
        if (this.size == 0) {
            throw new IndexOutOfBoundsException();
        }
        if (index < 0 || index >= this.size) {
            throw new IndexOutOfBoundsException();
        }
        Node node;
        if(index == 0){
            return removeForward();
        }
        if(index == this.size - 1){
            return removeBack();
        }
        if (index < (this.size >> 1)) {
            node = this.first;
            for(int i = 0; i < index; i++){
                node = node.next;
            }
            node.next.prev = node.prev;
            node.prev.next = node.next;
        } else {
            node = this.last;
            for (int i = this.size - 1; i > index; i--){
                node = node.prev;
            }
            node.prev.next = node.next;
            node.next.prev = node.prev;
        }
        this.size--;
        return node.value;
    }

    @Override
    public boolean remove(T element) {
        for(Node node = this.first; node != null; node = node.next){
            if(node.value.equals(element)){
                if(size == 1){
                    removeForward();
                } else if(node == this.first){
                    removeForward();
                } else if(node == this.last){
                    removeBack();
                } else {
                    node.prev.next = node.next;
                    node.next.prev = node.prev;
                    size--;
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public T get(int index) {
        if(index < 0 || index >= this.size){
            throw new IndexOutOfBoundsException();
        }
        Node node;
        if(index < (this.size >> 1)){
            node = this.first;
            for(int i = 0; i < index; i++){
                node = node.next;
            }
        } else {
            node = this.last;
            for (int i = this.size - 1; i > index; i--){
                node = node.prev;
            }
        }
        return node.value;
    }

    @Override
    public boolean set(int index, T element) {
        if(index < 0 || index >= this.size){
            throw new IndexOutOfBoundsException();
        }
        Node node;
        if(index < (this.size >> 1)){
            node = this.first;
            for(int i = 0; i < index; i++){
                node = node.next;
            }
        } else {
            node = this.last;
            for (int i = this.size - 1; i > index; i++){
                node = node.prev;
            }
        }
        node.value = element;
        return true;
    }

    @Override
    public boolean contains(T element) {
        for (Node node = this.first; node != null; node = node.next){
            if(node.value.equals(element)) {
                return true;
            }
        }
        return false;
    }


    @SuppressWarnings("unchecked")
    private T cast(Object o) {
        return (T) o;
    }

    @SuppressWarnings("unchecked")
    private <T> T[] castArray(Object[] objects) {
        return (T[]) objects;
    }

}
