package com.github.collections.list;

import java.util.NoSuchElementException;

public class GenericArrayList<T extends Comparable<?>> implements IGenericList<T>{

    private Object[] array;
    private int capacity;
    private int size;

    public GenericArrayList() {
        this.capacity = 10;
        this.array = new Object[this.capacity];
        this.size = 0;
    }

    public GenericArrayList(int capacity) {
        if (capacity < 0) {
            throw new IllegalArgumentException();
        }
        this.capacity = capacity;
        this.array = new Object[this.capacity];
        this.size = 0;
    }

    public GenericArrayList(T[] array) {
        if (array == null) {
            throw new NullPointerException();
        }
        this.size = array.length;
        this.capacity = array.length * 3 / 2;
        this.array = new Object[this.capacity];
        for (int i = 0; i < array.length; i++) {
            this.array[i] = array[i];
        }
    }

    @Override
    public void clear() {
        this.capacity = 10;
        this.array = new Object[10];
        this.size = 0;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public <T> T[] toArray() {
        Object[] result = new Object[this.size];
        for (int i = 0; i < this.size; i++) {
            result[i] = this.array[i];
        }
        return castArray(result);
    }

    @Override
    public void pushForward(T element) {
        this.resize();
        this.size++;
        for (int i = size; i > 0; i--) {
            this.array[i] = this.array[i - 1];
        }
        this.array[0] = element;
    }

    @Override
    public void pushBack(T element) {
        this.resize();
        this.size++;
        this.array[this.size - 1] = element;
    }

    @Override
    public void push(int index, T element) {
        if (index > size || index < 0) {
            throw new IndexOutOfBoundsException();
        }
        this.resize();
        this.size++;
        for (int i = this.size; i > index; i--) {
            this.array[i] = this.array[i - 1];
        }
        this.array[index] = element;
    }

    @Override
    public T removeForward() {
        Object tmp = this.array[0];
        this.size--;
        for (int i = 0; i < this.size; i++) {
            this.array[i] = this.array[i + 1];
        }
        return cast(tmp);
    }

    @Override
    public T removeBack() {
        Object tmp = this.array[this.size - 1];
        this.size--;
        return cast(tmp);
    }

    @Override
    public T removeByIndex(int index) {
        if (index > size || index < 0){
            throw new IndexOutOfBoundsException();
        }
        Object tmp = this.array[index];
        size--;
        for(int i = index; i < this.size; i++){
            this.array[i] = this.array[i + 1];
        }
        return cast(tmp);
    }

    @Override
    public boolean remove(T element) {
        for(int i = 0; i < this.size; i++){
            if(this.array[i].equals(element)){
                removeByIndex(i);
                return true;
            }
        }
        return false;
    }

    @Override
    public T get(int index) {
        if(index < 0 || index > this.size){
            throw new IndexOutOfBoundsException();
        }
        return cast(this.array[index]);
    }

    @Override
    public boolean set(int index, T element) {
        if(index < 0 || index > this.size){
            throw new IndexOutOfBoundsException();
        }
        this.array[index] = element;
        return true;
    }

    @Override
    public boolean contains(T element) {
        for(int i = 0; i < this.size; i++){
            if(this.array[i].equals(element)){
                return true;
            }
        }
        return false;
    }

    private void resize() {
        boolean isResized = false;
        while (this.size + 1 > this.capacity * 7 / 10) {
            this.capacity = this.capacity * 3 / 2 + 1;
            isResized = true;
        }
        if (isResized) {
            Object[] tmp = new Object[this.capacity];
            for (int i = 0; i < this.size; i++) {
                tmp[i] = this.array[i];
            }
            this.array = tmp;
        }
    }

    @SuppressWarnings("unchecked")
    private T cast(Object o){
        return (T) o;
    }

    @SuppressWarnings("unchecked")
    private <T> T[] castArray(Object[] objects){
        return (T[]) objects;
    }

}
