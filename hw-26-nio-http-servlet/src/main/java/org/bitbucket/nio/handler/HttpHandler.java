package org.bitbucket.nio.handler;

import org.bitbucket.nio.exception.IllegalMethodException;
import org.bitbucket.nio.request.INioHttpRequest;
import org.bitbucket.nio.response.INioHttpResponse;

public class HttpHandler implements IHttpHandler {

    public HttpHandler() {
    }

    @Override
    public void service(INioHttpRequest request, INioHttpResponse response){
        switch (request.getMethod()){
            case "GET":
                this.doGet(request, response);
                break;
            case "HEAD":
                this.doHead(request, response);
                break;
            case "POST":
                this.doPost(request, response);
                break;
            case "PUT":
                this.doPut(request, response);
                break;
            case "DELETE":
                this.doDelete(request, response);
                break;
            case "CONNECT":
                this.doConnect(request, response);
                break;
            case "OPTIONS":
                this.doOptions(request, response);
                break;
            case "TRACE":
                this.doTrace(request, response);
                break;
            case "PATCH":
                this.doPatch(request, response);
                break;
            default:
                throw new IllegalMethodException("Method " + request.getMethod() + " is allowed.");
        }
    }

    @Override
    public void doGet(INioHttpRequest request, INioHttpResponse response) {
        String outString = "<h1>Get method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
        response.setBody(outString);
    }

    @Override
    public void doHead(INioHttpRequest request, INioHttpResponse response) {
        String outString = "<h1>Head method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
    }

    @Override
    public void doPost(INioHttpRequest request, INioHttpResponse response) {
        String outString = "<h1>Post method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
        response.setBody(outString);
    }

    @Override
    public void doPut(INioHttpRequest request, INioHttpResponse response) {
        String outString = "<h1>Put method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
        response.setBody(outString);
    }

    @Override
    public void doDelete(INioHttpRequest request, INioHttpResponse response) {
        String outString = "<h1>Delete method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
        response.setBody(outString);
    }

    @Override
    public void doConnect(INioHttpRequest request, INioHttpResponse response) {
        String outString = "<h1>Connect method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
        response.setBody(outString);
    }

    @Override
    public void doOptions(INioHttpRequest request, INioHttpResponse response) {
        String outString = "<h1>Options method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
        response.setBody(outString);
    }

    @Override
    public void doTrace(INioHttpRequest request, INioHttpResponse response) {
        String outString = "<h1>Trace method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
        response.setBody(outString);
    }

    @Override
    public void doPatch(INioHttpRequest request, INioHttpResponse response) {
        String outString = "<h1>Patch method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
        response.setBody(outString);
    }

}
