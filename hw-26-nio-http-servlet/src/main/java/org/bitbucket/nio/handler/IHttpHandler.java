package org.bitbucket.nio.handler;


import org.bitbucket.nio.request.INioHttpRequest;
import org.bitbucket.nio.response.INioHttpResponse;

public interface IHttpHandler {

    void service(INioHttpRequest request, INioHttpResponse response);

    void doGet(INioHttpRequest request, INioHttpResponse response);

    void doHead(INioHttpRequest request, INioHttpResponse response);

    void doPost(INioHttpRequest request, INioHttpResponse response);

    void doPut(INioHttpRequest request, INioHttpResponse response);

    void doDelete(INioHttpRequest request, INioHttpResponse response);

    void doConnect(INioHttpRequest request, INioHttpResponse response);

    void doOptions(INioHttpRequest request, INioHttpResponse response);

    void doTrace(INioHttpRequest request, INioHttpResponse response);

    void doPatch(INioHttpRequest request, INioHttpResponse response);

}
