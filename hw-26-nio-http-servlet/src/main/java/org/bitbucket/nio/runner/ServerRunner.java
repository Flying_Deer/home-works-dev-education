package org.bitbucket.nio.runner;

import org.bitbucket.nio.exception.IllegalMethodException;
import org.bitbucket.nio.exception.ServerRunnerException;
import org.bitbucket.nio.handler.IHttpHandler;
import org.bitbucket.nio.request.INioHttpRequest;
import org.bitbucket.nio.request.NioHttpRequest;
import org.bitbucket.nio.response.INioHttpResponse;
import org.bitbucket.nio.response.NioHttpResponse;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Objects;

public class ServerRunner implements Runnable {

    private final int port;

    IHttpHandler handler;

    private boolean isRunning = true;

    private ServerSocketChannel channel;

    public ServerRunner(int port, IHttpHandler handler) {
        this.port = port;
        this.handler = handler;
    }

    private void startServer(){
        try {
            this.channel = ServerSocketChannel.open();
            this.channel.bind(new InetSocketAddress(this.port));
        } catch (IOException e){
            throw new ServerRunnerException(e.getMessage());
        }

        System.out.println("Server has started with port " + this.port);
    }

    @Override
    public void run() {

        this.startServer();

        INioHttpRequest request = new NioHttpRequest();
        INioHttpResponse response = new NioHttpResponse();
        SocketChannel channel = null;

        while (this.isRunning){
            try {
                channel = this.channel.accept();
                request.takeRequest(channel);
                this.handler.service(request, response);
                response.sendResponse(channel);
                channel.finishConnect();
            } catch (IOException | IllegalMethodException ignore){
            } finally {
                if(Objects.nonNull(channel)) {
                    try {
                        channel.close();
                    } catch (IOException ignore) { }
                }
            }

        }
    }
}
