package org.bitbucket.nio;

import org.bitbucket.nio.handler.HttpHandler;
import org.bitbucket.nio.runner.ServerRunner;

public class NioServletApp {

    public static void main(String[] args) {
        ServerRunner serverRunner = new ServerRunner(8080, new HttpHandler());
        Thread serverThread = new Thread(serverRunner, "Server-thread");
        serverThread.start();
    }

}
