package org.bitbucket.nio.exception;

public class IllegalMethodException extends RuntimeException {

    public IllegalMethodException() {
    }

    public IllegalMethodException(String message) {
        super(message);
    }

}
