package org.bitbucket.nio.exception;

public class ServerRunnerException extends RuntimeException {

    public ServerRunnerException() {
    }

    public ServerRunnerException(String message) {
        super(message);
    }

}
