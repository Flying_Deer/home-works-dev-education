package org.bitbucket.nio.response;


import java.io.IOException;
import java.nio.channels.SocketChannel;
import java.util.Map;

public interface INioHttpResponse {

    void setProtocol(String protocol);

    void setStatusCode(int statusCode);

    void setStatusText(String statusText);

    void setHeaders(Map<String, String> headers);

    void setBody(String body);

    void addHeader(String header, String value);

    void sendResponse(SocketChannel socketChannel) throws IOException;

}
