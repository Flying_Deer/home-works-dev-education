package org.bitbucket.nio.request;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class NioHttpRequest implements INioHttpRequest{

    private String method;

    private String protocol;

    private final Map<String, String> headers = new HashMap<>();

    private String body;

    private final int byteBufferCapacity = 512;

    public NioHttpRequest() {
    }

    @Override
    public String getMethod() {
        return this.method;
    }

    @Override
    public String getProtocol() {
        return this.protocol;
    }

    @Override
    public Map<String, String> getHeaders() {
        return this.headers;
    }

    @Override
    public String getBody() {
        return this.body;
    }

    @Override
    public void takeRequest(SocketChannel channel) throws IOException {
        String req = this.readRequest(channel);
        this.initialize(req);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(this.method).append(" / ").append(this.protocol).append("\n\r");
        for(String header : this.headers.keySet()){
            builder.append(header).append(": ").append(this.headers.get(header)).append("\n\r");
        }
        builder.append("\n\r");
        builder.append(this.body).append("\n\r\n\r");
        return builder.toString();

    }

    private String readRequest(SocketChannel channel) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        ByteBuffer byteBuffer = ByteBuffer.allocate(this.byteBufferCapacity);
        for(int bytes = this.byteBufferCapacity; bytes == this.byteBufferCapacity; ){
            bytes = channel.read(byteBuffer);
            if(bytes == -1){
                throw new IOException();
            }
            byteBuffer.flip();
            stringBuilder.append(new String(byteBuffer.array()));
        }
        byteBuffer.clear();
        return stringBuilder.toString();
    }

    private void initialize(String req){
        String[] arrayReq = req.split("\n");
        this.initFirstString(arrayReq[0]);
        int delimiterStringIndex = this.findDelimiterStringIndex(arrayReq);
        initHeaders(arrayReq, delimiterStringIndex);
        initBody(arrayReq, delimiterStringIndex);
    }

    private void initFirstString(String firstString){
        String[] buffer = firstString.split("/");
        this.method = buffer[0].trim();
        this.protocol = buffer[1].trim();
        if(buffer.length == 3) {
            this.protocol += "/" + buffer[2].trim();
        }
    }

    private int findDelimiterStringIndex(String[] arrayReq){
        for(int i = 1; i < arrayReq.length; i++){
            if (arrayReq[i].trim().length() == 0){
                return i;
            }
        }
        return arrayReq.length;
    }

    private void initHeaders(String[] arrayReq, int delimiterStringIndex){
        for(int i = 1; i < delimiterStringIndex; i++){
            String[] buffer = arrayReq[i].split(":");
            this.headers.put(buffer[0].trim(), buffer[1].trim());
        }
    }

    private void initBody(String[] arrayReq, int delimiterStringIndex){
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = delimiterStringIndex + 1; i < arrayReq.length; i++){
            if(i != delimiterStringIndex + 1){
                stringBuilder.append("\n");
            }
            stringBuilder.append(arrayReq[i]);
        }
        this.body = stringBuilder.toString();
    }

}
