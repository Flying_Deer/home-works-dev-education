package org.bitbucket.nio.request;

import java.io.IOException;
import java.nio.channels.SocketChannel;
import java.util.Map;

public interface INioHttpRequest {

    String getMethod();

    String getProtocol();

    Map<String, String> getHeaders();

    String getBody();

    void takeRequest(SocketChannel socket) throws IOException;

}
