package com.github.strings;

import java.util.ArrayList;

public class Task2 {

    public static String intToString(int num){
        StringBuilder builder = new StringBuilder();
        if(num < 0) {
            builder.append('-');
            num = - num;
        }
        ArrayList<Integer> revers = new ArrayList<>();
        do {
            revers.add(num % 10);
            num /= 10;
        }while (num != 0);
        for(int i = revers.size() - 1; i >= 0; i--){
            builder.append(revers.get(i).toString());
        }
        return builder.toString();
    }

    public static String floatToString(float num, int accuracy){
        if(accuracy < 0){
            throw new IllegalArgumentException();
        }
        StringBuilder result = new StringBuilder();
        if(num < 0) {
            result.append('-');
            num = -num;
        }
        int intPart = (int) num;
        result.append(intToString(intPart));
        if(accuracy == 0){
            return result.toString();
        }
        result.append(".");
        int buff = 0;
        for (int i = 0; i != accuracy; i++){
            buff = (int) num * 10;
            num *= 10;
            num -= buff;
            result.append(intToString((int) num));
        }
        return result.toString();
        /*double factor = Math.pow(10, accuracy);
        num *= factor;
        intPart *= factor;
        num -= intPart;
        intPart = (int) num;
        result.append(intToString(intPart));
        return result.toString();*/
    }

    public static int stringToInt(String num){
        int index = 0;
        boolean neg = false;
        int res = 0;
        int buff;
        if(num.charAt(index) == '-'){
            neg = true;
            index++;
        }
        for(; index < num.length(); index++){
            res *= 10;
            buff = num.charAt(index) - '0';
            if(buff < 0 || buff > 9){
                throw new IllegalArgumentException();
            }
            res += buff;
        }
        if(neg)
            res = -res;
        return res;
    }

    public static float stringToFloat(String num){
        float result = 0;
        boolean isNegative = num.startsWith("-");
        if(!num.contains(".")){
            return stringToInt(num);
        }
        float factor = 0.1f;
        if(isNegative){
            factor = -factor;
        }
        result += stringToInt(num.substring(0, num.indexOf(".")));
        for(int i = num.indexOf(".") + 1; i < num.length(); i++){
            if(num.charAt(i) < '0' || num.charAt(i) > '9'){
                throw new IllegalArgumentException();
            }
            result += factor * (num.charAt(i) - '0');
            factor *= 0.1f;
        }
        return result;
    }
}
