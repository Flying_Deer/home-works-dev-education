package com.github.strings;

import org.junit.Assert;
import org.junit.Test;

public class Task2Test {

    @Test
    public void intToStringPosTest(){
        String exp = "1";
        String act = Task2.intToString(1);
        Assert.assertEquals(exp,act);
    }

    @Test
    public void intToStringNegTest(){
        String exp = "-1";
        String act = Task2.intToString(-1);
        Assert.assertEquals(exp,act);
    }

    @Test
    public void intToStringZeroTest(){
        String exp = "0";
        String act = Task2.intToString(0);
        Assert.assertEquals(exp,act);
    }

    @Test
    public void intToStringBigPosTest(){
        String exp = "124";
        String act = Task2.intToString(124);
        Assert.assertEquals(exp,act);
    }

    @Test
    public void intToStringBigNegTest(){
        String exp = "-124";
        String act = Task2.intToString(-124);
        Assert.assertEquals(exp,act);
    }

    //-----------------------------------------------------------------

    @Test
    public void floatToStringPosAcc0(){
        String exp = "1";
        String act = Task2.floatToString(1, 0);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void floatToStringPosAcc1(){
        String exp = "5.0";
        String act = Task2.floatToString(5, 1);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void floatToStringPosAcc10(){
        String exp = "8.0000000000";
        String act = Task2.floatToString(8, 10);
        Assert.assertEquals(exp, act);
    }

    //fails because of float
    @Test
    public void floatToStringBigPosAcc4(){
        String exp = "13.1234";
        String act = Task2.floatToString(13.1234f, 4);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void floatToStringNegAcc0(){
        String exp = "-1";
        String act = Task2.floatToString(-1, 0);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void floatToStringNegAcc1(){
        String exp = "-5.0";
        String act = Task2.floatToString(-5, 1);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void floatToStringNegAcc10(){
        String exp = "-8.0000000000";
        String act = Task2.floatToString(-8, 10);
        Assert.assertEquals(exp, act);
    }

    //fails because of float
    @Test
    public void floatToStringBigNegAcc4(){
        String exp = "-13.1234";
        String act = Task2.floatToString(-13.1234f, 4);
        Assert.assertEquals(exp, act);
    }

    //-----------------------------------------------------------------

    @Test
    public void stringToIntPosTest(){
        int exp = 5;
        int act = Task2.stringToInt("5");
        Assert.assertEquals(exp,act);
    }

    @Test
    public void stringToIntNegTest(){
        int exp = -5;
        int act = Task2.stringToInt("-5");
        Assert.assertEquals(exp,act);
    }

    @Test
    public void stringToIntZeroTest(){
        int exp = 0;
        int act = Task2.stringToInt("0");
        Assert.assertEquals(exp,act);
    }

    @Test
    public void stringToIntBigPosTest(){
        int exp = 545;
        int act = Task2.stringToInt("545");
        Assert.assertEquals(exp,act);
    }

    @Test
    public void stringToIntBigNegTest(){
        int exp = -545;
        int act = Task2.stringToInt("-545");
        Assert.assertEquals(exp,act);
    }

    //-------------------------------------------------------

    @Test
    public void stringToFloatZero(){
        float exp = 0;
        float act = Task2.stringToFloat("0");
        Assert.assertEquals(exp, act, 0);
    }

    @Test
    public void stringToFloatOne(){
        float exp = 1.1f;
        float act = Task2.stringToFloat("1.1");
        Assert.assertEquals(exp, act, 0.01);
    }

    @Test
    public void stringToFloatTwo(){
        float exp = 52.87f;
        float act = Task2.stringToFloat("52.87");
        Assert.assertEquals(exp, act, 0.001);
    }

    @Test
    public void stringToFloatNeg(){
        float exp = -1.1f;
        float act = Task2.stringToFloat("-1.1");
        Assert.assertEquals(exp, act, 0.01);
    }

    @Test
    public void stringToFloatBigNeg(){
        float exp = -10.01f;
        float act = Task2.stringToFloat("-10.01");
        Assert.assertEquals(exp, act, 0.001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void stringToFloatNAN(){
        float act = Task2.stringToFloat("-f");
    }
}
