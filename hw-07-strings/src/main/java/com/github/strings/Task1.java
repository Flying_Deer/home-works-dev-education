package com.github.strings;

public class Task1 {



    public static void printAZ(){
        for (char c = 'A'; c <= 'Z'; c++){
            ConsolePrinter.print(c + " ");
        }
        ConsolePrinter.print("\n");
    }

    public static void printaz(){
        for (char c = 'a'; c <= 'z'; c++){
            ConsolePrinter.print(c + " ");
        }
        ConsolePrinter.print("\n");
    }

    public static void printRu(){
        for (char c = 'а'; c <= 'я'; c++){
            ConsolePrinter.print(c + " ");
        }
        ConsolePrinter.print("\n");
    }

    public static void printNum(){
        for (char c = '0'; c <= '9'; c++){
            ConsolePrinter.print(c + " ");
        }
        ConsolePrinter.print("\n");
    }

    public static void printASCII(){
        for (char c = 32; c < 127; c++){
            ConsolePrinter.print(c + " ");
        }
        ConsolePrinter.print("\n");
    }


}
