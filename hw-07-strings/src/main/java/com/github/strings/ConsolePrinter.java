package com.github.strings;

public class ConsolePrinter {

    public static void printLine(String s){
        System.out.println(s);
    }

    public static void print(String s){
        System.out.print(s);
    }
}
