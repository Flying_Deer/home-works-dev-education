package com.github.websocket;

import com.github.websocket.exceptions.DecodingException;
import com.github.websocket.utils.BitConverter;

import java.nio.charset.StandardCharsets;

public class Decoder {

    public static String decode(byte[] bytes) {
        boolean fin = (bytes[0] & 0b10000000) != 0,
                mask = (bytes[1] & 0b10000000) != 0;
        int opcode = bytes[0] & 0b00001111;
        int msglen = bytes[1] + 0b10000000;
        int offset;
        if(msglen < 126) {
            offset = 2;
        } else {
            msglen = BitConverter.toInt16(new byte[]{bytes[3], bytes[2]});
            offset = 4;
        }

        if (msglen == 0) {
            throw new DecodingException("msglen == 0");
        } else if (mask) {
            byte[] decoded = new byte[msglen];
            byte[] masks = new byte[]{bytes[offset], bytes[offset + 1], bytes[offset + 2], bytes[offset + 3]};
            offset += 4;

            for (int i = 0; i < msglen; ++i) {
                decoded[i] = (byte) (bytes[offset + i] ^ masks[i % 4]);
            }
            return new String(decoded, StandardCharsets.UTF_8);
        } else {
            throw new DecodingException("mask bit not set");
        }
    }
}
