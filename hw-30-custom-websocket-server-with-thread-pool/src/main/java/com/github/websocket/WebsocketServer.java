package com.github.websocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class WebsocketServer implements Runnable {

    private final int port;

    private static final Logger log = LoggerFactory.getLogger(WebsocketServer.class);

    public WebsocketServer(int port) {
        this.port = port;
    }


    @Override
    public void run() {
        ExecutorService threadPool = new ThreadPoolExecutor(0, 5, 60L, TimeUnit.SECONDS, new SynchronousQueue<>());
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            log.info("Created server socket on the port: {}", port);
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    Socket clientSocket = serverSocket.accept();
                    threadPool.submit(new WebsocketHandler(clientSocket));
                } catch (IOException e) {
                    log.error("Exception: {}, Message: {}", e.getClass(), e.getMessage());
                }
            }
        } catch (IOException e) {
            log.error("Exception: {}, Message: {}", e.getClass(), e.getMessage());
        } finally {
            threadPool.shutdown();
        }
    }
}
