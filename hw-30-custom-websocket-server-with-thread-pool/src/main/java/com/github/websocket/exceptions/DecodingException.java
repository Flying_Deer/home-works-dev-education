package com.github.websocket.exceptions;

public class DecodingException extends RuntimeException{

    public DecodingException() {
    }

    public DecodingException(String message) {
        super(message);
    }

}
