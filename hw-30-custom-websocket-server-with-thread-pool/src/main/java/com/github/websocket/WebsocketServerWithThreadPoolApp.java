package com.github.websocket;

public class WebsocketServerWithThreadPoolApp {

    public static void main(String[] args) {
        WebsocketServer server = new WebsocketServer(80);
        Thread serverThread = new Thread(server);
        serverThread.start();
    }

}
