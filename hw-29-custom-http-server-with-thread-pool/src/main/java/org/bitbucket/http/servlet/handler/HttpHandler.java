package org.bitbucket.http.servlet.handler;

import org.bitbucket.http.servlet.exceptions.EmptyRequestException;
import org.bitbucket.http.servlet.exceptions.IllegalMethodException;
import org.bitbucket.http.servlet.request.ISimpleHttpRequest;
import org.bitbucket.http.servlet.request.SimpleHttpRequest;
import org.bitbucket.http.servlet.response.ISimpleHttpResponse;
import org.bitbucket.http.servlet.response.SimpleHttpResponse;

import java.io.IOException;
import java.net.Socket;

public class HttpHandler implements IHttpHandler {

    private final Socket clientSocket;

    public HttpHandler(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    @Override
    public void service(ISimpleHttpRequest request, ISimpleHttpResponse response){
        switch (request.getMethod()){
            case "GET":
                this.doGet(request, response);
                break;
            case "HEAD":
                this.doHead(request, response);
                break;
            case "POST":
                this.doPost(request, response);
                break;
            case "PUT":
                this.doPut(request, response);
                break;
            case "DELETE":
                this.doDelete(request, response);
                break;
            case "CONNECT":
                this.doConnect(request, response);
                break;
            case "OPTIONS":
                this.doOptions(request, response);
                break;
            case "TRACE":
                this.doTrace(request, response);
                break;
            case "PATCH":
                this.doPatch(request, response);
                break;
            default:
                throw new IllegalMethodException("Method " + request.getMethod() + " is allowed.");
        }
    }

    @Override
    public void doGet(ISimpleHttpRequest request, ISimpleHttpResponse response) {
        String outString = "<h1>Get method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
        response.setBody(outString);
    }

    @Override
    public void doHead(ISimpleHttpRequest request, ISimpleHttpResponse response) {
        String outString = "<h1>Head method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
    }

    @Override
    public void doPost(ISimpleHttpRequest request, ISimpleHttpResponse response) {
        String outString = "<h1>Post method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
        response.setBody(outString);
    }

    @Override
    public void doPut(ISimpleHttpRequest request, ISimpleHttpResponse response) {
        String outString = "<h1>Put method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
        response.setBody(outString);
    }

    @Override
    public void doDelete(ISimpleHttpRequest request, ISimpleHttpResponse response) {
        String outString = "<h1>Delete method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
        response.setBody(outString);
    }

    @Override
    public void doConnect(ISimpleHttpRequest request, ISimpleHttpResponse response) {
        String outString = "<h1>Connect method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
        response.setBody(outString);
    }

    @Override
    public void doOptions(ISimpleHttpRequest request, ISimpleHttpResponse response) {
        String outString = "<h1>Options method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
        response.setBody(outString);
    }

    @Override
    public void doTrace(ISimpleHttpRequest request, ISimpleHttpResponse response) {
        String outString = "<h1>Trace method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
        response.setBody(outString);
    }

    @Override
    public void doPatch(ISimpleHttpRequest request, ISimpleHttpResponse response) {
        String outString = "<h1>Patch method was called.</h1>\n";
        response.setProtocol("HTTP/1.1");
        response.setStatusCode(200);
        response.setStatusText("OK");
        response.addHeader("Content-Length", "" + outString.length());
        response.addHeader("Content-Type", "text/html");
        response.setBody(outString);
    }

    @Override
    public void run() {
        ISimpleHttpRequest request = new SimpleHttpRequest();
        ISimpleHttpResponse response = new SimpleHttpResponse();
            try {
                request.takeRequest(clientSocket);
                this.service(request, response);
                response.sendResponse(clientSocket);
            } catch (IOException | EmptyRequestException | IllegalMethodException ignore){
            }
    }

}
