package org.bitbucket.http.servlet.request;

import java.io.IOException;
import java.net.Socket;
import java.util.Map;

public interface ISimpleHttpRequest {

    String getMethod();

    String getProtocol();

    Map<String, String> getHeaders();

    String getBody();

    void takeRequest(Socket socket) throws IOException;

}
