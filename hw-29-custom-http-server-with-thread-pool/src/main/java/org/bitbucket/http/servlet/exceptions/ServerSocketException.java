package org.bitbucket.http.servlet.exceptions;

public class ServerSocketException extends RuntimeException{

    public ServerSocketException() {
    }

    public ServerSocketException(String message) {
        super(message);
    }

}
