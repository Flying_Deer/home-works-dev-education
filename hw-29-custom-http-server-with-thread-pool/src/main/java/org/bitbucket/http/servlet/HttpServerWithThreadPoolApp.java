package org.bitbucket.http.servlet;

public class HttpServerWithThreadPoolApp {

    public static void main(String[] args) {
        Server server = new Server(8000);
        Thread serverThread = new Thread(server);
        serverThread.start();
    }

}
