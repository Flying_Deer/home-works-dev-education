package com.github.randomizer;

public class MathRand {

    //Pseudo-random Numbers Generator
    //This function generates numbers in range 0-99999
    public static int rand(){
        long Seed = System.nanoTime();
        return Math.abs((int)(Seed * 73129 + 95121) % 100000);
    }

    public static int rand(int min, int max){
        int Number = rand();
        Number = min + Number % (max - min + 1);
        return Number;
    }

}
