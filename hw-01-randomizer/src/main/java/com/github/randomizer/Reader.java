package com.github.randomizer;

import java.util.Scanner;

public class Reader {

    public static int readMin(){
        Scanner Read = new Scanner(System.in);
        String Buffer;
        int Min;
        int[] SomeArray = {1}; // argument for exit(...) function

        Messages.askNumberMessage(VariableNames.Min);
        while(true){
            Buffer = Read.nextLine();
            Buffer = Buffer.toLowerCase();
            if(Buffer.equals(Command.Exit)){
                SomeArray = Command.exit(SomeArray);
                if (SomeArray.length == 0){
                    return Dialog.ExitNumber;
                }
                Messages.askNumberMessage(VariableNames.Min);
                continue;
            }
            else if(Buffer.equals(Command.Help)){
                Messages.helloMessage();
                Messages.askNumberMessage(VariableNames.Min);
                continue;
            }
            try{
                Min = Integer.parseInt(Buffer);
                if(!Validation.isValidRange(Min)){
                    Messages.wrongNumberMessage();
                }
                else
                    return Min;
            }
            catch(NumberFormatException nfe){
                Messages.wrongNumberMessage();
            }
        }
    }

    public static int readMax(){
        Scanner Read = new Scanner(System.in);
        String Buffer;
        int Max;
        int[] SomeArray = {1}; // argument for exit(...) function

        Messages.askNumberMessage(VariableNames.Max);
        while(true){
            Buffer = Read.nextLine();
            Buffer = Buffer.toLowerCase();
            if(Buffer.equals(Command.Exit)){
                SomeArray = Command.exit(SomeArray);
                if (SomeArray.length == 0){
                    return Dialog.ExitNumber;
                }
            }
            else if(Buffer.equals(Command.Help)){
                Messages.helloMessage();
                Messages.askNumberMessage(VariableNames.Max);
                continue;
            }
            try{
                Max = Integer.parseInt(Buffer);
                if(!Validation.isValidRange(Max)){
                    Messages.wrongNumberMessage();
                }
                else
                    return Max;
            }
            catch(NumberFormatException nfe){
                Messages.wrongNumberMessage();
            }
        }
    }

    public static int[] readCommand(int[] NumbersToGenerate){
        Scanner Read = new Scanner(System.in);
        String Buffer;
        Buffer = Read.nextLine();
        Buffer = Buffer.toLowerCase();
        if(Buffer.equals(Command.Exit))
            return Command.exit(NumbersToGenerate);
        else if(Buffer.equals(Command.Help)){
            Messages.helloMessage();
            return NumbersToGenerate;
        }
        else if(Buffer.equals(Command.Generate))
            return Command.generate(NumbersToGenerate);
        else{
            Messages.validCommandsMessage();
            return NumbersToGenerate;
        }
    }

}
