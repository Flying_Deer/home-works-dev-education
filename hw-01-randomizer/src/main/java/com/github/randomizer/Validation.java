package com.github.randomizer;

public class Validation {
    public static boolean isValidMinMax(int Min, int Max){
        if(Min > Max){
            System.out.println("Min number must not be greater then Max number.");
            System.out.println("Enter valid numbers.");
            return false;
        }
        return true;
    }

    public static  boolean isValidRange(int Range){
        return (Range >= RangeConstants.Min && Range <= RangeConstants.Max);
    }
}
