package com.github.randomizer;

import java.util.Scanner;

public class Command {

    final public  static  String Exit = "exit";
    final  public static  String Help = "help";
    final  public  static  String Generate = "generate";

    public static int[] exit(int[] NumbersToGenerate){
        Scanner Read = new Scanner(System.in);
        String Buffer;
        int[] Empty = {};
        while(true){
            Messages.quitConfirmMessage();
            Buffer = Read.nextLine();
            Buffer = Buffer.toLowerCase();
            if(Buffer.equals(ConfirmConstant.Yes)) {
                Messages.exitMessage();
                return Empty;
            }
            else if(Buffer.equals(ConfirmConstant.No)) {
                return NumbersToGenerate;
            }
        }
    }

    public static int[] generate(int[] NumbersToGenerate){
        int RandomIndex = MathRand.rand(0, NumbersToGenerate.length - 1);
        int RandomNumber = NumbersToGenerate[RandomIndex];
        NumbersToGenerate = ArrayElements.removeElement(NumbersToGenerate, RandomIndex);
        Messages.randomNumber(RandomNumber);
        if(NumbersToGenerate.length == 0){
            Messages.noMoreNumbers();
        }
        return NumbersToGenerate;
    }
}
