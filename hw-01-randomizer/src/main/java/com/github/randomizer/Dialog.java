package com.github.randomizer;
import java.util.Scanner;

public class Dialog {

    final public static int ExitNumber = -7777;

    public static  void  randomizer(){

        int Min = 0, Max = 0;
        int[] Empty = {};
        Messages.helloMessage();

        do{
            Min = Reader.readMin();
            if(Min == ExitNumber)
                break;
            Max = Reader.readMax();
            if(Max == ExitNumber)
                break;
        } while(!Validation.isValidMinMax(Min, Max));
        //Array of numbers in range, that wasn't generated yet
        int[] NumbersToGenerate = new int[Max - Min + 1];
        if(Min == ExitNumber || Max == ExitNumber){
            NumbersToGenerate = Empty;
        }
        else {
            for (int i = 0; i <= Max - Min; i++) {
                NumbersToGenerate[i] = Min + i;
            }
        }
        while(NumbersToGenerate.length != 0){
            NumbersToGenerate = Reader.readCommand(NumbersToGenerate);
        }
    }
}
