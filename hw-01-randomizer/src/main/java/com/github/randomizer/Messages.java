package com.github.randomizer;

public class Messages {

    public static void helloMessage(){
        System.out.println("*****************************************************************************************************");
        System.out.println("Hi there!");
        System.out.println("Welcome to the Randomizer program.");
        System.out.println("This program generates pseudorandom numbers in a certain range.");
        System.out.println("Generated numbers will never repeat in one session.");
        System.out.println("First you need to choose a range.");
        System.out.println("Both minimum and maximum must be in range from 1 to 500.");
        System.out.println("And of course minimum must not be greater then maximum.");
        System.out.println("You can quit anytime if you enter \"exit\".");
        System.out.println("If you want to read this message again, enter \"help\".");
        System.out.println("After defining Min and Max numbers you can generate new random numbers with command \"generate\".");
        System.out.println("Have fun!");
        System.out.println("*****************************************************************************************************");
    }

    public static void wrongNumberMessage(){
        System.out.println("Min and Max numbers must be integers from 1 to 500.");
        System.out.println("Please enter a valid number.");
    }

    public static  void quitConfirmMessage(){
        System.out.println("Are you sure you want to quit the app? (" + ConfirmConstant.Yes + "/" + ConfirmConstant.No + ")");
    }

    public  static  void askNumberMessage(String Name){
        System.out.println("Please enter " + Name + " value");
    }

    public static void validCommandsMessage(){
        System.out.println("Valid commands are: \"" + Command.Generate + "\" , \"" + Command.Help + "\" and \"" + Command.Exit);
        System.out.println("Please enter a valid command.");
    }

    public static void noMoreNumbers(){
        System.out.println("There is no more numbers to generate.");
        System.out.println("Thanks for choosing our app.");
    }

    public static void exitMessage(){
        System.out.println("Thanks for choosing our app.");
    }

    public static void randomNumber(int Number){
        System.out.println("Random number is:\t" + Number);
    }
}
