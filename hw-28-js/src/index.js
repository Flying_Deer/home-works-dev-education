const data = [
    {
        "username": "Jeannie_23",
        "name": "Jeannie Randolph",
        "pass": "$3z&Se8Ph8",
    },
    {
        "username": "k_brown",
        "name": "Kathie Brown",
        "pass": "*7Gf5iNy7@",
    },
    {
        "username": "mega_powers",
        "name": "Powers Justice",
        "pass": "8*DkE87jb&",
    },
    {
        "username": "o.neal",
        "name": "Morales Oneal",
        "pass": "68Zp6-@vUp",
    },
    {
        "username": "A-dela",
        "name": "Adela Heath",
        "pass": "83-2pgJSj$",
    }
];
const root = document.getElementById('root');

const form = document.createElement('form');
form.setAttribute('class', 'form');

const form__modal_wrapper = document.createElement('div');
form__modal_wrapper.setAttribute('class', 'form__modal_wrapper');

const firstName = document.createElement('input');
firstName.setAttribute('class', 'form__firstName form__input');
firstName.setAttribute('placeholder', 'firstname');

const lastName = document.createElement('input');
lastName.setAttribute('class', 'form__lastName form__input');
lastName.setAttribute('placeholder', 'lastname');

const password = document.createElement('input');
password.setAttribute('class', 'form__password form__input');
password.setAttribute('type', 'password');
password.setAttribute('placeholder', 'password');
password.setAttribute('required', 'true');

const btn = document.createElement('button');
btn.setAttribute('class', 'form__btn');
btn.setAttribute('type', 'button');

const textnode = document.createTextNode('authorization');

btn.append(textnode);

form__modal_wrapper.append(firstName);
form__modal_wrapper.append(lastName);
form__modal_wrapper.append(password);
form__modal_wrapper.append(btn);

form.append(form__modal_wrapper);

root.append(form);

firstName.addEventListener('input', (e) => {
    const regex = /^[A-Za-z-]/;
    let checkFName = regex.test(lastName.value);
    if (e.target.value.length < 2) {
        firstName.style.borderColor = 'red';
        firstName.style.boxShadow = '0 0 5px 5px red';
    } else {
        firstName.style.borderColor = 'green';
        firstName.style.boxShadow = '0 0 10px 2px green';
    }
    if (e.target.value.length > 10) {
        firstName.value = e.target.value.substr(0, 10);
    }
    if (checkFName !== true) {
        firstName.style.borderColor = 'red';
        firstName.style.boxShadow = '0 0 5px 5px red';
    }
})

lastName.addEventListener('input', (e) => {
    const regex = /^[A-Za-z-]/;
    let checkLName = regex.test(lastName.value);
    if (e.target.value.length < 2) {
        lastName.style.borderColor = 'red';
        lastName.style.boxShadow = '0 0 5px 5px red';
    } else {
        lastName.style.borderColor = 'green';
        lastName.style.boxShadow = '0 0 10px 2px green';
    }
    if (e.target.value.length > 20) {
        lastName.value = e.target.value.substr(0, 20);
    }
    if (checkLName !== true) {
        lastName.style.borderColor = 'red';
        lastName.style.boxShadow = '0 0 5px 5px red';
    }

})

password.addEventListener('input', (e) => {
    const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$&@*-])[A-Za-z\d$&@*-]/;
    let checkPass = regex.test(password.value);
    if (e.target.value.length < 5) {
        password.style.borderColor = 'red';
        password.style.boxShadow = '0 0 5px 5px red';
    } else {
        password.style.borderColor = 'green';
        password.style.boxShadow = '0 0 10px 2px green';
    }
    if (e.target.value.length > 10) {
        password.value = e.target.value.substr(0, 10);
    }
    if (checkPass !== true) {
        password.style.borderColor = 'red';
        password.style.boxShadow = '0 0 5px 5px red';
    }


})

btn.addEventListener('click', (e) => {
    sendData()
});

function sendData() {
    let firstNameValue = firstName.value;
    let lastNameValue = lastName.value;
    let passwordValue = password.value;

    let fieldsCounter = 0;
    const singleField = 'Поле';
    const multipleFields = 'Поля';
    const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$&@*-])[A-Za-z\d$&@*-]{5,10}$/;
    let fiedls = '';
    const fullName = firstNameValue + " " + lastNameValue;
    let checkReg = regex.test(password.value);
    if (checkReg !== true) {
        password.style.border = '1px solid red'
        password.style.boxShadow = '0 0 3px 3px red'
    } else {
        password.style.borderColor = 'green';
    }
    if (firstName.value === '' || lastName.value === '' || checkReg !== true) {
        let fieldsToShow = '';
        let alertString = '';
        if (firstNameValue === '') {
            fieldsToShow += 'firstname, '
            fieldsCounter++;
        }
        if (lastNameValue === '') {
            fieldsToShow += 'lastname, '
            fieldsCounter++;
        }
        if (passwordValue === '') {
            fieldsToShow += 'password '
            fieldsCounter++;
        } else if (checkReg === false) {
            fieldsToShow += 'пароль должен состоять только из букв, цифр и спецсимволов $, &, @, *'
        }
        if (fieldsCounter < 2) {
            alertString = singleField + ' ' + fieldsToShow + 'обязательное к заполнению';
        } else {
            alertString = multipleFields + ' ' + fieldsToShow + 'обязательные к заполнению';
        }
        alert('Ошибка валидации: \n' + alertString);
    } else {
        auth(fullName, passwordValue);
    }
}


function auth(name, password) {
    let resp = false;
    let i = 0;
    let userName = '';
    while (i < data.length) {
        if (data[i].name === name && data[i].pass === password) {
            userName = data[i].username;
            resp = true;
            i = data.length + 1;
        }
        i++;
    }
    if (resp === true) {
        alert('Вы успешно авторизовались как: ' + userName + ", " + name);
    } else {
        alert('Ошибка аутентификации');
    }
}

window.addEventListener('keypress', (e) => {
    if (e.key === 'Enter') {
        sendData();
    }
})