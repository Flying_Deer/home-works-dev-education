package org.bitbucket.trees.impl;

import org.bitbucket.trees.ITree;

import java.util.Objects;

public class RecursiveAVLTree implements ITree {

    private static class Node {

        int value;
        Node left;
        Node right;
        int height;
        Node parent;

        public Node(int value, Node parent) {
            this.value = value;
            this.left = null;
            this.right = null;
            int height = 0;
            this.parent = parent;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node node = (Node) o;
            return value == node.value && Objects.equals(left, node.left) && Objects.equals(right, node.right) && Objects.equals(parent, node.parent);
        }

        @Override
        public int hashCode() {
            return Objects.hash(value);
        }

        private void toArray(int[] array, Counter counter){

            if (Objects.nonNull(this.left)) {
                this.left.toArray(array, counter);
            }
            array[counter.counter] = this.value;
            counter.increment();
            if (Objects.nonNull(this.right)) {
                this.right.toArray(array, counter);
            }

        }

        private Node add(int value){
            if (value < this.value) {
                if (this.left == null) {
                    this.height--;
                    this.left = new Node(value, this);
                    return this.left;
                } else {
                    this.height--;
                    return this.left.add(value);
                }
            } else if (value > this.value) {
                if (this.right == null) {
                    this.height++;
                    this.right = new Node(value, this);
                    return this.right;
                } else {
                    this.height++;
                    return this.right.add(value);
                }
            } else {
                throw new IllegalArgumentException();
            }
        }

        private void rebalance(){
            if(this.height > 1 && this.right.height < 0 ){
                this.RLRotation();
            } else if(this.height > 1){
                this.RRotation();
            } else if(this.height < -1 && this.left.height > 0){
                this.LRRotation();
            } else if(this.height < -1){
                this.LRotation();
            }
        }

        private void RLRotation(){
            Node a = this;
            Node b = this.right;
            Node c = this.right.left;
            Node m = this.right.left.left;
            Node n = this.right.left.right;
            if(Objects.equals(this, this.parent.left)){
                this.parent.left = c;
            } else {
                this.parent.right = c;
            }
            c.parent = this.parent;
            c.right = a;
            a.parent = c;
            c.left = b;
            b.parent = c;
            a.right = m;
            m.parent = a;
            b.left = n;
            n.parent = b;
        }

        private void RRotation(){
            Node a = this;
            Node b = this.right;
            Node c = this.right.left;
            if(Objects.equals(this, this.parent.left)){
                this.parent.left = b;
            } else {
                this.parent.right = b;
            }
            a.parent = b;
            b.left = a;
            a.left = c;
            c.parent = a;
        }

        private void LRRotation(){
            Node a = this;
            Node b = this.left;
            Node c = this.left.right;
            Node m = this.left.right.left;
            Node n = this.left.right.right;
            if(Objects.equals(this, this.parent.left)){
                this.parent.left = c;
            } else {
                this.parent.right = c;
            }

            c.parent = this.parent;
            c.left = b;
            b.parent = c;
            c.right = a;
            a.parent = c;
            b.right = m;
            m.parent = b;
            a.left = n;
            n.parent = a;
        }

        private void LRotation(){
            Node a = this;
            Node b = this.left;
            Node c = this.left.right;
            if(Objects.equals(this, this.parent.left)){
                this.parent.left = b;
            } else {
                this.parent.right = b;
            }
            b.parent = this.parent;
            b.right = a;
            a.parent = b;
            a.left = c;
        }

    }

    private Node root;
    private int size;

    public RecursiveAVLTree(int[] array){
        this.init(array);
    }

    public RecursiveAVLTree() {
        this.init(new int[0]);
    }

    @Override
    public void init(int[] array) {
        this.clear();
        if (hasRepeatingElements(array)) {
            throw new IllegalArgumentException();
        }
        for (int element : array) {
            this.add(element);
        }
    }

    private boolean hasRepeatingElements(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] == array[j]) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void clear() {
        this.size = 0;
        this.root = null;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public int[] toArray() {
        int[] result = new int[this.size];
        if (this.size == 0) {
            return result;
        }
        Counter counter = new Counter();
        this.root.toArray(result, counter);
        return result;
    }

    @Override
    public void add(int value) {
        if (this.size == 0) {
            this.root = new Node(value, null);
            this.size++;
            return;
        }
        Node node = this.root.add(value);
        size++;
        while (node != null){
            node.rebalance();
            node = node.parent;
        }
    }

    @Override
    public void delete(int value) {

    }

    @Override
    public int width() {
        return 0;
    }

    @Override
    public int height() {
        return 0;
    }

    @Override
    public int nodes() {
        return 0;
    }

    @Override
    public int leaves() {
        return 0;
    }

    @Override
    public void reverse() {

    }

    @Override
    public boolean contains(int val){
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecursiveAVLTree that = (RecursiveAVLTree) o;
        return size == that.size && Objects.equals(root, that.root);
    }

    @Override
    public int hashCode() {
        return Objects.hash(root, size);
    }

    private static class Counter {

        int counter;

        public Counter(int counter) {
            this.counter = counter;
        }

        public Counter() {
            this.counter = 0;
        }

        void increment() {
            counter++;
        }

    }

}
