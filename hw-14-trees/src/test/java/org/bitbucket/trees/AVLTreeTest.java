package org.bitbucket.trees;

import org.bitbucket.trees.impl.ProcedureAVLTree;
import org.bitbucket.trees.impl.ProcedureBinaryTree;
import org.bitbucket.trees.impl.RecursiveAVLTree;
import org.bitbucket.trees.impl.RecursiveBinaryTree;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class AVLTreeTest {

    private final String name;

    private final ITree tree;

    public AVLTreeTest(String name, ITree tree) {
        this.name = name;
        this.tree = tree;
    }

    @Parameterized.Parameters(name = "{index} {0}")
    public static Collection<Object> instances() {
        return Arrays.asList(new Object[][]{
                {"Recursive AVL tree", new RecursiveAVLTree()},
                {"Procedure AVL tree", new ProcedureAVLTree()}
        });
    }
    @Before
    public void setUp() {
        tree.clear();
    }

    //=================================================
    //================== Width ========================
    //=================================================

    @Test
    public void widthZero() {
        int[] initialArray = {};
        tree.init(initialArray);
        int exp = 0;
        int act = tree.width();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void widthOne() {
        int[] initialArray = {0};
        tree.init(initialArray);
        int exp = 1;
        int act = tree.width();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void widthTwo() {
        int[] initialArray = {0, 1};
        tree.init(initialArray);
        int exp = 1;
        int act = tree.width();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void widthThree() {
        int[] initialArray = {1, 0, 2};
        tree.init(initialArray);
        int exp = 2;
        int act = tree.width();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void widthMany() {
        int[] initialArray = {8, 4, 12, 2, 6, 10, 14, 1, 3, 5, 7, 9, 11, 13, 15};
        tree.init(initialArray);
        int exp = 8;
        int act = tree.width();
        Assert.assertEquals(exp, act);
    }

    //=================================================
    //================== Height =======================
    //=================================================

    @Test
    public void heightZero() {
        int[] initialArray = {};
        tree.init(initialArray);
        int exp = 0;
        int act = tree.height();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void heightOne() {
        int[] initialArray = {0};
        tree.init(initialArray);
        int exp = 1;
        int act = tree.height();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void heightTwo() {
        int[] initialArray = {0, 1};
        tree.init(initialArray);
        int exp = 2;
        int act = tree.height();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void heightThree() {
        int[] initialArray = {1, 0, 2};
        tree.init(initialArray);
        int exp = 2;
        int act = tree.height();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void heightMany() {
        int[] initialArray = {8, 4, 12, 2, 6, 10, 14, 1, 3, 5, 7, 9, 11, 13, 15};
        tree.init(initialArray);
        int exp = 4;
        int act = tree.height();
        Assert.assertEquals(exp, act);
    }

    //=================================================
    //================== Leaves =======================
    //=================================================

    @Test
    public void leavesZero() {
        int[] initialArray = {};
        tree.init(initialArray);
        int exp = 0;
        int act = tree.leaves();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void leavesOne() {
        int[] initialArray = {0};
        tree.init(initialArray);
        int exp = 1;
        int act = tree.leaves();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void leavesTwo() {
        int[] initialArray = {0, 1};
        tree.init(initialArray);
        int exp = 1;
        int act = tree.leaves();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void leavesThree() {
        int[] initialArray = {1, 0, 2};
        tree.init(initialArray);
        int exp = 2;
        int act = tree.leaves();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void leavesMany() {
        int[] initialArray = {8, 4, 12, 2, 6, 10, 14, 1, 3, 5, 7, 9, 11, 13, 15};
        tree.init(initialArray);
        int exp = 8;
        int act = tree.leaves();
        Assert.assertEquals(exp, act);
    }

    //=================================================
    //================== Nodes ========================
    //=================================================

    @Test
    public void nodesZero() {
        int[] initialArray = {};
        tree.init(initialArray);
        int exp = 0;
        int act = tree.nodes();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void nodesOne() {
        int[] initialArray = {0};
        tree.init(initialArray);
        int exp = 0;
        int act = tree.nodes();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void nodesTwo() {
        int[] initialArray = {0, 1};
        tree.init(initialArray);
        int exp = 1;
        int act = tree.nodes();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void nodesThree() {
        int[] initialArray = {1, 0, 2};
        tree.init(initialArray);
        int exp = 1;
        int act = tree.nodes();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void nodesMany() {
        int[] initialArray = {8, 4, 12, 2, 6, 10, 14, 1, 3, 5, 7, 9, 11, 13, 15};
        tree.init(initialArray);
        int exp = 7;
        int act = tree.nodes();
        Assert.assertEquals(exp, act);
    }

}
