package com.github.mathRandomDrawing;

import java.util.Arrays;

public class MyRandom {

    private static double random(){
        return Math.random();
    }

    private static int random(int min, int max){
        return (int) (min + (max - min) * random());
    }

    private static double random(double min, double max){
        return min + (max - min) * random();
    }

//===================================================================

    public static void printRandom(){
        System.out.println("Your random number is " + random());
    }

//====================================================================

    public static void print10Random(){
        double[] output = new double[10];
        for( int i = 0; i < output.length; i++){
            output[i] = random();
        }
        System.out.println("Your 10 random numbers are:\n" + Arrays.toString(output));
    }

//=====================================================================

    public static void print10RandomInIntRange(int min, int max){
        int[] output = new int[10];
        for( int i = 0; i < output.length; i++){
            output[i] = random(min, max);
        }
        System.out.println("Your 10 random numbers are:\n" + Arrays.toString(output));
    }

    public static void print10RandomInDoubleRange(double min, double max){
        double[] output = new double[10];
        for( int i = 0; i < output.length; i++){
            output[i] = random(min, max);
        }
        System.out.println("Your 10 random numbers are:\n" + Arrays.toString(output));
    }

//=====================================================================================

    public static void printRandomNumberOfRandomIntNumbers(int min, int max){
        int size = random(3, 15);
        int[] output = new int[size];
        for( int i = 0; i < size; i++){
            output[i] = random(min, max);
        }
        System.out.println("Your random numbers are:\n" + Arrays.toString(output));
    }

    public static void printRandomNumberOfRandomDoubleNumbers(double min, double max){
        int size = random(3, 15);
        double[] output = new double[size];
        for( int i = 0; i < size; i++){
            output[i] = random(min, max);
        }
        System.out.println("Your random numbers are:\n" + Arrays.toString(output));
    }



}
