package com.github.mathRandomDrawing;

public class MyMath{

//=======================================================================================

    public static double howitzerDistanceRadian(double angle, double velocity){
        double gravitationalAcceleration = 9.8f;
        return velocity * velocity * Math.sin(2 * angle) / gravitationalAcceleration;
    }

    public static double howitzerDistanceDegree(double angle, double velocity){
        double gravitationalAcceleration = 9.8f;
        double angleInRadians = Math.toRadians(angle);
        return velocity * velocity * Math.sin(2 * angleInRadians) / gravitationalAcceleration;
    }

//=======================================================================================

    public static double distanceAuto(double velocity1, double velocity2, double distanceStart, double time){
        return distanceStart + velocity1 * time + velocity2 * time;
    }

//=======================================================================================

    private static double upperBorder(double x){
        if(x < 0) {
            return -x;
        }
        else {
            return x;
        }
    }

    private static double lowerBorder(double x){
        if(x < 0){
            return -3.d / 2.d * x - 1;
        }
        else {
            return 3.d / 2.d * x - 1;
        }
    }

    public static boolean inArea(double x, double y){
        if(x < -2 || x > 2){
            return false;
        }
        else return y <= upperBorder(x) && y >= lowerBorder(x);
    }

//=========================================================================================

    public static double calculateExpression(double x){
        return 6.d * Math.log(Math.sqrt(Math.exp(x + 1) + 2 * Math.exp(x) * Math.cos(x))) /
                Math.log(x - Math.exp(x + 1) * Math.sin(x)) +
                Math.abs(Math.cos(x) / Math.exp(Math.sin(x)));
    }

//===========================================================================================

}
