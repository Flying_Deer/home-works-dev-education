package com.github.mathRandomDrawing;

public class Draw {

    private static final int size = 7;

    private static void print(boolean[][] matrix){
        for(int i = 0; i < size; i++){
            for (int j = 0; j < size; j++){
                if(matrix[i][j]) {
                    System.out.print(" * ");
                }
                else{
                    System.out.print("   ");
                }
            }
            System.out.print("\n");
        }
    }

    public static void task1(){
        // *  *  *  *  *  *  *
        // *  *  *  *  *  *  *
        // *  *  *  *  *  *  *
        // *  *  *  *  *  *  *
        // *  *  *  *  *  *  *
        // *  *  *  *  *  *  *
        // *  *  *  *  *  *  *
        boolean[][] star = new boolean[size][size];
        for(int i = 0; i < size; i++){
            for(int j = 0; j < size; j++){
                star[i][j] = true;
            }
        }
        print(star);
    }

    public static void task2(){
        // *  *  *  *  *  *  *
        // *                 *
        // *                 *
        // *                 *
        // *                 *
        // *                 *
        // *  *  *  *  *  *  *
        boolean[][] star = new boolean[size][size];
        for(int i = 0; i < size; i++){
            for(int j = 0; j < size; j++){
                star[i][j] =
                        i == 0 || i == size - 1 ||
                        j == 0 || j == size - 1;
            }
        }
        print(star);
    }

    public static void task3(){
        // *  *  *  *  *  *  *
        // *              *
        // *           *
        // *        *
        // *     *
        // *  *
        // *
        boolean[][] star = new boolean[size][size];
        for(int i = 0; i < size; i++){
            for(int j = 0; j < size; j++){
                star[i][j] =
                        i == 0 || j == 0 ||
                        i == size - j - 1;
            }
        }
        print(star);
    }

    public static void task4(){
        // *
        // *  *
        // *     *
        // *        *
        // *           *
        // *              *
        // *  *  *  *  *  *  *

        boolean[][] star = new boolean[size][size];
        for(int i = 0; i < size; i++){
            for(int j = 0; j < size; j++){
                star[i][j] =
                        i == size - 1 || j == 0 ||
                                i == j;
            }
        }
        print(star);
    }

    public static void task5(){
        //                   *
        //                *  *
        //             *     *
        //          *        *
        //       *           *
        //    *              *
        // *  *  *  *  *  *  *

        boolean[][] star = new boolean[size][size];
        for(int i = 0; i < size; i++){
            for(int j = 0; j < size; j++){
                star[i][j] =
                        i == size - 1 || j == size - 1 ||
                                i == size - j - 1;
            }
        }
        print(star);
    }

    public static void task6(){
        // *  *  *  *  *  *  *
        //    *              *
        //       *           *
        //          *        *
        //             *     *
        //                *  *
        //                   *

        boolean[][] star = new boolean[size][size];
        for(int i = 0; i < size; i++){
            for(int j = 0; j < size; j++){
                star[i][j] =
                        i == 0 || j == size - 1 ||
                                i == j;
            }
        }
        print(star);
    }

    public static void task7(){
        // *                 *
        //    *           *
        //       *     *
        //          *
        //       *     *
        //    *           *
        // *                 *

        boolean[][] star = new boolean[size][size];
        for(int i = 0; i < size; i++){
            for(int j = 0; j < size; j++){
                star[i][j] =
                        i == j || i == size - j - 1;
            }
        }
        print(star);
    }

    public static void task8(){
        // *  *  *  *  *  *  *
        //    *           *
        //       *     *
        //          *
        //
        //
        //

        boolean[][] star = new boolean[size][size];
        for(int i = 0; i < size; i++){
            for(int j = 0; j < size; j++){
                star[i][j] =
                        i == 0 ||
                        (i == j  && i < size / 2 + 1) ||
                        (i == size - j - 1 && i < size / 2 + 1);
            }
        }
        print(star);
    }

    public static void task9(){
        //
        //
        //
        //          *
        //       *     *
        //    *           *
        // *  *  *  *  *  *  *

        boolean[][] star = new boolean[size][size];
        for(int i = 0; i < size; i++){
            for(int j = 0; j < size; j++){
                star[i][j] =
                        i == size - 1 ||
                                (i == j  && i > size / 2 - 1) ||
                                (i == size - j - 1 && i > size / 2 - 1);
            }
        }
        print(star);
    }

}
