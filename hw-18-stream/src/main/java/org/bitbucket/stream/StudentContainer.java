package org.bitbucket.stream;

import org.bitbucket.stream.entity.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StudentContainer {

    private final List<Student> students = new ArrayList<>();

    public StudentContainer(List<Student> students) {
        init(students);
    }

    public void init(List<Student> students){
        this.students.clear();
        students.forEach(student -> this.students.add(new Student(student)));
    }

    public List<Student> getStudents() {
        return students;
    }

    public List<Student> byFaculty(String faculty){

        return this.students.
                stream()
                .filter(s -> s.getFaculty().equals(faculty))
                .collect(Collectors.toList());

    }

    public List<Student> byFacultyAndCourse(String faculty, String course){

        return this.students
                .stream()
                .filter(s -> s.getFaculty().equals(faculty))
                .filter(s -> s.getCourse().equals(course))
                .collect(Collectors.toList());

    }

    public List<Student> bornAfter(int year){

        return this.students
                .stream()
                .filter(s -> s.getYearOfBirth() > year)
                .collect(Collectors.toList());

    }

    public List<String> getGroup(String group){

        return this.students
                .stream()
                .filter(s -> s.getGroup().equals(group))
                .flatMap(s -> Stream.of(s.getFirstName() + " " + s.getLastName()))
                .collect(Collectors.toList());

    }

    public long studentsOnFaculty(String faculty){

        return this.students
                .stream()
                .filter(s -> s.getFaculty().equals(faculty))
                .count();

    }

    public List<Student> changeGroup(String fromGroup, String toGroup){
        return this.students
                .stream()
                .peek(s -> {
                    if(s.getGroup().equals(fromGroup)){
                        s.setGroup(toGroup);
                    }
                })
                .collect(Collectors.toList());
    }

    public List<Student> changeFaculty(String fromFaculty, String toFaculty){
        return this.students
                .stream()
                .peek(s -> {
                    if(s.getFaculty().equals(fromFaculty)){
                        s.setFaculty(toFaculty);
                    }
                })
                .collect(Collectors.toList());
    }

    public String studentsToString(){
        return this.students
                .stream()
                .flatMap(s -> Stream.of(s.getFirstName() + " " + s.getLastName() + " - "
                + s.getFaculty() + " " + s.getGroup() + "\n"))
                .reduce("", (partialString, element) -> partialString + element);

    }

    public Map<String, List<Student>> groupByFaculty(){
        return this.students
                .stream()
                .collect(Collectors.groupingBy(Student::getFaculty));
    }

    public Map<String, List<Student>> groupByCourse(){
        return this.students
                .stream()
                .collect(Collectors.groupingBy(Student::getCourse));
    }

    public Map<String, List<Student>> groupByGroup(){
        return this.students
                .stream()
                .collect(Collectors.groupingBy(Student::getGroup));
    }

    public boolean checkAllFaculty(String faculty){
        return this.students
                .stream()
                .allMatch(s -> s.getFaculty().equals(faculty));
    }

    public boolean checkOneFaculty(String faculty){
        return this.students
                .stream()
                .anyMatch(s -> s.getFaculty().equals(faculty));
    }

    public boolean checkAllGroup(String group){
        return this.students
                .stream()
                .allMatch(s -> s.getGroup().equals(group));
    }

    public boolean checkOneGroup(String group){
        return this.students
                .stream()
                .anyMatch(s -> s.getGroup().equals(group));
    }

}
