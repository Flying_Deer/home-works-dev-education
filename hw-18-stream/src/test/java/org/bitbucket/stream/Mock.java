package org.bitbucket.stream;

import org.bitbucket.stream.entity.Student;

import java.util.ArrayList;
import java.util.List;

public class Mock {

    public static List<Student> mockStudents = new ArrayList<>();

    static {
        mockStudents.add(new Student(
                1, "Ivan", "Ivanov", 1990,
                "Ivanova, 1", "+380501111111", "A",
                "First", "First group"
        ));

        mockStudents.add(new Student(
                2, "Petr", "Petrov", 2000,
                "Ivanova, 1", "+380501111112", "A",
                "First", "First group"
        ));

        mockStudents.add(new Student(
                1, "Sidor", "Karas", 1990,
                "Petrova, 1", "+380501111117", "B",
                "First", "First group"
        ));

        mockStudents.add(new Student(
                1, "Ivan", "Karas", 1995,
                "Sidorova, 1", "+380501111115", "C",
                "First", "First group"
        ));

        mockStudents.add(new Student(
                1, "Mark", "Ivanov", 1999,
                "Sidorova, 1", "+380501111113", "A",
                "Last", "First group"
        ));

        mockStudents.add(new Student(
                1, "Ernest", "Ivanov", 2002,
                "Petrova, 1", "+380501111123", "B",
                "Second", "Second group"
        ));
    }

}
