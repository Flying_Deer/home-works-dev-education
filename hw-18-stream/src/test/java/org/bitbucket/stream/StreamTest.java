package org.bitbucket.stream;

import org.bitbucket.stream.entity.Student;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

public class StreamTest {

    private final StudentContainer data = new StudentContainer(Mock.mockStudents);

    List<Student> exp = new ArrayList<>();

    List<Student> act = new ArrayList<>();

    @Before
    public void setUp(){
        exp.clear();
        act.clear();
        data.init(Mock.mockStudents);
    }

    @Test(expected = NullPointerException.class)
    public void streamNull(){
        data.init(null);
    }

    @Test
    public void byFacultyA(){
        exp.add(new Student(
                1, "Ivan", "Ivanov", 1990,
                "Ivanova, 1", "+380501111111", "A",
                "First", "First group"
        ));

        exp.add(new Student(
                2, "Petr", "Petrov", 2000,
                "Ivanova, 1", "+380501111112", "A",
                "First", "First group"
        ));
        exp.add(new Student(
                1, "Mark", "Ivanov", 1999,
                "Sidorova, 1", "+380501111113", "A",
                "Last", "First group"
        ));
        act = data.byFaculty("A");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void byFacultyB(){
        exp.add(new Student(
                1, "Sidor", "Karas", 1990,
                "Petrova, 1", "+380501111117", "B",
                "First", "First group"
        ));
        exp.add(new Student(
                1, "Ernest", "Ivanov", 2002,
                "Petrova, 1", "+380501111123", "B",
                "Second", "Second group"
        ));
        act = data.byFaculty("B");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void byFacultyC(){
        exp.add(new Student(
                1, "Ivan", "Karas", 1995,
                "Sidorova, 1", "+380501111115", "C",
                "First", "First group"
        ));
        act = data.byFaculty("C");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void byFacultyZero(){
        act = data.byFaculty("Zero");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void byFacultyAndCourseAFirst(){
        exp.add(new Student(
                1, "Ivan", "Ivanov", 1990,
                "Ivanova, 1", "+380501111111", "A",
                "First", "First group"
        ));
        exp.add(new Student(
                2, "Petr", "Petrov", 2000,
                "Ivanova, 1", "+380501111112", "A",
                "First", "First group"
        ));
        act = data.byFacultyAndCourse("A", "First");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void byFacultyAndCourseALast(){
        exp.add(new Student(
                1, "Mark", "Ivanov", 1999,
                "Sidorova, 1", "+380501111113", "A",
                "Last", "First group"
        ));
        act = data.byFacultyAndCourse("A", "Last");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void byFacultyAndCourseBSecond(){
        exp.add(new Student(
                1, "Ernest", "Ivanov", 2002,
                "Petrova, 1", "+380501111123", "B",
                "Second", "Second group"
        ));
        act = data.byFacultyAndCourse("B", "Second");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void byFacultyAndCourseZero(){
        act = data.byFacultyAndCourse("V", "Third");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void bornAfterAll(){
        exp.addAll(data.getStudents());
        act = data.bornAfter(0);
        Assert.assertEquals(exp, act);
    }
    
    @Test
    public void BornAfter1993(){
        exp.add(new Student(
                2, "Petr", "Petrov", 2000,
                "Ivanova, 1", "+380501111112", "A",
                "First", "First group"
        ));
        exp.add(new Student(
                1, "Ivan", "Karas", 1995,
                "Sidorova, 1", "+380501111115", "C",
                "First", "First group"
        ));

        exp.add(new Student(
                1, "Mark", "Ivanov", 1999,
                "Sidorova, 1", "+380501111113", "A",
                "Last", "First group"
        ));

        exp.add(new Student(
                1, "Ernest", "Ivanov", 2002,
                "Petrova, 1", "+380501111123", "B",
                "Second", "Second group"
        ));
        act = data.bornAfter(1993);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void BornAfter2005(){
        act = data.bornAfter(2005);
        Assert.assertEquals(exp, act);
    }
    
    @Test
    public void getGroupFirst(){
        List<String> expectedString = new ArrayList<>();
        expectedString.add("Ivan Ivanov");
        expectedString.add("Petr Petrov");
        expectedString.add("Sidor Karas");
        expectedString.add("Ivan Karas");
        expectedString.add("Mark Ivanov");
        List<String> actualString = data.getGroup("First group");
        Assert.assertEquals(expectedString, actualString);
    }

    @Test
    public void getGroupSecond(){
        List<String> expectedString = new ArrayList<>();
        expectedString.add("Ernest Ivanov");
        List<String> actualString = data.getGroup("Second group");
        Assert.assertEquals(expectedString, actualString);
    }

    @Test
    public void getGroupZero(){
        List<String> expectedString = new ArrayList<>();
        List<String> actualString = data.getGroup("Zero");
        Assert.assertEquals(expectedString, actualString);
    }

    @Test
    public void studentsOnFacultyA(){
        long exp = 3;
        long act = data.studentsOnFaculty("A");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void studentsOnFacultyB(){
        long exp = 2;
        long act = data.studentsOnFaculty("B");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void studentsOnFacultyC(){
        long exp = 1;
        long act = data.studentsOnFaculty("C");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void studentsOnFacultyZero(){
        long exp = 0;
        long act = data.studentsOnFaculty("Zero");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void changeGroupSecondFirst(){
        exp.add(new Student(
                1, "Ivan", "Ivanov", 1990,
                "Ivanova, 1", "+380501111111", "A",
                "First", "First group"
        ));

        exp.add(new Student(
                2, "Petr", "Petrov", 2000,
                "Ivanova, 1", "+380501111112", "A",
                "First", "First group"
        ));

        exp.add(new Student(
                1, "Sidor", "Karas", 1990,
                "Petrova, 1", "+380501111117", "B",
                "First", "First group"
        ));

        exp.add(new Student(
                1, "Ivan", "Karas", 1995,
                "Sidorova, 1", "+380501111115", "C",
                "First", "First group"
        ));

        exp.add(new Student(
                1, "Mark", "Ivanov", 1999,
                "Sidorova, 1", "+380501111113", "A",
                "Last", "First group"
        ));

        exp.add(new Student(
                1, "Ernest", "Ivanov", 2002,
                "Petrova, 1", "+380501111123", "B",
                "Second", "First group"
        ));

        act = data.changeGroup("Second group", "First group");

        Assert.assertEquals(exp, act);

    }

    @Test
    public void changeGroupFirstSecond(){
        exp.add(new Student(
                1, "Ivan", "Ivanov", 1990,
                "Ivanova, 1", "+380501111111", "A",
                "First", "Second group"
        ));

        exp.add(new Student(
                2, "Petr", "Petrov", 2000,
                "Ivanova, 1", "+380501111112", "A",
                "First", "Second group"
        ));

        exp.add(new Student(
                1, "Sidor", "Karas", 1990,
                "Petrova, 1", "+380501111117", "B",
                "First", "Second group"
        ));

        exp.add(new Student(
                1, "Ivan", "Karas", 1995,
                "Sidorova, 1", "+380501111115", "C",
                "First", "Second group"
        ));

        exp.add(new Student(
                1, "Mark", "Ivanov", 1999,
                "Sidorova, 1", "+380501111113", "A",
                "Last", "Second group"
        ));

        exp.add(new Student(
                1, "Ernest", "Ivanov", 2002,
                "Petrova, 1", "+380501111123", "B",
                "Second", "Second group"
        ));

        act = data.changeGroup("First group", "Second group");

        Assert.assertEquals(exp, act);

    }

    @Test
    public void changeFacultyAB(){
        exp.add(new Student(
                1, "Ivan", "Ivanov", 1990,
                "Ivanova, 1", "+380501111111", "B",
                "First", "First group"
        ));

        exp.add(new Student(
                2, "Petr", "Petrov", 2000,
                "Ivanova, 1", "+380501111112", "B",
                "First", "First group"
        ));

        exp.add(new Student(
                1, "Sidor", "Karas", 1990,
                "Petrova, 1", "+380501111117", "B",
                "First", "First group"
        ));

        exp.add(new Student(
                1, "Ivan", "Karas", 1995,
                "Sidorova, 1", "+380501111115", "C",
                "First", "First group"
        ));

        exp.add(new Student(
                1, "Mark", "Ivanov", 1999,
                "Sidorova, 1", "+380501111113", "B",
                "Last", "First group"
        ));

        exp.add(new Student(
                1, "Ernest", "Ivanov", 2002,
                "Petrova, 1", "+380501111123", "B",
                "Second", "Second group"
        ));

        act = data.changeFaculty("A", "B");

        Assert.assertEquals(exp, act);

    }

    @Test
    public void changeFacultyCD(){
        exp.add(new Student(
                1, "Ivan", "Ivanov", 1990,
                "Ivanova, 1", "+380501111111", "A",
                "First", "First group"
        ));

        exp.add(new Student(
                2, "Petr", "Petrov", 2000,
                "Ivanova, 1", "+380501111112", "A",
                "First", "First group"
        ));

        exp.add(new Student(
                1, "Sidor", "Karas", 1990,
                "Petrova, 1", "+380501111117", "B",
                "First", "First group"
        ));

        exp.add(new Student(
                1, "Ivan", "Karas", 1995,
                "Sidorova, 1", "+380501111115", "D",
                "First", "First group"
        ));

        exp.add(new Student(
                1, "Mark", "Ivanov", 1999,
                "Sidorova, 1", "+380501111113", "A",
                "Last", "First group"
        ));

        exp.add(new Student(
                1, "Ernest", "Ivanov", 2002,
                "Petrova, 1", "+380501111123", "B",
                "Second", "Second group"
        ));

        act = data.changeFaculty("C", "D");

        Assert.assertEquals(exp, act);

    }

    @Test
    public void studentsToString(){
        String expectedString = "Ivan Ivanov - A First group\n" +
                "Petr Petrov - A First group\n" +
                "Sidor Karas - B First group\n" +
                "Ivan Karas - C First group\n" +
                "Mark Ivanov - A First group\n" +
                "Ernest Ivanov - B Second group\n";
        String actualString = data.studentsToString();
        Assert.assertEquals(expectedString, actualString);
    }

    @Test
    public void groupByFaculty(){
        Map<String, List<Student>> exp = new HashMap<>();
        exp.put("A", new ArrayList<>());
        exp.put("B", new ArrayList<>());
        exp.put("C", new ArrayList<>());

        exp.get("A").addAll(data.byFaculty("A"));
        exp.get("B").addAll(data.byFaculty("B"));
        exp.get("C").addAll(data.byFaculty("C"));

        Map<String, List<Student>> act = data.groupByFaculty();

        Assert.assertEquals(exp, act);

    }

    private List<Student> byCourse(List<Student> students, String course){
        return students.
                stream()
                .filter(s -> s.getCourse().equals(course))
                .collect(Collectors.toList());
    }

    @Test
    public void groupByCourse(){
        Map<String, List<Student>> exp = new HashMap<>();
        exp.put("First", new ArrayList<>());
        exp.put("Second", new ArrayList<>());
        exp.put("Last", new ArrayList<>());

        exp.get("First").addAll(byCourse(data.getStudents(), "First"));
        exp.get("Second").addAll(byCourse(data.getStudents(), "Second"));
        exp.get("Last").addAll(byCourse(data.getStudents(), "Last"));

        Map<String, List<Student>> act = data.groupByCourse();

        Assert.assertEquals(exp, act);
    }

    private List<Student> byGroup(List<Student> students, String group){
        return students.
                stream()
                .filter(s -> s.getGroup().equals(group))
                .collect(Collectors.toList());
    }

    @Test
    public void groupByGroup(){
        Map<String, List<Student>> exp = new HashMap<>();
        exp.put("First group", new ArrayList<>());
        exp.put("Second group", new ArrayList<>());

        exp.get("First group").addAll(byGroup(data.getStudents(), "First group"));
        exp.get("Second group").addAll(byGroup(data.getStudents(), "Second group"));

        Map<String, List<Student>> act = data.groupByGroup();

        Assert.assertEquals(exp, act);
    }

    @Test
    public void checkAllFacultyTrue(){
        data.changeFaculty("B", "A");
        data.changeFaculty("C", "A");
        Assert.assertTrue(data.checkAllFaculty("A"));
    }

    @Test
    public void checkAllFacultyFalse(){
        Assert.assertFalse(data.checkAllFaculty("A"));
    }

    @Test
    public void checkOneFacultyTrue(){
        Assert.assertTrue(data.checkOneFaculty("A"));
    }

    @Test
    public void checkOneFacultyFalse(){
        Assert.assertFalse(data.checkOneFaculty("X"));
    }

    @Test
    public void checkAllGroupTrue(){
        data.changeGroup("Second group", "First group");
        Assert.assertTrue(data.checkAllGroup("First group"));
    }

    @Test
    public void checkAllGroupFalse(){
        Assert.assertFalse(data.checkAllGroup("First group"));
    }

    @Test
    public void checkOneGroupTrue(){
        Assert.assertTrue(data.checkOneGroup("Second group"));
    }

    @Test
    public void checkOneGroupFalse(){
        Assert.assertFalse(data.checkOneGroup("X group"));
    }

}
