package com.github.simplecalc;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;

public class Interface {

    JFrame calculatorFrame;

    public JTextField firstNumberField = new JTextField();
    public JTextField secondNumberField = new JTextField();
    public JTextField operationField = new JTextField();

    JButton buttonCalculate = new JButton("calculate");

    JTextField resultField = new JTextField();

    Interface() {

        calculatorFrame = new JFrame();

        Color frameColor = new Color(221,226,205);
        calculatorFrame.getContentPane().setBackground(frameColor);

        buttonCalculate.setBackground(frameColor);

        final int frameWidth = 400;
        final int frameHeight = 500;

        final int verticalSpace = 20;
        final int horizontalSpace = 10;

        final int buttonWidth = frameWidth - 3 * horizontalSpace;
        final int buttonHeight = 40;

        final int labelWidth = frameWidth - 2 * horizontalSpace;

        final int textFieldLeft = 100;
        final int textFieldRight = frameWidth - 2 * horizontalSpace;

        final int rowHeight = 40;

        final int secondRowY = verticalSpace + rowHeight + verticalSpace;
        final int thirdRowY = secondRowY + rowHeight + verticalSpace;
        final int forthRowY = thirdRowY + rowHeight + verticalSpace;
        final int fifthRowY = forthRowY + buttonHeight + verticalSpace;

        JLabel firstNumberLabel = new JLabel("Число 1", SwingConstants.RIGHT);
        JLabel secondNumberLabel = new JLabel("Число 2", SwingConstants.RIGHT);
        JLabel operationLabel = new JLabel("Операция", SwingConstants.RIGHT);
        JLabel resultLabel = new JLabel("Результат", SwingConstants.RIGHT);


        firstNumberField.setBounds(textFieldLeft, verticalSpace, textFieldRight - textFieldLeft, rowHeight);
        secondNumberField.setBounds(textFieldLeft, secondRowY, textFieldRight - textFieldLeft, rowHeight);
        operationField.setBounds(textFieldLeft, thirdRowY, textFieldRight - textFieldLeft, rowHeight);

        firstNumberLabel.setBounds(textFieldLeft - horizontalSpace - labelWidth, verticalSpace, labelWidth, rowHeight);
        secondNumberLabel.setBounds(textFieldLeft - horizontalSpace - labelWidth, secondRowY, labelWidth, rowHeight);
        operationLabel.setBounds(textFieldLeft - horizontalSpace - labelWidth, thirdRowY, labelWidth, rowHeight);

        buttonCalculate.setBounds(horizontalSpace, forthRowY, buttonWidth, rowHeight);

        resultField.setBounds(textFieldLeft, fifthRowY, textFieldRight - textFieldLeft, rowHeight);
        resultLabel.setBounds(textFieldLeft - horizontalSpace - labelWidth, fifthRowY, labelWidth, rowHeight);

        calculatorFrame.add(firstNumberLabel);
        calculatorFrame.add(firstNumberField);
        calculatorFrame.add(secondNumberLabel);
        calculatorFrame.add(secondNumberField);
        calculatorFrame.add(operationField);
        calculatorFrame.add(operationLabel);
        calculatorFrame.add(buttonCalculate);
        calculatorFrame.add(resultField);
        calculatorFrame.add(resultLabel);

        calculatorFrame.setSize(frameWidth, frameHeight);
        calculatorFrame.setLayout(null);
        calculatorFrame.setVisible(true);

    }

}
