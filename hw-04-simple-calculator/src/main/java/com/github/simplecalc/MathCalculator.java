package com.github.simplecalc;

public class MathCalculator {

    public static float calculate(float a, float b, String op){
        switch (op){
            case "+":
                return a + b;
            case "-":
                return a - b;
            case "*":
                return a * b;
            case "/":
                return a / b;
            default:
                throw new IllegalArgumentException();
        }
    }

}
