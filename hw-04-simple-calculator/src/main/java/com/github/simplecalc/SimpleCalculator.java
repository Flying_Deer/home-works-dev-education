package com.github.simplecalc;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SimpleCalculator {

    SimpleCalculator(){

        Interface calculator = new Interface();


        calculator.buttonCalculate.addActionListener(e -> {
            float firstNumber = 0;
            float secondNumber = 0;
            float result;
            boolean wrongNumber = false;
            try {
                firstNumber = Float.parseFloat(calculator.firstNumberField.getText());
                secondNumber = Float.parseFloat(calculator.secondNumberField.getText());
            }
            catch (Exception someException){
                calculator.resultField.setText("Wrong number!");
                wrongNumber = true;
            }
            if(!wrongNumber) {
                String operation = calculator.operationField.getText();
                try {
                    result = MathCalculator.calculate(firstNumber, secondNumber, operation);
                    calculator.resultField.setText(String.valueOf(result));
                } catch (Exception someException) {
                    calculator.resultField.setText("Wrong operation!");
                }
            }
        });
    }
}
