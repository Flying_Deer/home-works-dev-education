package org.bitbucket.servlet;

import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;

import javax.servlet.ServletException;
import java.io.File;

public class ServletApp {

    public static void main(String[] args) {

        try {
            Tomcat tomcat = new Tomcat();
            tomcat.setPort(8080);
            Context ctx = tomcat.addWebapp("/", new File(".").getAbsolutePath());
            tomcat.addServlet("", "Handler", Config.getHandler());
            ctx.addServletMappingDecoded("/*", "Handler");
            tomcat.start();
            tomcat.getServer().await();
        } catch (LifecycleException | ServletException e) {
            e.printStackTrace();
        }

    }
}
