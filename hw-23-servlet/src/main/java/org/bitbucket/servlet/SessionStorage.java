package org.bitbucket.servlet;

import org.apache.catalina.Session;

import java.util.HashMap;
import java.util.Map;

public class SessionStorage {

    private SessionStorage(){

    }

    private static final SessionStorage instance = new SessionStorage();

    public static SessionStorage getInstance() {
        return instance;
    }

    private Map<String, Session> storage = new HashMap<>();

    public void put(Session session){
        this.storage.put(session.getId(), session);
    }

    public boolean contains(Session session){
        return this.storage.containsKey(session.getId());
    }

}
