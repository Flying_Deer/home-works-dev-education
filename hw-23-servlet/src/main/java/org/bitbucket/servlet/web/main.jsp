<html lang="en">
<head>
    <title>Login V8</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body>
        <span class="login100-form-title">
            Sign In
        </span>
        <div>
            <label>
                <input class="usernameInput" type="text" name="username" placeholder="Username">
            </label>
        </div>
        <div>
            <label>
                <input class="passwordInput" type="password" name="pass" placeholder="Password">
            </label>
        </div>
        <div>
            <button class="signInButton" onclick="onClick()">
                Sign in
            </button>
        </div>
        <p class="change_link">
            <a href="/main" class="to_register">Main</a>
        </p>
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
        <script>
            function onClick(){
                let username = document.getElementById('usernameInput').value;
                let password = document.getElementById('passwordInput').value;
                let data = {
                    username: username,
                    password: password
                };
                axios.post('http://localhost:8000/auth', data);
            }
        </script>
</body>
</html>
