package org.bitbucket.servlet;

import org.bitbucket.servlet.exceptions.BadRequest;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Collectors;

public class Handler extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp)  throws ServletException, IOException {
        try {
            super.service(req, resp);
        } catch (BadRequest e) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid body");
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        PrintWriter writer = null;
        try {
            String url = req.getRequestURI();
            writer = resp.getWriter();
            String buffer;
            switch (url) {
                case "/auth":
                    resp.setContentType("text/html");
                    buffer = Files.readString(Path.of(System.getProperty("user.dir") + "/hw-23-servlet/src/main/java/org/bitbucket/servlet/web/main.jsp"));
                    resp.setContentLength(buffer.length());
                    writer.write(buffer);
                    break;
                case "/main":
                    break;
            }
        } catch (IOException e) {
            resp.setStatus(500);
        } finally {
            if (writer != null) {
                writer.flush();
                writer.close();
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        PrintWriter writer = null;
        try {
            String url = req.getRequestURI();
            writer = resp.getWriter();
            String buffer;
            switch (url) {
                case "/auth":
                    String body = req.getReader().lines().collect(Collectors.joining());
                    AuthorizationDto dto = JsonHelper.fromJson(body, AuthorizationDto.class)
                            .orElseThrow(BadRequest::new);
                    break;
                case "/main":
                    break;
            }
        } catch (IOException e) {
            resp.setStatus(500);
        } finally {
            if (writer != null) {
                writer.flush();
                writer.close();
            }
        }
    }
}
