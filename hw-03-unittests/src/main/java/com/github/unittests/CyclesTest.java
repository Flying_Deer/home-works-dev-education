package com.github.unittests;

import org.junit.Assert;
import org.junit.Test;

public class CyclesTest {


    //==================================================================
    //=================   sumAndQuantityOfEvens tests ==================
    //==================================================================

    @Test
    public void sumAndQuantityOfEvens(){
        int[] exp = {2450, 49};
        int[] act = Cycles.sumAndQuantityOfEvens(1,99);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void sumAndQuantityOfEvens1_1(){
        int[] exp = {0, 0};
        int[] act = Cycles.sumAndQuantityOfEvens(1,1);
        Assert.assertArrayEquals(exp, act);
    }
    @Test
    public void sumAndQuantityOfEvens_4_2(){
        int[] exp = {-6, 2};
        int[] act = Cycles.sumAndQuantityOfEvens(-4,-2);
        Assert.assertArrayEquals(exp, act);
    }

    //==================================================================
    //=========================   isPrime tests ========================
    //==================================================================

    @Test
    public void isPrime1(){
        boolean exp = false;
        boolean act = Cycles.isPrime(1);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void isPrime2(){
        boolean exp = true;
        boolean act = Cycles.isPrime(2);
        Assert.assertEquals(exp, act);
    }


    @Test
    public void isPrime11(){
        boolean exp = true;
        boolean act = Cycles.isPrime(11);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void isPrime15(){
        boolean exp = false;
        boolean act = Cycles.isPrime(15);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void isPrime0(){
        boolean exp = false;
        boolean act = Cycles.isPrime(0);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void isPrimeNeg(){
        boolean exp = false;
        boolean act = Cycles.isPrime(-15);
        Assert.assertEquals(exp, act);
    }

    //==================================================================
    //=========================   rootConsistent tests =================
    //==================================================================

    @Test
    public void rootConsistent15(){
        int exp = 3;
        int act = Cycles.rootConsistent(15);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void rootConsistent3(){
        int exp = 1;
        int act = Cycles.rootConsistent(3);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void rootConsistent1(){
        int exp = 1;
        int act = Cycles.rootConsistent(1);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void rootConsistent0(){
        int exp = 0;
        int act = Cycles.rootConsistent(0);
        Assert.assertEquals(exp, act);
    }

    @Test(expected = IllegalArgumentException.class)
    public void rootConsistentNeg(){
        int exp = -1;
        int act = Cycles.rootConsistent(-1);
        Assert.assertEquals(exp, act);
    }

    //==================================================================
    //=========================   rootBinary tests =====================
    //==================================================================

    @Test
    public void rootBinary4(){
        int exp = 2;
        int act = Cycles.rootBinary(4);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void rootBinary5(){
        int exp = 2;
        int act = Cycles.rootBinary(5);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void rootBinary48(){
        int exp = 6;
        int act = Cycles.rootBinary(48);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void rootBinary1(){
        int exp = 1;
        int act = Cycles.rootBinary(1);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void rootBinary0(){
        int exp = 0;
        int act = Cycles.rootBinary(0);
        Assert.assertEquals(exp, act);
    }

    @Test(expected = IllegalArgumentException.class)
    public void rootBinaryNeg(){
        int exp = -1;
        int act = Cycles.rootBinary(-1);
        Assert.assertEquals(exp, act);
    }


    //==================================================================
    //=========================   factorial tests ======================
    //==================================================================

    @Test
    public void factorial0(){
        int exp = 1;
        int act = Cycles.factorial(0);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void factorial1(){
        int exp = 1;
        int act = Cycles.factorial(1);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void factorial5(){
        int exp = 120;
        int act = Cycles.factorial(5);
        Assert.assertEquals(exp, act);
    }

    @Test(expected = IllegalArgumentException.class)
    public void factorialNeg(){
        int exp = -1;
        int act = Cycles.rootBinary(-1);
        Assert.assertEquals(exp, act);
    }


    //==================================================================
    //=========================   factorial tests ======================
    //==================================================================

    @Test
    public void sumOfNumerals0(){
        int exp = 0;
        int act = Cycles.sumOfNumerals(0);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void sumOfNumerals1(){
        int exp = 1;
        int act = Cycles.sumOfNumerals(1);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void sumOfNumerals15(){
        int exp = 6;
        int act = Cycles.sumOfNumerals(15);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void sumOfNumeralsNeg(){
        int exp = 2;
        int act = Cycles.sumOfNumerals(-11);
        Assert.assertEquals(exp, act);
    }


    //==================================================================
    //=========================   mirrorNumber tests ===================
    //==================================================================

    @Test
    public void mirrorNumber1(){
        int exp = 1;
        int act = Cycles.mirrorNumber(1);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void mirrorNumber10(){
        int exp = 1;
        int act = Cycles.mirrorNumber(10);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void mirrorNumber35(){
        int exp = 53;
        int act = Cycles.mirrorNumber(35);
        Assert.assertEquals(exp, act);
    }

    @Test(expected = IllegalArgumentException.class)
    public void mirrorNumberNeg(){
        int exp = 54;
        int act = Cycles.mirrorNumber(-45);
        Assert.assertEquals(exp, act);
    }

}
