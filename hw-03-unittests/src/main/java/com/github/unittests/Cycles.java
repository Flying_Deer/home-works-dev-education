package com.github.unittests;

public class Cycles {

    public static int[] sumAndQuantityOfEvens(int min, int max){
        int[] result = {0, 0};

        for(int i = min; i <= max; i++) {
            if (i % 2 == 0) {
                result[0] += i;
                result[1]++;
            }
        }
        return result;
    }

    public static boolean isPrime(int number){
        if(number <= 1)
            return false;
        if(number == 2)
            return true;
        if (number%2 == 0){
            return false;
        }
        for(int i = 3; i * i <= number; i += 2){
            if (number%i == 0){
                return false;
            }
        }
        return true;
    }

    public static int rootConsistent(int number){
        if(number < 0) {
            throw new IllegalArgumentException();
        }
        int root;
        for(root = 0; root * root < number; root++);
        if(root * root != number) {
            root--;
        }
        return root;
    }

    public static int rootBinary(int number){
        if (number < 0){
            throw new IllegalArgumentException();
        }
        if(number == 0)
            return 0;
        if(number == 1)
            return 1;
        int root = number/2;
        int first = 0;
        int last = number;
        while (true){
            if(root * root > number){
                last = root;
            }
            else if(root * root < number){
                first = root;
            }
            else
                break;
            root = (last + first) / 2;
            if(root == first || root == last)
                break;
        }
        return root;
    }

    public static int factorial(int number) {
        if (number < 0){
            throw new IllegalArgumentException();
        }
        int fact = 1;
        for (int i = 1; i <= number; i++) {
            fact *= i;
        }
        return fact;
    }

    public static int sumOfNumerals(int number) {
        int sum = 0;
        if (number < 0)
            number = -number;
        while (number / 10 != 0 || number % 10 != 0) {
            sum += number % 10;
            number = number / 10;
        }
        return sum;
    }

    public static int mirrorNumber(int number){
        if (number < 0){
            throw new IllegalArgumentException();
        }
        String Buffer = "";
        while(number/10 != 0 || number%10 != 0){
            Buffer = Buffer.concat(Integer.toString(number%10));
            number = number/10;
        }
        return Integer.parseInt(Buffer);

    }

}
