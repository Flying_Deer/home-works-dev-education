package com.github.unittests;

import javax.print.attribute.standard.Sides;

public class Arrays {

    public static int minimalElement(int[] array){
        if(array == null){
            throw new NullPointerException();
        }
        if(array.length == 0){
            throw new IllegalArgumentException();
        }
        int min = array[0];
        for (int j : array) {
            if (j < min) {
                min = j;
            }
        }
        return min;
    }

    public static int maximalElement(int[] array){
        if(array == null){
            throw new NullPointerException();
        }
        if(array.length == 0){
            throw new IllegalArgumentException();
        }
        int max = array[0];
        for (int j : array) {
            if (j > max) {
                max = j;
            }
        }
        return max;
    }

    public static int minimalElementIndex(int[] array){
        if(array == null){
            throw new NullPointerException();
        }
        if(array.length == 0){
            throw new IllegalArgumentException();
        }
        int minIndex = 0;
        int size = array.length;
        for (int i = 0; i < size; i++) {
            if (array[i] < array[minIndex]) {
                minIndex = i;
            }
        }
        return minIndex;
    }

    public static int maximalElementIndex(int[] array){
        if(array == null){
            throw new NullPointerException();
        }
        if(array.length == 0){
            throw new IllegalArgumentException();
        }
        int maxIndex = 0;
        int size = array.length;
        for (int i = 0; i < size; i++) {
            if (array[i] > array[maxIndex]) {
                maxIndex = i;
            }
        }
        return maxIndex;
    }

    public static int sumOfOddElements(int[] array){
        if(array == null){
            throw new NullPointerException();
        }
        if(array.length == 0){
            throw new IllegalArgumentException();
        }
        int sum = 0;
        int size = array.length;
        for(int i = 1; i < size; i+=2){
            sum += array[i];
        }
        return sum;
    }

    public static int[] reversedArray(int[] array){
        if(array == null){
            throw new NullPointerException();
        }
        int size = array.length;
        int[] newArray = new int[size];

        for(int i = 0; i < size; i++)
            newArray[i] = array[size - 1- i];

        return newArray;
    }

    public static int numberOfOddElements(int[] array){
        if(array == null){
            throw new NullPointerException();
        }
        int size = array.length;
        int counter = 0;
        for (int j : array) {
            if (j % 2 != 0) {
                counter++;
            }
        }
        return counter;
    }

    public static int[] swapHalves(int[] array){
        if(array == null){
            throw new NullPointerException();
        }
        int size = array.length;
        int[] newArray = new int[size];
        for(int i = 0; i < size/2; i++) {
            newArray[i + size / 2 + size % 2] = array[i];
        }

        if (size - size / 2 >= 0) System.arraycopy(array, size / 2, newArray, 0, size - size / 2);
        return newArray;
    }

    public static int[] bubbleSort(int[] array){
        if(array == null){
            throw new NullPointerException();
        }
        int size = array.length;
        int tmp;
        boolean sortComplete = false;
        for(int i = 0; !sortComplete; i++){
            sortComplete = true;
            for(int j = 0; j < size - i - 1; j++){
                if(array[j] > array[j + 1]){
                    tmp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tmp;
                    sortComplete = false;
                }
            }
        }
        return array;
    }

    public static int[] selectSort(int[] array){
        if(array == null){
            throw new NullPointerException();
        }
        int size = array.length;
        int tmp;
        for (int i = 0; i < size - 1; i++) {
            int Least = i;
            for (int j = i + 1; j < size; j++) {
                if (array[j] < array[Least]) {
                    Least = j;
                }
            }
            tmp = array[i];
            array[i] = array[Least];
            array[Least] = tmp;
        }
        return array;
    }

    public static int[] insertSort(int[] array){
        if(array == null){
            throw new NullPointerException();
        }
        int size = array.length;
        int j;
        int tmp;
        for(int i = 1; i < size; i++){
            tmp = array[i];
            j =  i - 1;
            while(j >= 0 && array[j] > tmp){
                array[j + 1] = array[j];
                j--;
            }
            array[j + 1] = tmp;
        }
        return array;
    }

    public static void quickSort(int[] array, int start, int end) {
        if(array == null){
            throw new NullPointerException();
        }

        if (array.length == 0)
            return;

        if (start >= end)
            return;

        int middle = start + (end - start) / 2;
        int pivot = array[middle];

        int i = start, j = end;
        while (i <= j) {
            while (array[i] < pivot) {
                i++;
            }

            while (array[j] > pivot) {
                j--;
            }

            if (i <= j) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            }
        }

        if (start < j)
            quickSort(array, start, j);

        if (end > i)
            quickSort(array, i, end);
    }

    public static void merge(int[] array, int[] left, int [] right){
        int sizeLeft = left.length;
        int sizeRight = right.length;
        int size = array.length;

        int i = 0, j = 0, k = 0;
        while(i < sizeLeft && j < sizeRight){
            if(left[i] < right[j]){
                array[k] = left[i];
                k++;
                i++;
            }
            else{
                array[k] = right[j];
                j++;
                k++;
            }
        }
        while(i < sizeLeft){
            array[k] = left[i];
            i++;
            k++;
        }
        while(j < sizeRight){
            array[k] = right[j];
            j++;
            k++;
        }
    }

    public static void mergeSort(int[] array){

        if(array == null){
            throw new NullPointerException();
        }

        int size = array.length;
        if(size < 2)
            return;

        int middle = size/2;
        int[] left = new int[middle];
        int[] right = new int[size - middle];

        System.arraycopy(array, 0, left, 0, middle);
        if (size - middle >= 0) System.arraycopy(array, middle, right, 0, size - middle);

        mergeSort(left);
        mergeSort(right);

        merge(array, left, right);
    }

    public static void shellSort(int[] array){
        if(array == null){
            throw new NullPointerException();
        }

        int size = array.length;
        for(int step = size/2; step > 0; step /= 2){
            for(int i = step; i < size; i++){
                for(int j = i - step; j >= 0 && array[j] > array[j + step]; j -= step){
                    int temp = array[j];
                    array[j] = array[j + step];
                    array[j + step] = temp;
                }
            }
        }
    }

    public static void heapSort(int[] array){
        if(array == null){
            throw new NullPointerException();
        }

        int size = array.length;

        for(int i = size/2 - 1; i >= 0; i--)
            heapify(array, size, i);

        for(int i = size - 1; i > 0; i--){
            int temp = array[0];
            array[0] = array[i];
            array[i] = temp;

            heapify(array, i, 0);
        }
    }

    public static void heapify(int[] array, int size, int i){
        int largest = i;
        int left = 2 * i + 1;
        int right = 2 * i + 2;

        if(left < size && array[left] > array[largest])
            largest = left;

        if(right < size && array[right] > array[largest])
            largest = right;

        if(largest != i){
            int temp = array[i];
            array[i] = array[largest];
            array[largest] = temp;

            heapify(array, size, largest);
        }
    }

}
