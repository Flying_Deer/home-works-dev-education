package com.github.unittests;

import org.junit.Assert;
import org.junit.Test;

public class ArraysSortTest {


    //==================================================================
    //=================   bubbleSort tests =============================
    //==================================================================

    @Test
    public void bubbleSort6(){
        int[] someArray = {5, 4, 3, 2, 1, 0};
        int[] exp = {0, 1, 2, 3, 4, 5};
        int[] act = Arrays.bubbleSort(someArray);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void bubbleSort5(){
        int[] someArray = {4, 6, 4, 2, 1};
        int[] exp = {1, 2, 4, 4, 6};
        int[] act = Arrays.bubbleSort(someArray);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void bubbleSort1(){
        int[] someArray = {-1};
        int[] exp = {-1};
        int[] act = Arrays.bubbleSort(someArray);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void bubbleSort0(){
        int[] someArray = {};
        int[] exp = {};
        int[] act = Arrays.bubbleSort(someArray);
        Assert.assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void bubbleSortNull(){
        int[] exp = null;
        int[] act = Arrays.bubbleSort(null);
        Assert.assertArrayEquals(exp, act);
    }

    //==================================================================
    //=================   selectSort tests =============================
    //==================================================================

    @Test
    public void selectSort6(){
        int[] someArray = {5, 4, 3, 2, 1, 0};
        int[] exp = {0, 1, 2, 3, 4, 5};
        int[] act = Arrays.selectSort(someArray);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void selectSort5(){
        int[] someArray = {4, 6, 4, 2, 1};
        int[] exp = {1, 2, 4, 4, 6};
        int[] act = Arrays.selectSort(someArray);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void selectSort1(){
        int[] someArray = {-1};
        int[] exp = {-1};
        int[] act = Arrays.selectSort(someArray);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void selectSort0(){
        int[] someArray = {};
        int[] exp = {};
        int[] act = Arrays.selectSort(someArray);
        Assert.assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void selectSortNull(){
        int[] exp = null;
        int[] act = Arrays.selectSort(null);
        Assert.assertArrayEquals(exp, act);
    }

    //==================================================================
    //=================   insertSort tests =============================
    //==================================================================

    @Test
    public void insertSort6(){
        int[] someArray = {5, 4, 3, 2, 1, 0};
        int[] exp = {0, 1, 2, 3, 4, 5};
        int[] act = Arrays.insertSort(someArray);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void insertSort5(){
        int[] someArray = {4, 6, 4, 2, 1};
        int[] exp = {1, 2, 4, 4, 6};
        int[] act = Arrays.insertSort(someArray);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void insertSort1(){
        int[] someArray = {-1};
        int[] exp = {-1};
        int[] act = Arrays.insertSort(someArray);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void insertSort0(){
        int[] someArray = {};
        int[] exp = {};
        int[] act = Arrays.insertSort(someArray);
        Assert.assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void insertSortNull(){
        int[] exp = null;
        int[] act = Arrays.insertSort(null);
        Assert.assertArrayEquals(exp, act);
    }

    //==================================================================
    //=================   quickSort tests ==============================
    //==================================================================

    @Test
    public void quickSort6(){
        int[] act = {5, 4, 3, 2, 1, 0};
        int[] exp = {0, 1, 2, 3, 4, 5};
        Arrays.quickSort(act, 0, act.length - 1);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void quickSort5(){
        int[] act = {4, 6, 4, 2, 1};
        int[] exp = {1, 2, 4, 4, 6};
        Arrays.quickSort(act, 0, act.length - 1);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void quickSort1(){
        int[] act = {-1};
        int[] exp = {-1};
        Arrays.quickSort(act, 0, 0);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void quickSort0(){
        int[] act = {};
        int[] exp = {};
        Arrays.quickSort(act, 0, act.length - 1);
        Assert.assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void quickSortNull(){
        int[] act = null;
        int[] exp = null;
        Arrays.quickSort(null, 0, 0);
        Assert.assertArrayEquals(exp, act);
    }

    //==================================================================
    //=================   mergeSort tests ==============================
    //==================================================================

    @Test
    public void mergeSort6(){
        int[] act = {5, 4, 3, 2, 1, 0};
        int[] exp = {0, 1, 2, 3, 4, 5};
        Arrays.mergeSort(act);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void mergeSort5(){
        int[] act = {4, 6, 4, 2, 1};
        int[] exp = {1, 2, 4, 4, 6};
        Arrays.mergeSort(act);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void mergeSort1(){
        int[] act = {-1};
        int[] exp = {-1};
        Arrays.mergeSort(act);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void mergeSort0(){
        int[] act = {};
        int[] exp = {};
        Arrays.mergeSort(act);
        Assert.assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void mergeSortNull(){
        int[] act = null;
        int[] exp = null;
        Arrays.mergeSort(null);
        Assert.assertArrayEquals(exp, act);
    }

    //==================================================================
    //=================   shellSort tests ==============================
    //==================================================================

    @Test
    public void shellSort6(){
        int[] act = {5, 4, 3, 2, 1, 0};
        int[] exp = {0, 1, 2, 3, 4, 5};
        Arrays.shellSort(act);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void shellSort5(){
        int[] act = {4, 6, 4, 2, 1};
        int[] exp = {1, 2, 4, 4, 6};
        Arrays.shellSort(act);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void shellSort1(){
        int[] act = {-1};
        int[] exp = {-1};
        Arrays.shellSort(act);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void shellSort0(){
        int[] act = {};
        int[] exp = {};
        Arrays.shellSort(act);
        Assert.assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void shellSortNull(){
        int[] act = null;
        int[] exp = null;
        Arrays.shellSort(null);
        Assert.assertArrayEquals(exp, act);
    }

    //==================================================================
    //=================   heapSort tests ==============================
    //==================================================================

    @Test
    public void heapSort6(){
        int[] act = {5, 4, 3, 2, 1, 0};
        int[] exp = {0, 1, 2, 3, 4, 5};
        Arrays.heapSort(act);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void heapSort5(){
        int[] act = {4, 6, 4, 2, 1};
        int[] exp = {1, 2, 4, 4, 6};
        Arrays.heapSort(act);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void heapSort1(){
        int[] act = {-1};
        int[] exp = {-1};
        Arrays.heapSort(act);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void heapSort0(){
        int[] act = {};
        int[] exp = {};
        Arrays.heapSort(act);
        Assert.assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void heapSortNull(){
        int[] act = null;
        int[] exp = null;
        Arrays.heapSort(null);
        Assert.assertArrayEquals(exp, act);
    }

}
