package com.github.unittests;
import java.util.Arrays;
import java.util.List;

public class Functions {

    public static String dayOfWeek(int number){
        switch(number){
            case 1:
                return "1st day of week is Sunday.";
            case 2:
                return "2nd day of week is Monday.";
            case 3:
                return "3rd day of week is Tuesday.";
            case 4:
                return "4th day of week is Wednesday.";
            case 5:
                return "5th day of week is Thursday.";
            case 6:
                return "6th day of week is Friday.";
            case 7:
                return "7th day of week is Saturday.";
            default:
                throw new IllegalArgumentException();
        }
    }

    private static String onesToWord(int number){
        String Str = "";
        switch(number){
            case 1: return Str.concat("one");
            case 2: return Str.concat("two");
            case 3: return Str.concat("three");
            case 4: return Str.concat("four");
            case 5: return Str.concat("five");
            case 6: return Str.concat("six");
            case 7: return Str.concat("seven");
            case 8: return Str.concat("eight");
            case 9: return Str.concat("nine");
            default: break;
        }
        return Str;
    }

    private static String firstDecToWords(int number){
        String Str = "";
        switch(number){
            case 0: return Str.concat("ten");
            case 1: return Str.concat("eleven");
            case 2: return Str.concat("twelve");
            case 3: return Str.concat("thirteen");
            case 4: return Str.concat("fourteen");
            case 5: return Str.concat("fifteen");
            case 6: return Str.concat("sixteen");
            case 7: return Str.concat("seventeen");
            case 8: return Str.concat("eighteen");
            case 9: return Str.concat("nineteen");
            default: break;
        }
        return Str;
    }

    private static String decToWord(int number){
        String Str = "";
        switch(number){
            case 2: return Str.concat("twenty");
            case 3: return Str.concat("thirty");
            case 4: return Str.concat("forty");
            case 5: return Str.concat("fifty");
            case 6: return Str.concat("sixty");
            case 7: return Str.concat("seventy");
            case 8: return Str.concat("eighty");
            case 9: return Str.concat("ninety");
            default: break;
        }
        return Str;
    }

    public static String numberToWords(int number){
        if(number < 0){
            throw new IllegalArgumentException();
        }
        int ones, dec, hun;
        ones = number % 10;
        dec = (number % 100)/10;
        hun = (number)/100;
        if(hun > 9){
            throw new IllegalArgumentException();
        }

        String Words = "";

        if(hun!= 0){
            Words = Words.concat(onesToWord(hun));
            Words = Words.concat(" hundred");
        }
        if(dec > 1){
            Words = Words.concat(" ");
            if(hun != 0 )
                Words = Words.concat("and ");
            Words = Words.concat(decToWord(dec));
        }else if(dec == 1){
            Words = Words.concat(" ");
            if(hun != 0 )
                Words = Words.concat("and ");
            Words = Words.concat(firstDecToWords(ones));
            return Words;
        }
        if(dec != 0 && ones != 0)
            Words = Words.concat("-");
        Words = Words.concat(onesToWord(ones));
        if(Words.equals(""))
            Words = "zero";
        return Words;
    }

    public static int wordsToInt(String input){
        if(input == null){
            throw new NullPointerException();
        }

        int Result = 0;

        List<String> AllowedStrings = Arrays.asList
                (
                        "zero","one","two","three","four","five","six","seven",
                        "eight","nine","ten","eleven","twelve","thirteen","fourteen",
                        "fifteen","sixteen","seventeen","eighteen","nineteen","twenty",
                        "thirty","forty","fifty","sixty","seventy","eighty","ninety",
                        "hundred"
                );

        if(input.length()> 0)
        {
            //Змена всех разделительных знаков на пробелы
            input = input.replaceAll("-", " ");
            input = input.toLowerCase().replaceAll(" and", " ");
            String[] SplittedParts = input.trim().split("\\s+");

            //Проверка того, что все слова в считываемой строке являются частью числа
            for(String str : SplittedParts)
            {
                if(!AllowedStrings.contains(str))
                {
                    throw new IllegalArgumentException();
                }
            }
            //Для всех слов:
            //1-90 - прибавляем к промежуточному результату
            //100 - умножаем промежуточный результат на 100
            for(String str : SplittedParts)
            {
                if(str.equalsIgnoreCase("zero")) {
                    Result += 0;
                }
                else if(str.equalsIgnoreCase("one")) {
                    Result += 1;
                }
                else if(str.equalsIgnoreCase("two")) {
                    Result += 2;
                }
                else if(str.equalsIgnoreCase("three")) {
                    Result += 3;
                }
                else if(str.equalsIgnoreCase("four")) {
                    Result += 4;
                }
                else if(str.equalsIgnoreCase("five")) {
                    Result += 5;
                }
                else if(str.equalsIgnoreCase("six")) {
                    Result += 6;
                }
                else if(str.equalsIgnoreCase("seven")) {
                    Result += 7;
                }
                else if(str.equalsIgnoreCase("eight")) {
                    Result += 8;
                }
                else if(str.equalsIgnoreCase("nine")) {
                    Result += 9;
                }
                else if(str.equalsIgnoreCase("ten")) {
                    Result += 10;
                }
                else if(str.equalsIgnoreCase("eleven")) {
                    Result += 11;
                }
                else if(str.equalsIgnoreCase("twelve")) {
                    Result += 12;
                }
                else if(str.equalsIgnoreCase("thirteen")) {
                    Result += 13;
                }
                else if(str.equalsIgnoreCase("fourteen")) {
                    Result += 14;
                }
                else if(str.equalsIgnoreCase("fifteen")) {
                    Result += 15;
                }
                else if(str.equalsIgnoreCase("sixteen")) {
                    Result += 16;
                }
                else if(str.equalsIgnoreCase("seventeen")) {
                    Result += 17;
                }
                else if(str.equalsIgnoreCase("eighteen")) {
                    Result += 18;
                }
                else if(str.equalsIgnoreCase("nineteen")) {
                    Result += 19;
                }
                else if(str.equalsIgnoreCase("twenty")) {
                    Result += 20;
                }
                else if(str.equalsIgnoreCase("thirty")) {
                    Result += 30;
                }
                else if(str.equalsIgnoreCase("forty")) {
                    Result += 40;
                }
                else if(str.equalsIgnoreCase("fifty")) {
                    Result += 50;
                }
                else if(str.equalsIgnoreCase("sixty")) {
                    Result += 60;
                }
                else if(str.equalsIgnoreCase("seventy")) {
                    Result += 70;
                }
                else if(str.equalsIgnoreCase("eighty")) {
                    Result += 80;
                }
                else if(str.equalsIgnoreCase("ninety")) {
                    Result += 90;
                    System.out.println(Result);
                }
                else if(str.equalsIgnoreCase("hundred")) {
                    Result *= 100;
                }
            }
        }
        return Result;
    }

    public static String thousandToWords(int Num){

        int ones, dec, hun;

        ones = Num % 10;
        dec = (Num % 100)/10;
        hun = (Num)/100;

        String Words = "";

        if(hun!= 0){
            Words = Words.concat(onesToWord(hun));
            Words = Words.concat(" hundred ");
        }
        if(dec > 1){
            Words = Words.concat(" ");
            if(hun != 0 )
                Words = Words.concat("and ");
            Words = Words.concat(decToWord(dec));
        }else if(dec == 1){
            Words = Words.concat(" ");
            if(hun != 0 )
                Words = Words.concat("and ");
            Words = Words.concat(firstDecToWords(ones));
            return Words;
        }
        if(dec != 0 && ones != 0)
            Words = Words.concat("-");
        Words = Words.concat(onesToWord(ones));

        return Words;
    }

    public static String longToWords(long Num){
        if(Num < 0){
            throw new IllegalArgumentException();
        }
        final int Thous = 1000;
        final int Mill = Thous * Thous;
        final int Bill = Mill * Thous;
        int ones =(int)(Num % Thous);
        int Thousands = (int)((Num % Mill) / Thous);
        int Millions = (int)((Num % Bill) / Mill);
        int Billions = (int)(Num / Bill);
        String Words = "";

        if(Billions != 0){
            Words = Words.concat(thousandToWords(Billions));
            Words = Words.concat(" billion ");
        }
        if(Millions != 0){
            Words = Words.concat(thousandToWords(Millions));
            Words = Words.concat(" million ");
        }
        if(Thousands != 0){
            Words = Words.concat(thousandToWords(Thousands));
            Words = Words.concat(" thousand ");
        }
        if(ones != 0){
            Words = Words.concat(thousandToWords(ones));
        }
        if(Words.equals(""))
            Words = "zero";

        Words = Words.replaceAll("  ", " ");
        return Words;
    }

    public static long wordsToLong(String input){
        if(input == null){
            throw new NullPointerException();
        }

        long Result = 0;
        long FinalResult = 0;

        List<String> AllowedStrings = Arrays.asList
                (
                        "zero","one","two","three","four","five","six","seven",
                        "eight","nine","ten","eleven","twelve","thirteen","fourteen",
                        "fifteen","sixteen","seventeen","eighteen","nineteen","twenty",
                        "thirty","forty","fifty","sixty","seventy","eighty","ninety",
                        "hundred", "thousand", "million", "billion"
                );

        if(input.length()> 0)
        {
            //Змена всех разделительных знаков на пробелы
            input = input.replaceAll("-", " ");
            input = input.toLowerCase().replaceAll(" and", " ");
            String[] SplittedParts = input.trim().split("\\s+");

            //Проверка того, что все слова в считываемой строке являются частью числа
            for(String str : SplittedParts)
            {
                if(!AllowedStrings.contains(str))
                {
                    throw new IllegalArgumentException();
                }
            }
            //Для всех слов:
            //1-90 - прибавляем к промежуточному результату
            //100 - умножаем промежуточный результат на 100
            for(String str : SplittedParts)
            {
                if(str.equalsIgnoreCase("zero")) {
                    Result += 0;
                }
                else if(str.equalsIgnoreCase("one")) {
                    Result += 1;
                }
                else if(str.equalsIgnoreCase("two")) {
                    Result += 2;
                }
                else if(str.equalsIgnoreCase("three")) {
                    Result += 3;
                }
                else if(str.equalsIgnoreCase("four")) {
                    Result += 4;
                }
                else if(str.equalsIgnoreCase("five")) {
                    Result += 5;
                }
                else if(str.equalsIgnoreCase("six")) {
                    Result += 6;
                }
                else if(str.equalsIgnoreCase("seven")) {
                    Result += 7;
                }
                else if(str.equalsIgnoreCase("eight")) {
                    Result += 8;
                }
                else if(str.equalsIgnoreCase("nine")) {
                    Result += 9;
                }
                else if(str.equalsIgnoreCase("ten")) {
                    Result += 10;
                }
                else if(str.equalsIgnoreCase("eleven")) {
                    Result += 11;
                }
                else if(str.equalsIgnoreCase("twelve")) {
                    Result += 12;
                }
                else if(str.equalsIgnoreCase("thirteen")) {
                    Result += 13;
                }
                else if(str.equalsIgnoreCase("fourteen")) {
                    Result += 14;
                }
                else if(str.equalsIgnoreCase("fifteen")) {
                    Result += 15;
                }
                else if(str.equalsIgnoreCase("sixteen")) {
                    Result += 16;
                }
                else if(str.equalsIgnoreCase("seventeen")) {
                    Result += 17;
                }
                else if(str.equalsIgnoreCase("eighteen")) {
                    Result += 18;
                }
                else if(str.equalsIgnoreCase("nineteen")) {
                    Result += 19;
                }
                else if(str.equalsIgnoreCase("twenty")) {
                    Result += 20;
                }
                else if(str.equalsIgnoreCase("thirty")) {
                    Result += 30;
                }
                else if(str.equalsIgnoreCase("forty")) {
                    Result += 40;
                }
                else if(str.equalsIgnoreCase("fifty")) {
                    Result += 50;
                }
                else if(str.equalsIgnoreCase("sixty")) {
                    Result += 60;
                }
                else if(str.equalsIgnoreCase("seventy")) {
                    Result += 70;
                }
                else if(str.equalsIgnoreCase("eighty")) {
                    Result += 80;
                }
                else if(str.equalsIgnoreCase("ninety")) {
                    Result += 90;
                }
                else if(str.equalsIgnoreCase("hundred")) {
                    Result *= 100;
                }
                else if(str.equalsIgnoreCase("thousand")) {
                    Result *= 1000;
                    FinalResult += Result;
                    Result=0;
                }
                else if(str.equalsIgnoreCase("million")) {
                    Result *= 1000000;
                    FinalResult += Result;
                    Result=0;
                }
                else if(str.equalsIgnoreCase("billion")) {
                    Result *= 1000000000;
                    FinalResult += Result;
                    Result=0;
                }
            }
        }
        FinalResult += Result;
        return FinalResult;
    }
}
