package com.github.unittests;

import org.junit.Assert;
import org.junit.Test;

public class ArraysTest {


    //==================================================================
    //=================   minimalElement tests =========================
    //==================================================================

    @Test
    public void minimalElement6(){
        int[] someArray = {0, 1, 2, 3, 5, 0};
        int exp = 0;
        int act = Arrays.minimalElement(someArray);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void minimalElement2(){
        int[] someArray = {1, 2};
        int exp = 1;
        int act = Arrays.minimalElement(someArray);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void minimalElement1(){
        int[] someArray = {-1};
        int exp = -1;
        int act = Arrays.minimalElement(someArray);
        Assert.assertEquals(exp, act);
    }

    @Test(expected = IllegalArgumentException.class)
    public void minimalElement0(){
        int[] someArray = {};
        int exp = 1;
        int act = Arrays.minimalElement(someArray);
        Assert.assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void minimalElementNull(){
        int exp = 1;
        int act = Arrays.minimalElement(null);
        Assert.assertEquals(exp, act);
    }

    //==================================================================
    //=================   maximalElement tests =========================
    //==================================================================

    @Test
    public void maximalElement6(){
        int[] someArray = {0, 1, 2, 3, 5, 0};
        int exp = 5;
        int act = Arrays.maximalElement(someArray);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void maximalElement2(){
        int[] someArray = {1, 2};
        int exp = 2;
        int act = Arrays.maximalElement(someArray);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void maximalElement1(){
        int[] someArray = {-1};
        int exp = -1;
        int act = Arrays.maximalElement(someArray);
        Assert.assertEquals(exp, act);
    }

    @Test(expected = IllegalArgumentException.class)
    public void maximalElement0(){
        int[] someArray = {};
        int exp = 1;
        int act = Arrays.maximalElement(someArray);
        Assert.assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void maximalElementNull(){
        int exp = 1;
        int act = Arrays.maximalElement(null);
        Assert.assertEquals(exp, act);
    }

    //==================================================================
    //=================   minimalElementIndex tests ====================
    //==================================================================

    @Test
    public void minimalElementIndex6(){
        int[] someArray = {0, 1, 2, 3, 5, 0};
        int exp = 0;
        int act = Arrays.minimalElementIndex(someArray);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void minimalElementIndex2(){
        int[] someArray = {1, 2};
        int exp = 0;
        int act = Arrays.minimalElementIndex(someArray);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void minimalElementIndex1(){
        int[] someArray = {-1};
        int exp = 0;
        int act = Arrays.minimalElementIndex(someArray);
        Assert.assertEquals(exp, act);
    }

    @Test(expected = IllegalArgumentException.class)
    public void minimalElementIndex0(){
        int[] someArray = {};
        int exp = 1;
        int act = Arrays.minimalElementIndex(someArray);
        Assert.assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void minimalElementIndexNull(){
        int exp = 1;
        int act = Arrays.minimalElementIndex(null);
        Assert.assertEquals(exp, act);
    }

    //==================================================================
    //=================   maximalElementIndex tests ====================
    //==================================================================

    @Test
    public void maximalElementIndex6(){
        int[] someArray = {0, 1, 2, 3, 5, 0};
        int exp = 4;
        int act = Arrays.maximalElementIndex(someArray);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void maximalElementIndex2(){
        int[] someArray = {1, 2};
        int exp = 1;
        int act = Arrays.maximalElementIndex(someArray);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void maximalElementIndex1(){
        int[] someArray = {-1};
        int exp = 0;
        int act = Arrays.maximalElementIndex(someArray);
        Assert.assertEquals(exp, act);
    }

    @Test(expected = IllegalArgumentException.class)
    public void maximalElementIndex0(){
        int[] someArray = {};
        int exp = 1;
        int act = Arrays.maximalElementIndex(someArray);
        Assert.assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void maximalElementIndexNull(){
        int exp = 1;
        int act = Arrays.maximalElementIndex(null);
        Assert.assertEquals(exp, act);
    }

    //==================================================================
    //=================   sumOfOddElements tests =======================
    //==================================================================

    @Test
    public void sumOfOddElements6(){
        int[] someArray = {0, 1, 2, 3, 5, 0};
        int exp = 4;
        int act = Arrays.sumOfOddElements(someArray);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void sumOfOddElements2(){
        int[] someArray = {1, 2};
        int exp = 2;
        int act = Arrays.sumOfOddElements(someArray);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void sumOfOddElements1(){
        int[] someArray = {-1};
        int exp = 0;
        int act = Arrays.sumOfOddElements(someArray);
        Assert.assertEquals(exp, act);
    }

    @Test(expected = IllegalArgumentException.class)
    public void sumOfOddElements0(){
        int[] someArray = {};
        int exp = 1;
        int act = Arrays.sumOfOddElements(someArray);
        Assert.assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void sumOfOddElementsNull(){
        int exp = 1;
        int act = Arrays.sumOfOddElements(null);
        Assert.assertEquals(exp, act);
    }

    //==================================================================
    //=================   reversedArray tests ==========================
    //==================================================================

    @Test
    public void reversedArray6(){
        int[] someArray = {0, 1, 2, 3, 5, 0};
        int[] exp = {0, 5, 3, 2, 1, 0};
        int[] act = Arrays.reversedArray(someArray);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void reversedArray2(){
        int[] someArray = {1, 2};
        int[] exp = {2, 1};
        int[] act = Arrays.reversedArray(someArray);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void reversedArray1(){
        int[] someArray = {-1};
        int[] exp = {-1};
        int[] act = Arrays.reversedArray(someArray);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void reversedArray0(){
        int[] someArray = {};
        int[] exp = {};
        int[] act = Arrays.reversedArray(someArray);
        Assert.assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void reversedArrayNull(){
        int[] exp = null;
        int[] act = Arrays.reversedArray(null);
        Assert.assertArrayEquals(exp, act);
    }

    //==================================================================
    //=================   numberOfOddElements tests ====================
    //==================================================================

    @Test
    public void numberOfOddElements6(){
        int[] someArray = {0, 1, 2, 3, 5, 0};
        int exp = 3;
        int act = Arrays.numberOfOddElements(someArray);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void numberOfOddElements2(){
        int[] someArray = {1, 2};
        int exp = 1;
        int act = Arrays.numberOfOddElements(someArray);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void numberOfOddElements1(){
        int[] someArray = {-1};
        int exp = 1;
        int act = Arrays.numberOfOddElements(someArray);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void numberOfOddElements0(){
        int[] someArray = {};
        int exp = 0;
        int act = Arrays.numberOfOddElements(someArray);
        Assert.assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void numberOfOddElementsNull(){
        int exp = 1;
        int act = Arrays.numberOfOddElements(null);
        Assert.assertEquals(exp, act);
    }

    //==================================================================
    //=================   swapHalves tests =============================
    //==================================================================

    @Test
    public void swapHalves6(){
        int[] someArray = {0, 1, 2, 3, 5, 0};
        int[] exp = {3, 5, 0, 0, 1, 2};
        int[] act = Arrays.swapHalves(someArray);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void swapHalves5(){
        int[] someArray = {1, 2, 3, 4, 5};
        int[] exp = {3, 4, 5, 1, 2};
        int[] act = Arrays.swapHalves(someArray);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void swapHalves1(){
        int[] someArray = {-1};
        int[] exp = {-1};
        int[] act = Arrays.swapHalves(someArray);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void swapHalves0(){
        int[] someArray = {};
        int[] exp = {};
        int[] act = Arrays.swapHalves(someArray);
        Assert.assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void swapHalvesNull(){
        int[] exp = null;
        int[] act = Arrays.swapHalves(null);
        Assert.assertArrayEquals(exp, act);
    }

}

