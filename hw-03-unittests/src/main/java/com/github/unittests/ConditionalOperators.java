package com.github.unittests;

public class ConditionalOperators {

    public static int evenAddOddMultiply(int a, int b) {
        if(a % 2 == 0)
            return a * b;
        else
             return a + b;
    }

    public static int quarter(int x, int y) {
        if (y > 0){
            if (x > 0)
                return 1;
            else
                return 2;
        }
        else{
            if (x < 0)
                return 3;
            else
                return 4;
        }
    }

    public static int addPositive(int a, int b, int c){
        int sum = 0;
        if(a > 0)
            sum += a;
        if(b > 0)
            sum += b;
        if(c > 0)
            sum += c;
        return sum;
    }

    public static int calculateExpression(int a, int b, int c){
        int product = a * b * c;
        int sum = a + b + c;

        if(product > sum)
            return product + 3;
        else
            return sum + 3;
    }

    public static char score(int Rating){
        char Score = 'S';
        if(Rating >= 0 && Rating < 20)
            Score = 'F';
        else if(Rating >= 20 && Rating < 40)
            Score = 'E';
        else if(Rating >= 40 && Rating < 60)
            Score = 'D';
        else if(Rating >= 60 && Rating < 75)
            Score = 'C';
        else if(Rating >= 75 && Rating < 90)
            Score = 'B';
        else if(Rating >=90 && Rating <= 100)
            Score = 'A';

        if(Score != 'S')
            return Score;
        else
            throw new IllegalArgumentException();
    }



}
