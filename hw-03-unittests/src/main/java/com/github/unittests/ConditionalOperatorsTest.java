package com.github.unittests;

import org.junit.Assert;
import org.junit.Test;

public class ConditionalOperatorsTest {

    //==================================================================
    //=================   evenAddOddMultiply tests =====================
    //==================================================================

    @Test
    public void evenAddOddMultiplyEven(){
        int exp = 12;
        int act = ConditionalOperators.evenAddOddMultiply(4,3);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void evenAddOddMultiplyOdd(){
        int exp = 7;
        int act = ConditionalOperators.evenAddOddMultiply(3,4);
        Assert.assertEquals(exp, act);
    }

    //==================================================================
    //======================   quarter tests ===========================
    //==================================================================

    @Test
    public void quarterFirst(){
        int exp = 1;
        int act = ConditionalOperators.quarter(1,1);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void quarterSecond(){
        int exp = 2;
        int act = ConditionalOperators.quarter(-1,1);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void quarterThird(){
        int exp = 3;
        int act = ConditionalOperators.quarter(-1,-1);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void quarterFourth(){
        int exp = 4;
        int act = ConditionalOperators.quarter(1,-1);
        Assert.assertEquals(exp, act);
    }


    //==================================================================
    //===================   addPositive tests ==========================
    //==================================================================

    @Test
    public void addPositiveAPos(){
        int exp = 1;
        int act = ConditionalOperators.addPositive(1,-1, -1);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void addPositiveBPos(){
        int exp = 2;
        int act = ConditionalOperators.addPositive(-5,2, -1);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void addPositiveCPos(){
        int exp = 5;
        int act = ConditionalOperators.addPositive(-5,-1, 5);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void addPositiveABPos(){
        int exp = 11;
        int act = ConditionalOperators.addPositive(4,7, -11);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void addPositiveACPos(){
        int exp = 9;
        int act = ConditionalOperators.addPositive(4,-1, 5);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void addPositiveBCPos(){
        int exp = 3;
        int act = ConditionalOperators.addPositive(-4,2, 1);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void addPositiveABCPos(){
        int exp = 7;
        int act = ConditionalOperators.addPositive(4,2, 1);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void addPositiveAllNeg(){
        int exp = 0;
        int act = ConditionalOperators.addPositive(-1,-2, -7);
        Assert.assertEquals(exp, act);
    }

    //==================================================================
    //=================   calculateExpression tests =====================
    //==================================================================

    @Test
    public void calculateExpressionSum(){
        int exp = 9;
        int act = ConditionalOperators.calculateExpression(4,1, 1);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void calculateExpressionProd(){
        int exp = 11;
        int act = ConditionalOperators.calculateExpression(4,2, 1);
        Assert.assertEquals(exp, act);
    }


    //==================================================================
    //=========================   score tests ==========================
    //==================================================================
    @Test
    public void scoreF(){
        char exp = 'F';
        int act = ConditionalOperators.score(15);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void scoreE(){
        char exp = 'E';
        int act = ConditionalOperators.score(25);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void scoreD(){
        char exp = 'D';
        int act = ConditionalOperators.score(54);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void scoreC(){
        char exp = 'C';
        int act = ConditionalOperators.score(67);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void scoreB(){
        char exp = 'B';
        int act = ConditionalOperators.score(83);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void scoreA(){
        char exp = 'A';
        int act = ConditionalOperators.score(100);
        Assert.assertEquals(exp, act);
    }
}
