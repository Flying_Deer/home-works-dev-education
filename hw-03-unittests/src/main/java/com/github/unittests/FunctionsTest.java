package com.github.unittests;

import org.junit.Assert;
import org.junit.Test;

public class FunctionsTest {

    //==================================================================
    //=================   dayOfWeek tests ==============================
    //==================================================================

    @Test
    public void dayOfWeek1(){
        int Day = 1;
        String exp = "1st day of week is Sunday.";
        String act = Functions.dayOfWeek(Day);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void dayOfWeek2(){
        int Day = 2;
        String exp = "2nd day of week is Monday.";
        String act = Functions.dayOfWeek(Day);
        Assert.assertEquals(exp, act);
    }

    @Test(expected = IllegalArgumentException.class)
    public void dayOfWeek10(){
        int Day = 10;
        String exp = "Wrong number";
        String act = Functions.dayOfWeek(Day);
        Assert.assertEquals(exp, act);
    }

    @Test(expected = IllegalArgumentException.class)
    public void dayOfWeek0(){
        int Day = 0;
        String exp = "Wrong number";
        String act = Functions.dayOfWeek(Day);
        Assert.assertEquals(exp, act);
    }

    //==================================================================
    //=================   NumberToWords tests ==========================
    //==================================================================
    @Test
    public void NumberToWords1(){
        int Number = 1;
        String exp = "one";
        String act = Functions.numberToWords(Number);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void NumberToWords0(){
        int Number = 0;
        String exp = "zero";
        String act = Functions.numberToWords(Number);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void NumberToWordsMany(){
        int Number = 832;
        String exp = "eight hundred and thirty-two";
        String act = Functions.numberToWords(Number);
        Assert.assertEquals(exp, act);
    }

    @Test(expected = IllegalArgumentException.class)
    public void NumberToWordsNegative(){
        int Number = -1;
        String exp = "Wrong Number";
        String act = Functions.numberToWords(Number);
        Assert.assertEquals(exp, act);
    }

    //==================================================================
    //=================   WordsToInt tests =============================
    //==================================================================
    @Test
    public void NumberToWordsOne(){
        String Words = "one";
        int exp = 1;
        int act = Functions.wordsToInt(Words);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void NumberToWordsDec(){
        String Words = "twenty-two";
        int exp = 22;
        int act = Functions.wordsToInt(Words);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void NumberToWordsHun(){
        String Words = "five hundred and sixty-seven";
        int exp = 567;
        int act = Functions.wordsToInt(Words);
        Assert.assertEquals(exp, act);
    }

    @Test(expected = IllegalArgumentException.class)
    public void NumberToWordsWrong(){
        String Words = "some number";
        int exp = 1;
        int act = Functions.wordsToInt(Words);
        Assert.assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void NumberToWordsNull(){
        String Words = null;
        int exp = 1;
        int act = Functions.wordsToInt(Words);
        Assert.assertEquals(exp, act);
    }

    //==================================================================
    //=================   LongToWords tests ============================
    //==================================================================
    @Test
    public void LongToWords1(){
        int Number = 1;
        String exp = "one";
        String act = Functions.longToWords(Number);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void LongToWords0(){
        int Number = 0;
        String exp = "zero";
        String act = Functions.longToWords(Number);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void LongToWordsMany(){
        long Number = 832586489783L;
        String exp = "eight hundred and thirty-two billion " +
                "five hundred and eighty-six million " +
                "four hundred and eighty-nine thousand " +
                "seven hundred and eighty-three";
        String act = Functions.longToWords(Number);
        Assert.assertEquals(exp, act);
    }

    @Test(expected = IllegalArgumentException.class)
    public void LongToWordsNegative(){
        int Number = -1;
        String exp = "Wrong Number";
        String act = Functions.longToWords(Number);
        Assert.assertEquals(exp, act);
    }

    //==================================================================
    //=================   LongToWords tests ============================
    //==================================================================
    @Test
    public void WordsToLongOne(){
        String Words = "one";
        long exp = 1L;
        long act = Functions.wordsToLong(Words);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void WordsToLongDec(){
        String Words = "twenty-two";
        long exp = 22L;
        long act = Functions.wordsToLong(Words);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void WordsToLongHun(){
        String Words = "five hundred and sixty-seven";
        long exp = 567L;
        long act = Functions.wordsToLong(Words);
        Assert.assertEquals(exp, act);
    }
    @Test
    public void WordsToLongMany(){
        String Words = "five hundred and sixty-seven billion " +
                "eight hundred and forty-eight million " +
                "one";
        long exp = 567848000001L;
        long act = Functions.wordsToLong(Words);
        Assert.assertEquals(exp, act);
    }

    @Test(expected = IllegalArgumentException.class)
    public void WordsToLongWrong(){
        String Words = "some number";
        long exp = 1L;
        long act = Functions.wordsToLong(Words);
        Assert.assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void WordsToLongNull(){
        String Words = null;
        long exp = 1;
        long act = Functions.wordsToLong(Words);
        Assert.assertEquals(exp, act);
    }

}
