package org.bitbucket.balls;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class BallsPanel extends JPanel {

    AtomicInteger counter = new AtomicInteger(0);

    List<Ball> balls = new ArrayList<>();

    List<Thread> ballThreads = new ArrayList<>();

    public void addBall(Ball ball){
        Thread ballThread = new Thread(ball, "Ball-" + counter.incrementAndGet());
        this.balls.add(ball);
        this.ballThreads.add(ballThread);
        this.add(ball);
        ballThread.start();
    }

    public BallsPanel(JFrame parent){

        this.setLayout(null);
        this.setVisible(true);
        this.setLocation(5, 5);
        this.setSize(parent.getWidth() - 23, parent.getHeight() - 45);
        this.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        this.addMouseListener(new BallFactory(this));

    }


}
