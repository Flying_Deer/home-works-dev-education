package org.bitbucket.balls;

import javax.swing.*;

public class BallsFrame extends JFrame {

    public BallsFrame(){

        this.setLayout(null);
        this.setVisible(true);
        this.setBounds(5, 5, 1280, 800);

        this.add(new BallsPanel(this));

        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

}
