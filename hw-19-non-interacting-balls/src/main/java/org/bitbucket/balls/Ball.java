package org.bitbucket.balls;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class Ball extends JPanel implements Runnable{

    Random random = new Random();

    private int centerX;

    private int centerY;

    private int radius;

    private Color color;

    private int vx;

    private int vy;

    private JComponent parent;

    public Ball(Point point, JComponent parent) {
        this.parent = parent;
        this.centerX = point.x;
        this.centerY = point.y;
        this.radius = random.nextInt(Math.min(this.parent.getWidth(), this.parent.getHeight()) / 8);
        this.setLocation(this.centerX - this.radius, this.centerY - this.radius);
        this.setSize(this.radius << 1, this.radius << 1);
        this.color = new Color(random.nextInt(255), random.nextInt(255), random.nextInt(255));
        this.vx = random.nextInt(this.parent.getWidth() / 40) - this.parent.getWidth() / 80;
        this.vy = random.nextInt(this.parent.getHeight() / 40) - this.parent.getHeight() / 80;

        this.setVisible(true);
        this.setLayout(null);
        this.setOpaque(false);

    }

    public int getCenterX() {
        return centerX;
    }

    public void setCenterX(int centerX) {
        this.centerX = centerX;
    }

    public int getCenterY() {
        return centerY;
    }

    public void setCenterY(int centerY) {
        this.centerY = centerY;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getVx() {
        return vx;
    }

    public void setVx(int vx) {
        this.vx = vx;
    }

    public int getVy() {
        return vy;
    }

    public void setVy(int vy) {
        this.vy = vy;
    }

    @Override
    public void paint(Graphics g){
        super.paint(g);
        Graphics2D graphics2D = (Graphics2D) g;
        graphics2D.setColor(this.color);
        graphics2D.fillOval(0, 0, radius << 1, radius << 1);
    }

    private void move(){
        this.setLocation(this.getX() + vx, this.getY() + vy);
        this.centerX += vx;
        this.centerY += vy;

        if(this.centerX + this.radius > this.parent.getWidth() || this.centerX - this.radius < 0){
            this.vx = -this.vx;
        }

        if(this.centerY + this.radius > this.parent.getHeight() || this.centerY - this.radius < 0){
            this.vy = - this.vy;
        }

    }


    @Override
    public void run() {
        try {
            while (true) {
                this.move();
                Thread.sleep(25);
            }
        } catch (InterruptedException e){
            Thread.currentThread().interrupt();
        }

    }
}
