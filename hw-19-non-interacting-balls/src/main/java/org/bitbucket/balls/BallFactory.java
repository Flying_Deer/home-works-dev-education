package org.bitbucket.balls;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class BallFactory implements MouseListener {

    private final BallsPanel parent;

    public BallFactory(BallsPanel parent) {
        this.parent = parent;
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {
        Ball ball = new Ball(e.getPoint(), this.parent);
        this.parent.addBall(ball);
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

}
