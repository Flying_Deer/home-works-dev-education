package org.bitbucket.log4j;

import org.apache.log4j.Logger;

public class Application {

    public static Logger logger = Logger.getLogger(Application.class);

    public static void main(String[] args) {

        for(int i = 0; i < 100; i++){
            try{
                if(RandomNumberGenerator.generate() <= 5){
                    throw new CustomException("Generated number is lesser or equals then 5.");
                }
            } catch (Exception e){
                logger.warn(e.getMessage(), e);
            }
        }

    }

}
