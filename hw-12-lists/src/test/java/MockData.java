public class MockData {

    static int pushableElement = 0;

    static int[] arrayMany = { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };

    static int[] arrayManyPushedForward = { pushableElement, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };

    static int[] arrayManyPushedBack = { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 , pushableElement };

    static int[] arrayManyRemovedForward = { 9, 8, 7, 6, 5, 4, 3, 2, 1 };

    static int[] arrayManyRemovedBack = { 10, 9, 8, 7, 6, 5, 4, 3, 2 };

    static int[] arrayTwo = { 1, 10 };

    static int[] arrayTwoPushedForward = { pushableElement, 1, 10 };

    static int[] arrayTwoPushedBack = { 1, 10 , pushableElement };

    static int[] arrayTwoRemovedForward = { 10 };

    static int[] arrayTwoRemovedBack = { 1 };

    static int[] arrayOne = { 100 };

    static int[] arrayOnePushedForward = { pushableElement, 100 };

    static int[] arrayOnePushedBack = { 100 , pushableElement };

    static int[] arrayOneRemoved = {};

    static int[] arrayZero = {};

    static int[] arrayZeroPushed = { pushableElement };

    static int[] arrayNull = null;

}
