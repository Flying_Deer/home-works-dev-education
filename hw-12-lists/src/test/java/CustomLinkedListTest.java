import com.github.collections.list.CustomLinkedList;
import com.github.collections.list.IGenericList;
import org.junit.Assert;
import org.junit.Test;

import java.util.NoSuchElementException;

public class CustomLinkedListTest {

    private IGenericList<Integer> list = new CustomLinkedList<>();

    private final Integer[] manyNumbers = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    private final Integer[] twoNumbers = {0, 1};
    private final Integer[] oneNumber = {0};
    private final Integer[] emptyArray = {};

    //=================================================
    //=================== Null ========================
    //=================================================

    @Test(expected = NullPointerException.class)
    public void nullArray() {
        list = new CustomLinkedList<>(null);
    }

    //=================================================
    //=================== Clean =======================
    //=================================================
    @Test
    public void cleanMany() {
        list = new CustomLinkedList<>(manyNumbers);
        list.clear();
        Object[] exp = {};
        Object[] act = this.list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void cleanTwo() {
        list = new CustomLinkedList<>(twoNumbers);
        list.clear();
        Object[] exp = {};
        Object[] act = this.list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void cleanOne() {
        list = new CustomLinkedList<>(oneNumber);
        list.clear();
        Object[] exp = {};
        Object[] act = this.list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void cleanZero() {
        list = new CustomLinkedList<>(emptyArray);
        list.clear();
        Object[] exp = {};
        Object[] act = this.list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    //=================================================
    //=================== Size ========================
    //=================================================

    @Test
    public void sizeMany() {
        list = new CustomLinkedList<>(manyNumbers);
        int exp = manyNumbers.length;
        int act = list.size();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void sizeTwo() {
        list = new CustomLinkedList<>(twoNumbers);
        int exp = twoNumbers.length;
        int act = list.size();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void sizeOne() {
        list = new CustomLinkedList<>(oneNumber);
        int exp = oneNumber.length;
        int act = list.size();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void sizeZero() {
        list = new CustomLinkedList<>(emptyArray);
        int exp = emptyArray.length;
        int act = list.size();
        Assert.assertEquals(exp, act);
    }


    //=================================================
    //=============== Push forward ====================
    //=================================================

    @Test
    public void pushForwardMany() {
        list = new CustomLinkedList<>(manyNumbers);
        Object[] exp = {-1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        list.pushForward(-1);
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void pushForwardTwo() {
        list = new CustomLinkedList<>(twoNumbers);
        Object[] exp = {-1, 0, 1};
        list.pushForward(-1);
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void pushForwardOne() {
        list = new CustomLinkedList<>(oneNumber);
        Object[] exp = {-1, 0};
        list.pushForward(-1);
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void pushForwardZero() {
        list = new CustomLinkedList<>(emptyArray);
        Object[] exp = {-1};
        list.pushForward(-1);
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void pushNullForward() {
        list = new CustomLinkedList<>(oneNumber);
        Object[] exp = {null, 0};
        list.pushForward(null);
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    //=================================================
    //================== Push back ====================
    //=================================================

    @Test
    public void pushBackMany() {
        list = new CustomLinkedList<>(manyNumbers);
        Object[] exp = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        list.pushBack(10);
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void pushBackTwo() {
        list = new CustomLinkedList<>(twoNumbers);
        Object[] exp = {0, 1, 2};
        list.pushBack(2);
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void pushBackOne() {
        list = new CustomLinkedList<>(oneNumber);
        Object[] exp = {0, 1};
        list.pushBack(1);
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void pushBackZero() {
        list = new CustomLinkedList<>(emptyArray);
        Object[] exp = {0};
        list.pushBack(0);
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void pushNullBack() {
        list = new CustomLinkedList<>(oneNumber);
        Object[] exp = {0, null};
        list.pushBack(null);
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    //=================================================
    //=================== Push ========================
    //=================================================

    @Test
    public void pushMany() {
        list = new CustomLinkedList<>(manyNumbers);
        list.push(0, 5);
        Object[] exp = {5, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void pushTwo() {
        list = new CustomLinkedList<>(twoNumbers);
        list.push(2, 7);
        Object[] exp = {0, 1, 7};
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void pushOne() {
        list = new CustomLinkedList<>(oneNumber);
        list.push(0, 4);
        Object[] exp = {4, 0};
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void pushZero() {
        list = new CustomLinkedList<>(emptyArray);
        list.push(0, 2);
        Object[] exp = {2};
        Object[] act = list.toArray();
        Assert.assertEquals(exp.length, act.length);
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void pushNull() {
        list = new CustomLinkedList<>(oneNumber);
        Object[] exp = {0, null};
        list.push(1, null);
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void pushOutOfBounds() {
        list = new CustomLinkedList<>(oneNumber);
        list.push(100, 1);
    }

    //=================================================
    //================ Remove forward =================
    //=================================================

    @Test
    public void removeForwardMany() {
        list = new CustomLinkedList<>(manyNumbers);
        Object[] exp = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        list.removeForward();
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void removeForwardTwo() {
        list = new CustomLinkedList<>(twoNumbers);
        Object[] exp = {1};
        list.removeForward();
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void removeForwardOne() {
        list = new CustomLinkedList<>(oneNumber);
        Object[] exp = {};
        list.removeForward();
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeForwardZero() {
        list = new CustomLinkedList<>(emptyArray);
        list.removeForward();
    }

    //=================================================
    //================ Remove back ====================
    //=================================================

    @Test
    public void removeBackMany() {
        list = new CustomLinkedList<>(manyNumbers);
        Object[] exp = {0, 1, 2, 3, 4, 5, 6, 7, 8};
        list.removeBack();
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void removeBackTwo() {
        list = new CustomLinkedList<>(twoNumbers);
        Object[] exp = {0};
        list.removeBack();
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void removeBackOne() {
        list = new CustomLinkedList<>(oneNumber);
        Object[] exp = {};
        list.removeBack();
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeBackZero() {
        list = new CustomLinkedList<>(emptyArray);
        list.removeBack();
    }

    //=================================================
    //==================== Remove =====================
    //=================================================

    @Test
    public void removeMany() {
        list = new CustomLinkedList<>(manyNumbers);
        list.remove(5);
        Object[] exp = {0, 1, 2, 3, 4, 6, 7, 8, 9};
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void removeTwo() {
        list = new CustomLinkedList<>(twoNumbers);
        list.remove(1);
        Object[] exp = {0};
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void removeOne() {
        list = new CustomLinkedList<>(oneNumber);
        list.remove(0);
        Object[] exp = {};
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test(expected = NoSuchElementException.class)
    public void removeZero() {
        list = new CustomLinkedList<>(emptyArray);
        list.remove(1);
    }

    @Test(expected = NoSuchElementException.class)
    public void removeNoSuchElement() {
        list = new CustomLinkedList<>(manyNumbers);
        list.remove(100);
    }

    //=================================================
    //=============== Remove by index =================
    //=================================================

    @Test
    public void removeByIndexMany() {
        list = new CustomLinkedList<>(manyNumbers);
        list.removeByIndex(5);
        Object[] exp = {0, 1, 2, 3, 4, 6, 7, 8, 9};
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void removeByIndexTwo() {
        list = new CustomLinkedList<>(twoNumbers);
        list.removeByIndex(1);
        Object[] exp = {0};
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void removeByIndexOne() {
        list = new CustomLinkedList<>(oneNumber);
        list.removeByIndex(0);
        Object[] exp = {};
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeByIndexZero() {
        list = new CustomLinkedList<>(emptyArray);
        list.removeByIndex(1);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeByIndexOutOfBounds() {
        list = new CustomLinkedList<>(manyNumbers);
        list.removeByIndex(100);
    }


    //=================================================
    //===================== Get =======================
    //=================================================

    @Test
    public void getMany() {
        list = new CustomLinkedList<>(manyNumbers);
        Object exp = 5;
        Object act = list.get(5);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void getTwo() {
        list = new CustomLinkedList<>(twoNumbers);
        Object exp = 0;
        Object act = list.get(0);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void getOne() {
        list = new CustomLinkedList<>(oneNumber);
        Object exp = 0;
        Object act = list.get(0);
        Assert.assertEquals(exp, act);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void getOutOfBounds() {
        list = new CustomLinkedList<>(manyNumbers);
        Object act = list.get(700);
    }

    //=================================================
    //===================== Set =======================
    //=================================================

    @Test
    public void setMany() {
        list = new CustomLinkedList<>(manyNumbers);
        list.set(3, 19);
        Object[] exp = {0, 1, 2, 19, 4, 5, 6, 7, 8, 9};
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void setTwo() {
        list = new CustomLinkedList<>(twoNumbers);
        list.set(1, 12);
        Object[] exp = {0, 12};
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void setOne() {
        list = new CustomLinkedList<>(oneNumber);
        list.set(0, 5);
        Object[] exp = {5};
        Object[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void setOutOfBounds() {
        list = new CustomLinkedList<>(emptyArray);
        list.set(5, 6);
    }

    //=================================================
    //================== Contains =====================
    //=================================================

    @Test
    public void containsMany() {
        list = new CustomLinkedList<>(manyNumbers);
        Assert.assertTrue(list.contains(6));
    }

    @Test
    public void containsTwo() {
        list = new CustomLinkedList<>(twoNumbers);
        Assert.assertTrue(list.contains(1));
    }

    @Test
    public void containsOne() {
        list = new CustomLinkedList<>(oneNumber);
        Assert.assertTrue(list.contains(0));
    }

    @Test
    public void notContains() {
        list = new CustomLinkedList<>(oneNumber);
        Assert.assertFalse(list.contains(6));
    }

}
