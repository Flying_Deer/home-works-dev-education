import com.github.collections.list.IntArrayList;
import com.github.collections.list.IList;
import org.junit.Assert;
import org.junit.Test;

import java.util.NoSuchElementException;

public class ArrayListTest {

    private IList list = new IntArrayList();

    //=================================================
    //=================== Clean =======================
    //=================================================

    @Test
    public void cleanMany() {
        list = new IntArrayList(MockData.arrayMany);
        list.clear();
        int[] exp = {};
        int[] act = this.list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void cleanTwo() {
        list = new IntArrayList(MockData.arrayTwo);
        list.clear();
        int[] exp = {};
        int[] act = this.list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void cleanOne() {
        list = new IntArrayList(MockData.arrayOne);
        list.clear();
        int[] exp = {};
        int[] act = this.list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void cleanZero() {
        list = new IntArrayList(MockData.arrayZero);
        list.clear();
        int[] exp = {};
        int[] act = this.list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void cleanNull() {
        list = new IntArrayList(MockData.arrayNull);
    }

    //=================================================
    //=================== Size ========================
    //=================================================

    @Test
    public void sizeMany() {
        list = new IntArrayList(MockData.arrayMany);
        int exp = MockData.arrayMany.length;
        int act = list.size();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void sizeTwo() {
        list = new IntArrayList(MockData.arrayTwo);
        int exp = MockData.arrayTwo.length;
        int act = list.size();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void sizeOne() {
        list = new IntArrayList(MockData.arrayOne);
        int exp = MockData.arrayOne.length;
        int act = list.size();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void sizeZero() {
        list = new IntArrayList(MockData.arrayZero);
        int exp = MockData.arrayZero.length;
        int act = list.size();
        Assert.assertEquals(exp, act);
    }

    //=================================================
    //=============== Push forward ====================
    //=================================================

    @Test
    public void pushForwardMany() {
        list = new IntArrayList(MockData.arrayMany);
        list.pushForward(MockData.pushableElement);
        int[] exp = MockData.arrayManyPushedForward;
        int[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void pushForwardTwo() {
        list = new IntArrayList(MockData.arrayTwo);
        list.pushForward(MockData.pushableElement);
        int[] exp = MockData.arrayTwoPushedForward;
        int[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void pushForwardOne() {
        list = new IntArrayList(MockData.arrayOne);
        list.pushForward(MockData.pushableElement);
        int[] exp = MockData.arrayOnePushedForward;
        int[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void pushForwardZero() {
        list = new IntArrayList(MockData.arrayZero);
        list.pushForward(MockData.pushableElement);
        int[] exp = MockData.arrayZeroPushed;
        int[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    //=================================================
    //================== Push back ====================
    //=================================================

    @Test
    public void pushBackMany() {
        list = new IntArrayList(MockData.arrayMany);
        list.pushBack(MockData.pushableElement);
        int[] exp = MockData.arrayManyPushedBack;
        int[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void pushBackTwo() {
        list = new IntArrayList(MockData.arrayTwo);
        list.pushBack(MockData.pushableElement);
        int[] exp = MockData.arrayTwoPushedBack;
        int[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void pushBackOne() {
        list = new IntArrayList(MockData.arrayOne);
        list.pushBack(MockData.pushableElement);
        int[] exp = MockData.arrayOnePushedBack;
        int[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void pushBackZero() {
        list = new IntArrayList(MockData.arrayZero);
        list.pushBack(MockData.pushableElement);
        int[] exp = MockData.arrayZeroPushed;
        int[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    //=================================================
    //=================== Push ========================
    //=================================================

    @Test
    public void pushMany() {
        list = new IntArrayList(MockData.arrayMany);
        list.push(5, MockData.pushableElement);
        int[] exp = {10, 9, 8, 7, 6, 0, 5, 4, 3, 2, 1};
        int[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void pushTwo() {
        list = new IntArrayList(MockData.arrayTwo);
        list.push(1, MockData.pushableElement);
        int[] exp = {1, 0, 10};
        int[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void pushOne() {
        list = new IntArrayList(MockData.arrayOne);
        list.push(0, MockData.pushableElement);
        int[] exp = {0, 100};
        int[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void pushZero() {
        list = new IntArrayList(MockData.arrayZero);
        list.push(0, MockData.pushableElement);
        int[] exp = {0};
        int[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void pushOutOfBounds() {
        list = new IntArrayList(MockData.arrayOne);
        list.push(100, MockData.pushableElement);
    }

    //=================================================
    //================ Remove forward =================
    //=================================================

    @Test
    public void removeForwardMany() {
        list = new IntArrayList(MockData.arrayMany);
        list.removeForward();
        int[] exp = MockData.arrayManyRemovedForward;
        int[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void removeForwardTwo() {
        list = new IntArrayList(MockData.arrayTwo);
        list.removeForward();
        int[] exp = MockData.arrayTwoRemovedForward;
        int[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void removeForwardOne() {
        list = new IntArrayList(MockData.arrayOne);
        list.removeForward();
        int[] exp = MockData.arrayOneRemoved;
        int[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeForwardZero() {
        list = new IntArrayList(MockData.arrayZero);
        list.removeForward();
    }

    //=================================================
    //================ Remove back ====================
    //=================================================

    @Test
    public void removeBackMany() {
        list = new IntArrayList(MockData.arrayMany);
        list.removeBack();
        int[] exp = MockData.arrayManyRemovedBack;
        int[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void removeBackTwo() {
        list = new IntArrayList(MockData.arrayTwo);
        list.removeBack();
        int[] exp = MockData.arrayTwoRemovedBack;
        int[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void removeBackOne() {
        list = new IntArrayList(MockData.arrayOne);
        list.removeBack();
        int[] exp = MockData.arrayOneRemoved;
        int[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeBackZero() {
        list = new IntArrayList(MockData.arrayZero);
        list.removeBack();
    }

    //=================================================
    //==================== Remove =====================
    //=================================================

    @Test
    public void removeMany() {
        list = new IntArrayList(MockData.arrayMany);
        list.remove(1);
        int[] exp = {10, 8, 7, 6, 5, 4, 3, 2, 1};
        int[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void removeTwo() {
        list = new IntArrayList(MockData.arrayTwo);
        list.remove(0);
        int[] exp = {10};
        int[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void removeOne() {
        list = new IntArrayList(MockData.arrayOne);
        list.remove(0);
        int[] exp = {};
        int[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeZero() {
        list = new IntArrayList(MockData.arrayZero);
        list.remove(1);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeOutOfBounds() {
        list = new IntArrayList(MockData.arrayMany);
        list.remove(100);
    }

    //=================================================
    //===================== Max =======================
    //=================================================

    @Test
    public void maxMany() {
        list = new IntArrayList(MockData.arrayMany);
        int exp = 10;
        int act = list.max();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void maxTwo() {
        list = new IntArrayList(MockData.arrayTwo);
        int exp = 10;
        int act = list.max();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void maxOne() {
        list = new IntArrayList(MockData.arrayOne);
        int exp = 100;
        int act = list.max();
        Assert.assertEquals(exp, act);
    }

    @Test(expected = NoSuchElementException.class)
    public void maxZero() {
        list = new IntArrayList(MockData.arrayZero);
        list.max();
    }

    //=================================================
    //===================== Min =======================
    //=================================================

    @Test
    public void minMany() {
        list = new IntArrayList(MockData.arrayMany);
        int exp = 1;
        int act = list.min();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void minTwo() {
        list = new IntArrayList(MockData.arrayTwo);
        int exp = 1;
        int act = list.min();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void minOne() {
        list = new IntArrayList(MockData.arrayOne);
        int exp = 100;
        int act = list.min();
        Assert.assertEquals(exp, act);
    }

    @Test(expected = NoSuchElementException.class)
    public void minZero() {
        list = new IntArrayList(MockData.arrayZero);
        list.min();
    }

    //=================================================
    //================= MaxIndex ======================
    //=================================================

    @Test
    public void maxIndexMany() {
        list = new IntArrayList(MockData.arrayMany);
        int exp = 0;
        int act = list.maxIndex();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void maxIndexTwo() {
        list = new IntArrayList(MockData.arrayTwo);
        int exp = 1;
        int act = list.maxIndex();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void maxIndexOne() {
        list = new IntArrayList(MockData.arrayOne);
        int exp = 0;
        int act = list.maxIndex();
        Assert.assertEquals(exp, act);
    }

    @Test(expected = NoSuchElementException.class)
    public void maxIndexZero() {
        list = new IntArrayList(MockData.arrayZero);
        list.maxIndex();
    }

    //=================================================
    //================== MinIndex =====================
    //=================================================

    @Test
    public void minIndexMany() {
        list = new IntArrayList(MockData.arrayMany);
        int exp = 9;
        int act = list.minIndex();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void minIndexTwo() {
        list = new IntArrayList(MockData.arrayTwo);
        int exp = 0;
        int act = list.minIndex();
        Assert.assertEquals(exp, act);
    }

    @Test
    public void minIndexOne() {
        list = new IntArrayList(MockData.arrayOne);
        int exp = 0;
        int act = list.minIndex();
        Assert.assertEquals(exp, act);
    }

    @Test(expected = NoSuchElementException.class)
    public void minIndexZero() {
        list = new IntArrayList(MockData.arrayZero);
        list.minIndex();
    }

    //=================================================
    //===================== Sort ======================
    //=================================================

    @Test
    public void sortMany() {
        int[] array = {0, 1, 9, 2, 8, 3, 7, 4, 6, 5};
        list = new IntArrayList(array);
        int[] exp = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int[] act = list.sort();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void sortTwo() {
        int[] array = {54, 12};
        list = new IntArrayList(array);
        int[] exp = {12, 54};
        int[] act = list.sort();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void sortOne() {
        int[] array = {1};
        list = new IntArrayList(array);
        int[] exp = {1};
        int[] act = list.sort();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void sortZero() {
        int[] array = {};
        list = new IntArrayList(array);
        int[] exp = {};
        int[] act = list.sort();
        Assert.assertArrayEquals(exp, act);
    }

    //=================================================
    //===================== Get =======================
    //=================================================

    @Test
    public void getMany() {
        int[] array = {0, 1, 9, 2, 8, 3, 7, 4, 6, 5};
        list = new IntArrayList(array);
        int exp = 5;
        int act = list.get(9);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void getTwo() {
        int[] array = {54, 12};
        list = new IntArrayList(array);
        int act = list.get(0);
        int exp = 54;
        Assert.assertEquals(exp, act);
    }

    @Test
    public void getOne() {
        int[] array = {1};
        list = new IntArrayList(array);
        int act = list.get(0);
        int exp = 1;
        Assert.assertEquals(exp, act);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void getOutOfBounds() {
        int[] array = {};
        list = new IntArrayList(array);
        list.get(100);
    }

    //=================================================
    //===================== Set =======================
    //=================================================

    @Test
    public void setMany() {
        int[] array = {0, 1, 9, 2, 8, 3, 7, 4, 6, 5};
        list = new IntArrayList(array);
        list.set(3, 9);
        int[] exp = {0, 1, 9, 9, 8, 3, 7, 4, 6, 5};
        int[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void setTwo() {
        int[] array = {54, 12};
        list = new IntArrayList(array);
        list.set(0, 12);
        int[] exp = {12, 12};
        int[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void setOne() {
        int[] array = {1};
        list = new IntArrayList(array);
        list.set(0, 100);
        int[] exp = {100};
        int[] act = list.toArray();
        Assert.assertArrayEquals(exp, act);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void setOutOfBounds() {
        int[] array = {};
        list = new IntArrayList(array);
        list.set(100, 200);
    }

    //=================================================
    //==================== Revers =====================
    //=================================================

    @Test
    public void reversMany() {
        int[] array = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        list = new IntArrayList(array);
        int[] exp = {9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
        int[] act = list.revers();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void reversTwo() {
        int[] array = {54, 12};
        list = new IntArrayList(array);
        int[] exp = {12, 54};
        int[] act = list.revers();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void reversOne() {
        int[] array = {1};
        list = new IntArrayList(array);
        int[] exp = {1};
        int[] act = list.revers();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void reversZero() {
        int[] array = {};
        list = new IntArrayList(array);
        int[] exp = {};
        int[] act = list.revers();
        Assert.assertArrayEquals(exp, act);
    }

    //=================================================
    //================== Half revers ==================
    //=================================================

    @Test
    public void halfReversMany() {
        int[] array = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        list = new IntArrayList(array);
        int[] exp = {5, 6, 7, 8, 9, 0, 1, 2, 3, 4};
        int[] act = list.halfRevers();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void halfReversTwo() {
        int[] array = {54, 12};
        list = new IntArrayList(array);
        int[] exp = {12, 54};
        int[] act = list.halfRevers();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void halfReversOne() {
        int[] array = {1};
        list = new IntArrayList(array);
        int[] exp = {1};
        int[] act = list.halfRevers();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void halfReversZero() {
        int[] array = {};
        list = new IntArrayList(array);
        int[] exp = {};
        int[] act = list.halfRevers();
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void halfReversOdd(){
        int[] array = {54, 12, 15};
        list = new IntArrayList(array);
        int[] exp = {12, 15, 54};
        int[] act = list.halfRevers();
        Assert.assertArrayEquals(exp, act);
    }

}
