package com.github.collections.list;

public interface IList {

    void clear();

    int size();

    int[] toArray();

    void pushForward(int value);

    void pushBack(int value);

    void push(int index, int value);

    int removeForward();

    int removeBack();

    int remove(int index);

    int max();

    int min();

    int maxIndex();

    int minIndex();

    int[] sort();

    int get(int index);

    void set(int index, int value);

    int[] revers();

    int[] halfRevers();

}
