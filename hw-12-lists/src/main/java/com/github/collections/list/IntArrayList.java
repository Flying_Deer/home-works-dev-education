package com.github.collections.list;

import java.util.NoSuchElementException;

public class IntArrayList implements IList {

    private int capacity;

    private int size;

    private int[] array;

    public IntArrayList(int[] array) {
        if(array == null){
            throw new NullPointerException();
        }
        this.size = array.length;
        this.capacity = array.length * 3 / 2;
        this.array = new int[this.capacity];
        for (int i = 0; i < array.length; i++) {
            this.array[i] = array[i];
        }
    }

    public IntArrayList(int capacity) {
        if(capacity < 0){
            throw new IllegalArgumentException();
        }
        this.capacity = capacity;
        this.array = new int[this.capacity];
        this.size = 0;
    }

    public IntArrayList() {
        this.capacity = 10;
        this.size = 0;
        this.array = new int[this.capacity];
    }

    @Override
    public void clear() {
        this.capacity = 10;
        this.size = 0;
        this.array = new int[this.capacity];
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public int[] toArray() {
        int[] result = new int[this.size];
        for (int i = 0; i < this.size; i++) {
            result[i] = this.array[i];
        }
        return result;
    }

    @Override
    public void pushForward(int value) {
        while (this.size + 1 > this.capacity * 7 / 10) {
            resize();
        }
        this.size++;
        for(int i = size - 1; i > 0; i--){
            this.array[i] = this.array[i - 1];
        }
        this.array[0] = value;
    }

    @Override
    public void pushBack(int value) {
        while (this.size + 1 > this.capacity * 7 / 10) {
            resize();
        }
        this.size++;
        this.array[size] = value;
    }

    @Override
    public void push(int index, int value) {
        if(index > size || index < 0){
            throw new IndexOutOfBoundsException();
        }
        while (this.size + 1 > this.capacity * 7 / 10) {
            resize();
        }
        this.size++;
        int[] tmp = new int[this.capacity];
        for (int i = 0; i < index; i++) {
            tmp[i] = this.array[i];
        }
        tmp[index] = value;
        for (int i = index + 1; i < this.size; i++) {
            tmp[i] = this.array[i - 1];
        }
        this.array = tmp;
    }

    @Override
    public int removeForward() {
        int tmp = this.array[0];
        this.size--;
        for (int i = 0; i < this.size; i++) {
            this.array[i] = this.array[i + 1];
        }
        return tmp;
    }

    @Override
    public int removeBack() {
        int tmp = this.array[this.size - 1];
        this.array[size - 1] = 0;
        this.size--;
        return tmp;
    }

    @Override
    public int remove(int index) {
        if(index > size || index < 0){
            throw new IndexOutOfBoundsException();
        }
        int tmp = this.array[index];
        size--;
        for (int i = index; i < this.size; i++) {
            this.array[i] = this.array[i + 1];
        }
        return tmp;
    }

    @Override
    public int max() {
        if(this.size == 0){
            throw new NoSuchElementException();
        }
        int tmp = this.array[0];
        for(int i = 1; i < this.size; i++){
            if(this.array[i] > tmp){
                tmp = this.array[i];
            }
        }
        return tmp;
    }

    @Override
    public int min() {
        if(this.size == 0){
            throw new NoSuchElementException();
        }
        int tmp = this.array[0];
        for(int i = 1; i < this.size; i++){
            if(this.array[i] < tmp){
                tmp = this.array[i];
            }
        }
        return tmp;
    }

    @Override
    public int maxIndex() {
        if(this.size == 0){
            throw new NoSuchElementException();
        }
        int tmpIndex = 0;
        for(int i = 1; i < this.size; i++){
            if(this.array[i] > this.array[tmpIndex]){
                tmpIndex = i;
            }
        }
        return tmpIndex;
    }

    @Override
    public int minIndex() {
        if(this.size == 0){
            throw new NoSuchElementException();
        }
        int tmpIndex = 0;
        for(int i = 1; i < this.size; i++){
            if(this.array[i] < this.array[tmpIndex]){
                tmpIndex = i;
            }
        }
        return tmpIndex;
    }

    @Override
    public int[] sort() {
        quickSort(this.array, 0, size - 1);
        return this.toArray();
    }

    @Override
    public int get(int index) {
        return this.array[index];
    }

    @Override
    public int[] halfRevers() {
        int[] result = new int[this.size];
        for(int i = 0; i < this.size/2; i++) {
            result[i + this.size / 2 + this.size % 2] = this.array[i];
        }
        if (this.size - this.size / 2 >= 0) System.arraycopy(array, size / 2, result, 0, size - size / 2);
        return result;
    }

    @Override
    public int[] revers() {
        int[] result = new int[this.size];
        for(int i = 0; i < size; i++)
            result[i] = this.array[size - 1- i];
        return result;
    }

    @Override
    public void set(int index, int value) {
        if(index > size || index < 0){
            throw new IndexOutOfBoundsException();
        }
        this.array[index] = value;
    }

    private void resize() {
        this.capacity = this.capacity * 3 / 2 + 1;
        int[] tmp = new int[this.capacity];
        for (int i = 0; i < this.size; i++) {
            tmp[i] = this.array[i];
        }
        this.array = tmp;
    }

    private void quickSort(int[] array, int start, int end) {

        if (array.length == 0)
            return;

        if (start >= end)
            return;

        int middle = start + (end - start) / 2;
        int pivot = array[middle];

        int i = start, j = end;
        while (i <= j) {
            while (array[i] < pivot) {
                i++;
            }

            while (array[j] > pivot) {
                j--;
            }

            if (i <= j) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            }
        }

        if (start < j)
            quickSort(array, start, j);

        if (end > i)
            quickSort(array, i, end);
    }

}
