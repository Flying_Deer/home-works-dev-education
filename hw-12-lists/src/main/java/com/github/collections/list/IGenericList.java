package com.github.collections.list;

public interface IGenericList<T> {

    void clear();

    int size();

    Object[] toArray();

    void pushForward(T element);

    void pushBack(T element);

    void push(int index, T element);

    T removeForward();

    T removeBack();

    T removeByIndex(int index);

    T remove(T element);

    T get(int index);

    boolean set(int index, T element);

    boolean contains(T element);

}
