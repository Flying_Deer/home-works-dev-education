package com.github.collections.list;

import java.util.NoSuchElementException;
import java.util.Objects;

public class CustomLinkedList<T> implements IGenericList<T>{

    private class Node implements Cloneable{
        T value;
        Node next;
        public Node(T value) {
            this.value = value;
            next = null;
        }

        @Override
        public Node clone(){
            Node result = new Node(this.value);
            result.next = this.next;
            return result;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node node = (Node) o;
            return Objects.equals(value, node.value);
        }

        @Override
        public int hashCode() {
            return Objects.hash(value);
        }
    }

    private Node root;
    private int size;

    public CustomLinkedList() {
        this.size = 0;
        this.root = null;
    }

    public CustomLinkedList(T[] array) {
        this.size = 0;
        for(T element : array){
            this.pushBack(element);
        }
    }

    @Override
    public void clear() {
        this.size = 0;
        this.root = null;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public Object[] toArray() {
        Node currentNode = this.root;
        Object[] array = new Object[this.size];
        for(int i = 0; i < this.size; i++){
            array[i] = currentNode.value;
            currentNode = currentNode.next;
        }
        return array;
    }

    @Override
    public void pushForward(T element) {
        if(this.size == 0){
            this.root = new Node(element);
            this.size++;
            return;
        }
        Node newNode = new Node(element);
        newNode.next = this.root;
        this.root = newNode;
        size++;
    }

    @Override
    public void pushBack(T element) {
        if(this.size == 0){
            this.root = new Node(element);
            this.size++;
            return;
        }
        Node node = this.root;
        while (node.next != null){
            node = node.next;
        }
        node.next = new Node(element);
        size++;
    }

    @Override
    public void push(int index, T element) {
        if(index < 0 || index > this.size){
            throw new IndexOutOfBoundsException();
        }
        if(index == 0){
            this.pushForward(element);
            return;
        }
        Node node = this.root;
        for(int i = 0; i < index - 1; i++){
            node = node.next;
        }
        node.next = new Node(element);
        size++;
    }

    @Override
    public T removeForward() {
        if(this.size == 0){
            throw new IndexOutOfBoundsException();
        }
        Node tmp = this.root;
        this.root = this.root.next;
        size--;
        return tmp.value;
    }

    @Override
    public T removeBack() {
        if(this.size == 0){
            throw new IndexOutOfBoundsException();
        }
        if(this.size == 1){
            Node tmp = this.root.clone();
            this.root = null;
            size--;
            return tmp.value;
        }
        Node node = this.root;
        for(int i = 0; i < this.size - 2; i++){
            node = node.next;
        }
        Node tmp = node.next;
        node.next = null;
        size--;
        return tmp.value;
    }

    @Override
    public T removeByIndex(int index) {
        if(this.size == 0){
            throw new IndexOutOfBoundsException();
        }
        if(index < 0 || index >= this.size){
            throw new IndexOutOfBoundsException();
        }
        if(index == 0){
            return this.removeForward();
        }
        Node node = this.root;
        for(int i = 0; i < index - 1; i++){
            node = node.next;
        }
        Node tmp = node.next;
        node.next = node.next.next;
        size--;
        return tmp.value;
    }

    @Override
    public T remove(T element) {
        if(this.size == 0){
            throw new NoSuchElementException();
        }
        if(this.size == 1){
            if(this.root.value.equals(element)){
                this.root = null;
                this.size--;
                return element;
            }
            throw new NoSuchElementException();
        }
        for( Node node = this.root; node.next != null; node = node.next){
            if(node.next.value.equals(element)){
                node.next = node.next.next;
                this.size--;
                return element;
            }
        }
        throw new NoSuchElementException();
    }

    @Override
    public T get(int index) {
        if(index < 0 || index >= size){
            throw new IndexOutOfBoundsException();
        }
        Node node = this.root;
        for(int i = 0; i < index; i++){
            node = node.next;
        }
        return node.value;
    }

    @Override
    public boolean set(int index, T element) {
        if(index < 0 || index >= size){
            throw new IndexOutOfBoundsException();
        }
        Node node = this.root;
        for(int i = 0; i < index; i++){
            node = node.next;
        }
        node.value = element;
        return true;
    }

    @Override
    public boolean contains(T element) {
        for(Node node = this.root; node != null; node = node.next){
            if(node.value.equals(element)){
                return true;
            }
        }
        return false;
    }

}
