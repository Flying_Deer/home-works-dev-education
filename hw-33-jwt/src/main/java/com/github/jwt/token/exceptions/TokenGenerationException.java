package com.github.jwt.token.exceptions;

public class TokenGenerationException extends RuntimeException {

    public TokenGenerationException() {
    }

    public TokenGenerationException(String message) {
        super(message);
    }

}
