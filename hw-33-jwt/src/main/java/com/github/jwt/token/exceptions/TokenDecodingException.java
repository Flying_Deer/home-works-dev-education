package com.github.jwt.token.exceptions;

public class TokenDecodingException extends RuntimeException {

    public TokenDecodingException() {
    }

    public TokenDecodingException(String message) {
        super(message);
    }

}
