package com.github.jwt.token;

import com.github.jwt.token.annotations.TokenField;
import com.github.jwt.token.exceptions.TokenDecodingException;
import com.github.jwt.token.exceptions.TokenGenerationException;
import com.github.jwt.token.utils.ReflectionUtils;
import com.google.common.collect.Lists;
import com.google.gson.JsonObject;
import net.oauth.jsontoken.Checker;
import net.oauth.jsontoken.JsonToken;
import net.oauth.jsontoken.JsonTokenParser;
import net.oauth.jsontoken.crypto.HmacSHA256Signer;
import net.oauth.jsontoken.crypto.HmacSHA256Verifier;
import net.oauth.jsontoken.crypto.SignatureAlgorithm;
import net.oauth.jsontoken.crypto.Verifier;
import net.oauth.jsontoken.discovery.VerifierProvider;
import net.oauth.jsontoken.discovery.VerifierProviders;
import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.security.InvalidKeyException;
import java.util.Calendar;
import java.util.Objects;
import java.util.Set;

public class TokenProvider<T> {

    private static final Logger log = LoggerFactory.getLogger(TokenProvider.class);

    private final Set<Field> tokenFields;

    private static final String ISSUER = "HW-33-JWT";

    private static final String SIGNING_KEY = "NotTheBestChoice";

    private HmacSHA256Signer signer = null;

    private Verifier hmacVerifier = null;

    private JsonTokenParser parser = null;

    public TokenProvider(Class<T> entityClass) {
        this.tokenFields = this.findTokenFields(entityClass);
    }

    public String generateToken(T entity, long lifetimeInMilliseconds) {
        try {
            if(Objects.isNull(signer)){
                initializeSigner();
            }
            JsonToken token = generateTokenWithSigner();
            this.initializeIssuedAt(token);
            this.initializeExpiration(token, lifetimeInMilliseconds);
            this.initializePayload(token, entity);
            return token.serializeAndSign();
        } catch (Exception e) {
            log.error("Exception {}, Message {}", e.getClass(), e.getMessage());
            throw new TokenGenerationException();
        }
    }

    public boolean isValidToken(String token){
        try {
            if(Objects.isNull(parser)){
                this.initializeParser();
            }
            JsonToken jt = parser.verifyAndDeserialize(token);
            return this.isValidToken(jt);
        } catch (Exception e) {
            log.error("Exception {}, Message {}", e.getClass(), e.getMessage());
            throw new TokenDecodingException();
        }
    }

    private boolean isValidToken(JsonToken token){
        JsonObject payload = token.getPayloadAsJsonObject();
        long expirationTime = payload.getAsJsonPrimitive("exp").getAsLong() * 1000L;
        long currentTime = Calendar.getInstance().getTimeInMillis();
        return expirationTime > currentTime;
    }

    private void initializeSigner() throws InvalidKeyException {
        signer = new HmacSHA256Signer(ISSUER, null, SIGNING_KEY.getBytes());
    }

    private void initializeVerifier() throws InvalidKeyException {
        hmacVerifier = new HmacSHA256Verifier(SIGNING_KEY.getBytes());
    }

    private void initializeParser() throws InvalidKeyException {
        if(Objects.isNull(hmacVerifier)){
            initializeVerifier();
        }
        VerifierProvider hmacLocator = (id, key) -> Lists.newArrayList(hmacVerifier);
        VerifierProviders locators = new VerifierProviders();
        locators.setVerifierProvider(SignatureAlgorithm.HS256, hmacLocator);
        Checker checker = payload -> {};
        parser = new JsonTokenParser(locators, checker);
    }

    private JsonToken generateTokenWithSigner() {
        return new JsonToken(signer);
    }

    private void initializeIssuedAt(JsonToken token) {
        Calendar cal = Calendar.getInstance();
        token.setIssuedAt(new Instant(cal.getTimeInMillis()));
    }

    private void initializeExpiration(JsonToken token, long lifetimeInMilliseconds) {
        Calendar cal = Calendar.getInstance();
        token.setExpiration(new Instant(cal.getTimeInMillis() + lifetimeInMilliseconds));
    }

    private void initializePayload(JsonToken token, T entity) throws IllegalAccessException {
        JsonObject request = new JsonObject();
        for (Field field : tokenFields) {
            request.addProperty(field.getName(), field.get(entity).toString());
        }
        JsonObject payload = token.getPayloadAsJsonObject();
        payload.add("info", request);
    }

    private Set<Field> findTokenFields(Class<T> entityClass) {
        return ReflectionUtils.getAccessibleFields(entityClass, TokenField.class);
    }


}
