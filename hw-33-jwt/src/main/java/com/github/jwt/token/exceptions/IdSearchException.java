package com.github.jwt.token.exceptions;

public class IdSearchException extends RuntimeException {

    public IdSearchException() {
    }

    public IdSearchException(String message) {
        super(message);
    }
}
