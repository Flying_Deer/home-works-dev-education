package com.github.jwt.token.exceptions;

public class CustomInvalidKeyException extends RuntimeException {

    public CustomInvalidKeyException() {
    }

    public CustomInvalidKeyException(String message) {
        super(message);
    }

}
