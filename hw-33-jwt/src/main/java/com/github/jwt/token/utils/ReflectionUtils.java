package com.github.jwt.token.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

public class ReflectionUtils {

    private static Logger log = LoggerFactory.getLogger(ReflectionUtils.class);

    public static Set<Field> getAccessibleFields(Class<?> clz, Class<? extends Annotation> ann) {
        Set<Field> set = new HashSet<>();
        Class<?> c = clz;
        while (c != null) {
            for (Field field : c.getDeclaredFields()) {
                if (field.isAnnotationPresent(ann)) {
                    log.info(String.format("Field \"%s\" annotated with @%s found.", field.getName(), ann.getSimpleName()));
                    field.setAccessible(Boolean.TRUE);
                    set.add(field);
                }
            }
            c = c.getSuperclass();
        }
        return set;
    }

}
