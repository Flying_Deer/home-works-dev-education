package com.github.jwt.token;

import com.github.jwt.token.entities.User;
import org.junit.Assert;
import org.junit.Test;

public class TokenProviderTest {

    private static User user = new User(0L, "login", "password", "nickname");

    public static TokenProvider<User> userTokenProvider = new TokenProvider<>(User.class);

    @Test
    public void generateTokenTest(){
        String token = userTokenProvider.generateToken(user, 100000L);
        Assert.assertTrue(userTokenProvider.isValidToken(token));
    }

}